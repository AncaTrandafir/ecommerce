
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { BehaviorSubject, ReplaySubject, of } from 'rxjs';
import { IUser } from '../shared/models/user';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { IAddress } from '../shared/models/address';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  baseUrl = environment.apiUrl;
  private currentUserSource = new ReplaySubject<IUser>(1);
  currentUser$ = this.currentUserSource.asObservable();

  constructor(private http: HttpClient,
              private router: Router) { }

  login(values: any) {
    return this.http.post(this.baseUrl + 'account/login', values).pipe(
      map((user: IUser) => {
        if (user) {
          localStorage.setItem('token', user.token);
          this.currentUserSource.next(user);
          console.log(user);
        }
      })
    );
  }


  register(values: any) {
    return this.http.post(this.baseUrl + 'account/register', values).pipe(
      map((user: IUser) => {
        if (user) {
          localStorage.setItem('token', user.token);
          this.currentUserSource.next(user);

        }
      })
    );
  }


  logout() {
    localStorage.removeItem('token');
    this.currentUserSource.next(null);
    this.router.navigateByUrl('/');
  }


  checkEmailExists(email: string) {
    return this.http.get(this.baseUrl + 'account/emailexists?email=' + email);
  }


  loadCurrentUser(token: string) {
    if (token === null) {
      this.currentUserSource.next(null);
      return of(null);
    }

    let headers = new HttpHeaders();
    headers = headers.set('Authorization', `Bearer ${token}`);
    return this.http.get(this.baseUrl + 'account', { headers }).pipe(
      map((user: IUser) => {
        if (user) {
          user.roles = [];
          const roles = this.getDecodedToken(user.token).role;

          Array.isArray(roles) ? user.roles = roles : user.roles.push(roles);

          localStorage.setItem('token', user.token);
          this.currentUserSource.next(user);

          console.log(user);
        }
      }))
  }




  getUserAddress() {
    return this.http.get<IAddress>(this.baseUrl + 'account/address');
  }



  updateUserAddress(address: IAddress) {
    return this.http.put<IAddress>(this.baseUrl + 'account/address', address);
  }


  getDecodedToken(token) {    // get token from JSON
    return JSON.parse(atob(token.split('.')[1]));
  }


  sendForgotPasswordEmail(model: any) {
    return this.http.post(this.baseUrl + 'account/forgotpassword', model);
  }


  resetPassword(model: any) {
    return this.http.post(this.baseUrl + 'account/resetpassword', model);
  }


  confirmEmail(model: any) {
    return this.http.post(this.baseUrl + 'account/confirmemail', model);
  }



  FBAuthentication(userdata) {
    return this.http.post<IUser>(this.baseUrl + 'account/authenticate-facebook', userdata).pipe(
      map(user => {
        if (user) {
          localStorage.setItem('token', user.token);
          this.currentUserSource.next(user);
          console.log("fjfgjfjtrfjr");
        } 

        return user;

      })
    );
  }
}
