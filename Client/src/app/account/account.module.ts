import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AccountRoutingModule } from './account-routing.module';
import { SharedModule } from '../shared/shared.module';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ConfirmationEmailSentComponent } from './confirmation-email-sent/confirmation-email-sent.component';
import { ConfirmEmailComponent } from './confirm-email/confirm-email.component';
import { ConfirmPasswordSentComponent } from './confirm-password-sent/confirm-password-sent.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ResetPasswordCompleteComponent } from './reset-password-complete/reset-password-complete.component';



@NgModule({
  declarations: [LoginComponent, RegisterComponent, ForgotPasswordComponent, ConfirmationEmailSentComponent, ConfirmEmailComponent, ConfirmPasswordSentComponent, ResetPasswordComponent, ResetPasswordCompleteComponent],
  imports: [
    CommonModule,
    AccountRoutingModule,
    SharedModule
  ]
})
export class AccountModule { }
