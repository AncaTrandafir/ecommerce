
import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { AccountService } from '../account.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SocialAuthService } from 'angularx-social-login';
import { FacebookLoginProvider } from 'angularx-social-login';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  returnUrl: string;

  //create array to store user data we need
  userData: any[] = [];

  constructor(private accountService: AccountService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private authService: SocialAuthService) { }

  ngOnInit() {
    this.returnUrl = this.activatedRoute.snapshot.queryParams.returnUrl || '/shop';
    this.createLoginForm();
  }


  createLoginForm() {
    this.loginForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.pattern('^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$')]),
      password: new FormControl('', [Validators.required])
    });
  }


  onSubmit() {
    this.accountService.login(this.loginForm.value).subscribe(() => {
      this.router.navigateByUrl(this.returnUrl);
    }, error => {
      console.log(error);
    });
  }



  forgotPassword() {
    this.router.navigateByUrl('../account/forgot-password/');
  }




  //logIn with facebook method. Takes the platform (Facebook) parameter.
  logInWithFacebook(platform: string): any {

    console.log("facebook clicked");

    platform = FacebookLoginProvider.PROVIDER_ID;
    //Sign In and get user Info using authService that we just injected
    this.authService.signIn(platform).then(

      (response) => {

        //Get all user details
        console.log(platform + ' logged in user data is= ', response);

        //Take the details we need and store in an array
        this.userData.push({

          Email: response.email,
          DisplayName: response.firstName,
          Token: response.authToken // creez propriul JWT token in API
        });

        console.log(this.userData[0]);

        this.accountService.FBAuthentication(this.userData[0]).subscribe(
          result => {
            console.log('success', result);
            console.log("gd");
            localStorage.setItem('token', result.token);
            this.router.navigateByUrl(this.returnUrl);
          },
          error => {
            console.log(error);
          }
        );
      },
      (error) => {
        console.log(error);
      });


  }
}
