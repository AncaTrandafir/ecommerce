import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-confirmation-email-sent',
  templateUrl: './confirmation-email-sent.component.html',
  styleUrls: ['./confirmation-email-sent.component.scss']
})
export class ConfirmationEmailSentComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
