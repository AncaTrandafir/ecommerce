import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-confirm-password-sent',
  templateUrl: './confirm-password-sent.component.html',
  styleUrls: ['./confirm-password-sent.component.scss']
})
export class ConfirmPasswordSentComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
