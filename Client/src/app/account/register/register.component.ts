
import { Component, OnInit } from '@angular/core';
import { AccountService } from '../account.service';
import { FormBuilder, FormGroup, Validators, AsyncValidatorFn, FormControl, ValidatorFn, AbstractControl } from '@angular/forms';
import { Router } from '@angular/router';
import { timer, of } from 'rxjs';
import { switchMap, map } from 'rxjs/operators';
import { SocialAuthService } from 'angularx-social-login';
import { FacebookLoginProvider } from 'angularx-social-login';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  errors: string[];

  //create array to store user data we need
  userData: any[] = [];

  constructor(private fb: FormBuilder, private accountService: AccountService, private router: Router, private authService: SocialAuthService) { }

  ngOnInit() {
    this.createRegisterForm();
  }


  createRegisterForm() {
    this.registerForm = this.fb.group({
      displayName: [null, [Validators.required]],

      //email: [null,
      //       [Validators.required, Validators.pattern('^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$')],
      //       [this.validateEmailNotTaken()]   // async validation called only when the others have passed
      //       ],

      email: new FormControl('', [
        Validators.required,
        Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')
      ]),

      confirmEmail: new FormControl('', [
        Validators.required,
        Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'),
        this.matchEmail('email')
      ]),

      password: [null, [Validators.required]],

      confirmPassword: [
        '',
        [Validators.required, this.matchPassword('password')],
      ],

    });
  }




  matchEmail(email: string): ValidatorFn {
    return (control: AbstractControl) => {
      return control?.value === control?.parent?.controls[email].value
        ? null
        : { emailMatch: true };
    };
  }


  matchPassword(matchTo: string): ValidatorFn {
    return (control: AbstractControl) => {
      return control?.value === control?.parent?.controls[matchTo].value
        ? null
        : { isMatching: true };
    };
  }




  onSubmit() {
    this.accountService.register(this.registerForm.value).subscribe(response => {
      this.router.navigateByUrl('/shop');
    }, error => {
      console.log(error);
      this.errors = error.errors;

    });

  }


  validateEmailNotTaken(): AsyncValidatorFn {     // validate email while typing
    return control => {
      return timer(500).pipe(   // half of second
        switchMap(() => {
          if (!control.value) {
            return of(null);
          }
          return this.accountService.checkEmailExists(control.value).pipe(
            map(res => {
              return res ? { emailExists: true } : null;
            })
          );
        })
      );
    };
  }



  //logIn with facebook method. Takes the platform (Facebook) parameter.
  logInWithFacebook(platform: string): any {

    console.log("facebook clicked");

    platform = FacebookLoginProvider.PROVIDER_ID;
    //Sign In and get user Info using authService that we just injected
    this.authService.signIn(platform).then(

      (response) => {
        //Get all user details
        console.log(platform + ' logged in user data is= ', response.name);

        //Take the details we need and store in an array
        this.userData.push({

          Email: response.email,
          DisplayName: response.firstName,
          Token: response.authToken // creez propriul JWT token in API
        });

        console.log(this.userData[0]);

        this.accountService.FBAuthentication(this.userData[0]).subscribe(
          result => {
            console.log('success', result);
            localStorage.setItem('token', result.token);
            this.router.navigateByUrl('/shop');
          },
          error => {
            console.log(error);
          }
        );
      },
      (error) => {
        console.log(error);
      });


  }
}
