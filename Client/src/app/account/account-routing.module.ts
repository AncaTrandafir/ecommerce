import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ConfirmEmailComponent } from './confirm-email/confirm-email.component';
import { ConfirmationEmailSentComponent } from './confirmation-email-sent/confirmation-email-sent.component';
import { ConfirmPasswordSentComponent } from './confirm-password-sent/confirm-password-sent.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ResetPasswordCompleteComponent } from './reset-password-complete/reset-password-complete.component';


const routes: Routes = [
  { path: 'login', component: LoginComponent, data: { breadcrumb: 'Login' } },
  { path: 'register', component: RegisterComponent, data: { breadcrumb: 'Register' } },
  { path: 'forgot-password', component: ForgotPasswordComponent, data: { breadcrumb: 'Forgot password' } },
  { path: 'confirm-email', component: ConfirmEmailComponent, data: { breadcrumb: 'Confirm email' } },
  { path: 'confirmation-email-sent', component: ConfirmationEmailSentComponent, data: { breadcrumb: 'Email sent confirmation' } },
  { path: 'confirm-password-sent', component: ConfirmPasswordSentComponent, data: { breadcrumb: 'Password sent confirmation' } },
  { path: 'reset-password', component: ResetPasswordComponent, data: { breadcrumb: 'Reset password' } },
  { path: 'reset-password-complete', component: ResetPasswordCompleteComponent, data: { breadcrumb: 'Reset password confirmation' } }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class AccountRoutingModule { }
