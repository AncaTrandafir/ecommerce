import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { IBasket, IBasketItem, Basket, IBasketTotals } from '../shared/models/basket';
import { IProduct } from '../shared/models/product';
import { IDeliveryMethod } from '../shared/models/deliveryMethod';


@Injectable({
  providedIn: 'root'
})
export class BasketService {

  baseUrl = environment.apiUrl;

  private basketSource = new BehaviorSubject<IBasket>(null);  // initial value to null
  basket$ = this.basketSource.asObservable();

  private basketTotalSource = new BehaviorSubject<IBasketTotals>(null);
  basketTotal$ = this.basketTotalSource.asObservable();

  shipping = 0;


  constructor(private http: HttpClient) { }



  getBasket(id: string) {
    return this.http.get(this.baseUrl + 'basket?id=' + id)
      .pipe(
        map((basket: IBasket) => {
          this.basketSource.next(basket);
          this.shipping = basket.shippingPrice;
          this.calculateTotals();

          console.log(basket);
      })
    );
  }



  setBasket(basket: IBasket) {
    return this.http.post(this.baseUrl + 'basket', basket)
      .subscribe((response: IBasket) => {
        this.basketSource.next(response);   // updates the behavior subject with the new value that we have
        this.calculateTotals();
      }, error => {
        console.log(error);
      });
  }



  getCurrentBasketValue() {
    return this.basketSource.value;
  }



  addItemToBAsket(item: IProduct, quantity = 1) {
    const itemToAdd: IBasketItem = this.mapProductItemToBasketItem(item, quantity);
    const basket = this.getCurrentBasketValue() ?? this.createBasket();   // if null create basket
    console.log(basket);
    basket.items = this.addOrUpdateItem(basket.items, itemToAdd, quantity);
    this.setBasket(basket);
  }



  incrementItemQuantity(item: IBasketItem) {
    const basket = this.getCurrentBasketValue();
    const foundItemIndex = basket.items.findIndex(x => x.id === item.id);   // check to see if we have the item by id
    basket.items[foundItemIndex].quantity++;
    this.setBasket(basket);
  }


  decrementItemQuantity(item: IBasketItem) {
    const basket = this.getCurrentBasketValue();
    const foundItemIndex = basket.items.findIndex(x => x.id === item.id);
    if (basket.items[foundItemIndex].quantity > 1) {
      basket.items[foundItemIndex].quantity--;
      this.setBasket(basket);
    }
    else {
      this.removeItemFromBasket(item);
    }
  }



  removeItemFromBasket(item: IBasketItem) {
    const basket = this.getCurrentBasketValue();
    // if there are any items in the basket that match the id
    if (basket.items.some(x => x.id === item.id)) {
      // return an array of all of the items that do not match the id
      basket.items = basket.items.filter(i => i.id !== item.id)
      // check the basket length to see it if's greater than 0
      if (basket.items.length !== 0) {
        // set the basket with the current basket
        this.setBasket(basket);
      } else {
        // we now have an empty basket so we remove it
        this.deleteBasket(basket);
      }
    }
  }



  deleteLocalBasket(id: string) {     // clean up method to remove basket from Redis DB
    this.basketSource.next(null);     // so as user wouldn't create an empty basket with the same id
    this.basketTotalSource.next(null);
    localStorage.removeItem('basket_id');
  }



  deleteBasket(basket: IBasket) {
    return this.http.delete(this.baseUrl + 'basket?id=' + basket.id)
      .subscribe(() => {
        this.basketSource.next(null);   // is equal to null
        this.basketTotalSource.next(null);
        localStorage.removeItem('basket_id');   // remove basketId from local storage
      }, error => {
        console.log(error);
      });
    }


  private addOrUpdateItem(items: IBasketItem[], itemToAdd: IBasketItem, quantity: number): IBasketItem[] {
        // check to see if there's already a product of the same type by looking for the product id
        // if there is, we increase the quantity, if not we push a new item into our list
    console.log(items);
    const index = items.findIndex(i => i.id === itemToAdd.id);
    if (index == -1) {   // id not found
      itemToAdd.quantity = quantity;
      items.push(itemToAdd);
    } else {
      items[index].quantity += quantity;  // add the quantity on top of the current quantity of item
    }
    return items;
  }


   private createBasket(): IBasket {
     const basket = new Basket();
     // Use local storage on client's browser to store the basket's id once it's been created
     localStorage.setItem('basket_id', basket.id);    // persistence, even if client closes browser or restarts PC, as long as he doesn't clear local storage
     return basket;
   }


  private mapProductItemToBasketItem(item: IProduct, quantity: number): IBasketItem {
    return {
      id: item.id,
      productName: item.name,
      price: item.price,
      photoUrl: item.photoUrl,
      quantity,
      brand: item.productBrand,
      type: item.productType
    };
  }


  private calculateTotals() {
    const basket = this.getCurrentBasketValue();
    const shipping = this.shipping;
    const subtotal = basket.items.reduce((a, b) => (b.price * b.quantity) + a, 0);
      // b is the item whcih has a price and a quantity
    // a is the result of the reduce and it's given an initial value of 0
    const total = subtotal + shipping;
    this.basketTotalSource.next({ shipping, total, subtotal });
  }




  setShippingPrice(deliveryMethod: IDeliveryMethod) {
    this.shipping = deliveryMethod.price;
    const basket = this.getCurrentBasketValue();
    basket.deliveryMethodId = deliveryMethod.id;
    basket.shippingPrice = deliveryMethod.price;    // persist shippingPrice in OrderSummary when refresh
    this.calculateTotals();
    this.setBasket(basket);

  }



  createPaymentIntent() {
    return this.http.post(this.baseUrl + 'payments/' + this.getCurrentBasketValue().id, {})
      .pipe(
        map((basket: IBasket) => {
          this.basketSource.next(basket);     // update the basket with clientSecret and paymentIntentId
        })
      );
  }

}
