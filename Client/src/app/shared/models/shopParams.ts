export class ShopParams {   // unlike interface, we can initialize values to properties

  brandIdSelected = 0;      // initial value for id is 0 => all options selected, as we added id=0 to the array of brands
  typeIdSelected = 0;
  sortSelected = 'name';
  search: string;
  pageNumber = 1;
  pageSize = 6;
  predicate = 'favouredBy';    // for Favourite functionality

}
