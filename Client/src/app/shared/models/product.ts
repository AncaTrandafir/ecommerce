import { Photo } from './photo';

export interface IProduct {
  id: number;
  name: string;
  description: string;
  price: number;
  photoUrl: string;
  photos: Photo[];
  productType: string;
  productBrand: string;
}

export interface IProductToCreate {
  name: string;
  description: string;
  price: number;
  photos: Photo[];
  productTypeId: number;
  productBrandId: number;
}

export class ProductFormValues implements IProductToCreate {
  name = '';
  description = '';
  price = 0;
  photos = null;
  productBrandId: number;
  productTypeId: number;

  constructor(init?: ProductFormValues) {
    Object.assign(this, init);
  }
}
