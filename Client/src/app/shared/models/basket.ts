import { v4 as uuidv4 } from 'uuid';

export interface IBasket {
  id: string;
  items: IBasketItem[];
  shippingPrice?: number;
  // Stripe
  clientSecret?: string;
  paymentIntentId?: string;
  deliveryMethodId?: number;
}

export interface IBasketItem {
    id: number;
    productName: string;
    price: number;
    quantity: number;
    photoUrl: string;
    brand: string;
    type: string;
}


export class Basket implements IBasket {
    id = uuidv4();    // unique identifier like guid
    items: IBasketItem[] = [];    // equal to an empty array so that it is defined

}

export interface IBasketTotals {
  shipping: number;
  subtotal: number;
  total: number;
}
