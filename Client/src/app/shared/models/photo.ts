export interface Photo {
  id: number;
  photoUrl: string;
  fileName: string;
  isMain: boolean;
}
