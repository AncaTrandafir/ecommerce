
import { Component, OnInit, ElementRef, ViewChild, Input, Self, Optional } from '@angular/core';
import { ControlValueAccessor, NgControl } from '@angular/forms';

@Component({
  selector: 'app-text-input',
  templateUrl: './text-input.component.html',
  styleUrls: ['./text-input.component.scss']
})
export class TextInputComponent implements OnInit, ControlValueAccessor {   // bridge between Angular form and element in DOM

  @ViewChild('input', { static: true }) input: ElementRef;
  @Input() type = 'text';
  @Input() label: string;

  constructor(@Self() @Optional() public controlDir: NgControl) {
    if (this.controlDir) {
      this.controlDir.valueAccessor = this;
    }
  }

  ngOnInit() {
    const control = this.controlDir.control;
    const validators = control.validator ? [control.validator] : [];
    const asyncValidators = control.asyncValidator ? [control.asyncValidator] : [];

    control.setValidators(validators);
    control.setAsyncValidators(asyncValidators);
    control.updateValueAndValidity();
  }





  onChange(event) {
  }


  onTouched() {
  }



  writeValue(obj: any): void {    // access the value from input field
    this.input.nativeElement.value = obj || '';
  }


  registerOnChange(fn: any): void {
    this.onChange = fn;   // control value accessor function

  }



  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }



}
