import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ImageCroppedEvent, base64ToFile } from 'ngx-image-cropper';

@Component({
  selector: 'app-photo-widget',
  templateUrl: './photo-widget.component.html',
  styleUrls: ['./photo-widget.component.scss']
})
export class PhotoWidgetComponent implements OnInit {
  files: File[] = [];
  imageChangedEvent: any = '';
  croppedImage: any = '';

  // Output property so that we can emit the file and send it to the uploadFile method in the edit-product-photo component.
  @Output() addFile = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }


  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }


  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
  }

  // Passing the dropped file to the fileChangeEvent for the image cropper.
  // using a method from the image cropper to convert the data URL that the image cropper produces
  // so that we can output our cropped image as a file which makes it easier to upload to the API as we do not need to convert it to a file on the API.
  onSelect(event) {
    this.files = [];
    this.files.push(...event.addedFiles);
    this.fileChangeEvent(this.files[0]);
  }


  onUpload() {
    console.log(base64ToFile(this.croppedImage));
    this.addFile.emit(base64ToFile(this.croppedImage));
  }
}
