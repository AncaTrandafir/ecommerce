import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-pager',
  templateUrl: './pager.component.html',
  styleUrls: ['./pager.component.scss']
})
export class PagerComponent implements OnInit {

  @Input() totalCount: number;
  @Input() pageSize: number;      // an input property we receive from a parent component
  @Input() pageNumber: number;
  @Output() pageChanged = new EventEmitter<number>();    // we emit an output from our child component to our parent component
      // we want to call onPageChanged() from shop component to pager component

  constructor() { }

  ngOnInit() {
  }



  onPagerChange(event: any) {
    this.pageChanged.emit(event.page);    // we emit the page number from child component and pass it to shop component
  }

}
