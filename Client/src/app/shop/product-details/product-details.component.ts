import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IProduct } from '../../shared/models/product';
import { ShopService } from '../shop.service';
import { BreadcrumbService } from 'xng-breadcrumb';
import { BasketService } from '../../basket/basket.service';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation, NgxGalleryImageSize } from '@kolkov/ngx-gallery';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss']
})
export class ProductDetailsComponent implements OnInit {

  product: IProduct;
  quantity = 1;

  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[];

  constructor(private shopService: ShopService,
              private basketService: BasketService,
              private activatedRoute: ActivatedRoute,
              private breadcrumbService: BreadcrumbService,
              private toastr: ToastrService) {
      
      this.breadcrumbService.set('@productDetails', ' ');
  }

  ngOnInit() {
    this.loadProduct();
  }

  initializeGallery() {
    this.galleryOptions = [
      {
        width: '500px',
        height: '500px',
        imagePercent: 100,
        thumbnailsColumns: 4,
        imageArrows: true,
        imageAnimation: NgxGalleryAnimation.Slide,
        imageSize: NgxGalleryImageSize.Contain,
        thumbnailSize: NgxGalleryImageSize.Contain,
        preview: false
      }
    ];  
  }



  getImages(): NgxGalleryImage[] {
    const imageUrls = [];
    console.log(this.product.photos);
    for (const photo of this.product.photos) {
      imageUrls.push({
        small: photo?.photoUrl,
        medium: photo?.photoUrl,
        big: photo?.photoUrl
      });
    }
    return imageUrls;
  }



  loadProduct() {
    // + -> to convert from string from URL to integer
    this.shopService.getProduct(+this.activatedRoute.snapshot.paramMap.get('id'))
      .subscribe(response => {
        this.product = response;
        this.breadcrumbService.set('@productDetails', this.product.name)   // alias for breadcrumb in shop-routing module
        this.initializeGallery();
        this.galleryImages = this.getImages();
      },
      error => {
        console.log(error);
    });
  }


  addItemToBasket() {
    this.basketService.addItemToBAsket(this.product, this.quantity);
  }


  incrementQuantity() {
    this.quantity++;
  }


  decrementQuantity() {
    if (this.quantity > 1) {
      this.quantity--;
    }
  }



  addFavourite(product: IProduct) {
    this.shopService.addFavourite(product.id).subscribe(() => {
      this.toastr.success('Item ' + product.name + ' is marked as Favourite.');
    });
  }

}
