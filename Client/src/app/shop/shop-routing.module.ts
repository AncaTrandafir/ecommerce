import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShopComponent } from './shop.component';
import { ProductDetailsComponent } from './product-details/product-details.component';


const routes: Routes = [
  { path: '', component: ShopComponent },   // route component for shop module, no need to specify in path
  {
    path: ':id', component: ProductDetailsComponent, data: {
      breadcrumb: { alias: 'productDetails' }
    }
  }
]


@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
    // these routes are not available on app module, but on child = shop
  ],
  exports: [RouterModule]
})
export class ShopRoutingModule { }
