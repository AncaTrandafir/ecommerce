import { Component, OnInit, Input} from '@angular/core';
import { IProduct } from '../../shared/Models/product';
import { BasketService } from '../../basket/basket.service';
import { ShopService } from '../shop.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.scss']
})
export class ProductItemComponent implements OnInit {
  @Input() product: IProduct;    // import parent component which is ShopComponent
  

  constructor(private basketService: BasketService) { }

  ngOnInit() {
  }


  addItemToBasket() {
      this.basketService.addItemToBAsket(this.product);

  }

}
