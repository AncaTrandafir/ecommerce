import { Component, OnInit, Output } from '@angular/core';
import { ShopService } from '../shop.service';
import { IProduct } from '../../shared/models/product';
import { ShopParams } from '../../shared/models/shopParams';
import { IPagination } from '../../core/helpers/paginatedResult';


@Component({
  selector: 'app-favourite',

  templateUrl: './favourite.component.html',
  styleUrls: ['./favourite.component.scss']
})
export class FavouriteComponent implements OnInit {

  favourites: IProduct[];
  @Output() product: IProduct;

  predicate = 'favouredBy';
  pageIndex = 1;
  pageSize = 6;
  pagination: IPagination;

  totalCount: number;
  shopParams: ShopParams;

  constructor(private shopService: ShopService) {
    this.shopParams = this.shopService.getShopParams();}

  ngOnInit(): void {
    this.loadFavourites();
  }


  loadFavourites() {
    this.shopService.getUserFavourites(this.predicate, this.pageIndex, this.pageSize).subscribe(response => {
      this.favourites = response.result;
      this.pagination = response.pagination;
    }, error => {
      console.log(error);
    });
  }

  //onPageChanged(event: any) {
  //  const params = this.shopService.getShopParams();
  //  if (params.pageNumber !== event) {
  //    params.pageNumber = event;   
  //    this.shopService.setShopParams(params);
  //    this.loadFavourites();   
  //  }
  //}

  onPageChanged(event: any) {
    this.pageIndex = event.page;
    this.loadFavourites();
  }

}
