import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShopComponent } from './shop.component';
import { ProductItemComponent } from './product-item/product-item.component';
import { SharedModule } from '../shared/shared.module';
import { ShopRoutingModule } from './shop-routing.module';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { NgxGalleryModule } from '@kolkov/ngx-gallery';
import { FavouriteComponent } from './favourite/favourite.component';



@NgModule({
  declarations: [ShopComponent, ProductItemComponent, ProductDetailsComponent, FavouriteComponent],
  imports: [
    CommonModule,
    SharedModule,
    ShopRoutingModule,
    NgxGalleryModule
  ],
  exports: [
    NgxGalleryModule
  ]
})
export class ShopModule { }
