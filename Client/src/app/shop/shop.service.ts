import { Injectable, Output } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { IPagination, Pagination } from '../shared/models/pagination';
import { IBrand } from '../shared/models/brand';
import { IType } from '../shared/models/productType';
import { map } from 'rxjs/operators';
import { ShopParams } from '../shared/models/shopParams';
import { IProduct } from '../shared/models/product';
import { of } from 'rxjs';
import { environment } from '../../environments/environment.prod';
import { getPaginatedResult, getPaginationHeaders } from '../core/helpers/paginationHelper';

@Injectable({
  providedIn: 'root'
})
export class ShopService {

 // baseUrl = "https://localhost:44312/api/";   // development
  baseUrl = environment.apiUrl;   // production

  products: IProduct[] = [];
  brands: IBrand[] = [];
  types: IType[] = [];

  favourites: IProduct[] = [];

  @Output() product: IProduct; // relationship with child component favourite

  pagination = new Pagination();
  shopParams = new ShopParams();


  constructor(private http: HttpClient) { }



  getProducts(useCache: boolean) {
    if (useCache === false)
      this.products = [];   // reset to an empty array then go get the new data from the API

    // Pagination
    if (this.products.length > 0 && useCache === true) {
      const pagesReceived = Math.ceil(this.products.length / this.shopParams.pageSize);

      if (this.shopParams.pageNumber <= pagesReceived) {
        this.pagination.data =
          this.products.slice((this.shopParams.pageNumber - 1) * this.shopParams.pageSize, this.shopParams.pageNumber * this.shopParams.pageSize);

        return of(this.pagination);
      }
    }

    let params = new HttpParams();

    if (this.shopParams.brandIdSelected !== 0) {   // nu exista brandId = 0 pe server
      params = params.append('brandId', this.shopParams.brandIdSelected.toString());
    }

    if (this.shopParams.typeIdSelected !== 0) {
      params = params.append('typeId', this.shopParams.typeIdSelected.toString());
    }

    if (this.shopParams.search) {
      params = params.append('search', this.shopParams.search);
    }
 
    params = params.append('sort', this.shopParams.sortSelected);  // am initializat sortSelected = name, by default 

    params = params.append('pageIndex', this.shopParams.pageNumber.toString());
    params = params.append('pageSize', this.shopParams.pageSize.toString());

    return this.http.get<IPagination>(this.baseUrl + 'products', { observe: 'response', params })
      .pipe(    // wrapper to chain multiple rxjs operators like map to manipulate observable
        map(response => {
          // Caching => Store the products in the service and get them from there
          // ... => append the new set of body response from API to our already existing data stored in this.products
          this.products = [...this.products, ...response.body.data];
          this.pagination = response.body;

          return this.pagination;   // response returned as an IPagination format
        }));
  }




  setShopParams(params: ShopParams) {
    this.shopParams = params;
  }


  getShopParams() {
    return this.shopParams;
  }




  getProduct(id: number) {
    const product = this.products.find(p => p.id === id);

    if (product) {
      return of(product);   // observable of product
    }
    return this.http.get<IProduct>(this.baseUrl + "products/" + id);
  }



  getBrands() {
    // caching
    if (this.brands.length > 0) {       // if we have them, return them from our service rather than from API
      return of(this.brands);
    }
    return this.http.get<IBrand[]>(this.baseUrl + 'products/brands').pipe(
      map(response => {             // otherwise populate brands with pipe response
        this.brands = response;
        return response;
      })
    );
  }


  getTypes() {
    // caching
    if (this.types.length > 0) {
      return of(this.types);
    }
    return this.http.get<IType[]>(this.baseUrl + 'products/types').pipe(
      map(response => {
        this.types = response;
        return response;
      })
    );
  }



  addFavourite(productId: number) {
    return this.http.post(this.baseUrl + 'favourite/' + productId, {});
  }





  getUserFavourites(predicate: string, pageNumber: number, pageSize: number) {
    let params = getPaginationHeaders(pageNumber, pageSize);
    params = params.append('predicate', predicate);

    return getPaginatedResult<IProduct[]>(this.baseUrl + 'favourite', params, this.http);
  }

}
