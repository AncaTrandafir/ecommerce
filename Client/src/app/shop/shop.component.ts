import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { IProduct } from '../shared/Models/product';
import { ShopService } from './shop.service';
import { IBrand } from '../shared/models/brand';
import { IType } from '../shared/models/productType';
import { ShopParams } from '../shared/models/shopParams';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss']
})
export class ShopComponent implements OnInit {

  // input field of search is a child component of shop component; it is referred in html with #
  @ViewChild('search', {static: false}) searchTerm: ElementRef;    

  products: IProduct[];
  brands: IBrand[];
  types: IType[];

  shopParams: ShopParams;
  totalCount: number;

  sortOptions = [
    { name: 'Alphabetical', value: 'name' },
    { name: 'Price: Low to High', value: 'priceAsc' }, //priceAsc sent as query param to the API
    { name: 'Price: High to Low', value: 'priceDesc' }
  ];



  constructor(private shopService: ShopService) {
    this.shopParams = this.shopService.getShopParams();
  }



  ngOnInit() {

    this.getProducts(true);     // get products from the cache
    this.getBrands();
    this.getTypes();

  }


  getProducts(useCache = false) {
    this.shopService.getProducts(useCache).subscribe(response => {
      this.products = response.data;
      this.totalCount = response.count;
    }, error => {
      console.log(error);
    });

  }


  getBrands() {
    this.shopService.getBrands().subscribe(response => {
      this.brands = [{ id: 0, name: 'All' }, ...response]; 
    }, error => {
      console.log(error);
    });

  }



  getTypes() {
    this.shopService.getTypes().subscribe(response => {
      // ...response = spread operator and it adds another object at the array,
      // so we will have another type object: id = 0, name = all
      this.types = [{ id: 0, name: 'All' }, ...response];        
    }, error => {
      console.log(error);
    });

  }


 
  onBrandSelected(brandId: number) {
    const params = this.shopService.getShopParams();   // Set params in our service, not locally
    params.brandIdSelected = brandId;
    params.pageNumber = 1;   // when filtering, page should start with 1.
    this.shopService.setShopParams(params);
    this.getProducts();
  }


  onTypeSelected(typeId: number) {
    const params = this.shopService.getShopParams();
    params.typeIdSelected = typeId;
    params.pageNumber = 1;
    this.shopService.setShopParams(params);
    console.log(params);
    this.getProducts();
  }


  onSortSelected(sort: string) {
    const params = this.shopService.getShopParams();
    params.sortSelected = sort;
    this.shopService.setShopParams(params);
    this.getProducts();
  }


  onPageChanged(event: any) {
    // if needed to repair a bug...
    const params = this.shopService.getShopParams();
    if (params.pageNumber !== event) {
      params.pageNumber = event;    // we're passing the pageNumber from pager component because  the event pageChanged() emits a number     
      this.shopService.setShopParams(params);
      this.getProducts(true);       // get products from cache
    }
  }


  onSearch() {
    const params = this.shopService.getShopParams();
    params.search = this.searchTerm.nativeElement.value;
    params.pageNumber = 1;
    this.shopService.setShopParams(params);
    this.getProducts();
  }


  onReset() {
    this.searchTerm.nativeElement.value = '';
    this.shopParams = new ShopParams();     // will initialize locally all params with default values
    this.shopService.setShopParams(this.shopParams);
    this.getProducts();   // get the unfiltered products
  }

}
