import { Component, OnInit } from '@angular/core';
import { IProduct } from './shared/Models/product';
import { BasketService } from './basket/basket.service';
import { AccountService } from './account/account.service';
import { google } from '@google/maps';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {
  title = 'AncaTrandafir ECommerce App';

  constructor(private basketService: BasketService,
              private accountService: AccountService) {

  }



  ngOnInit() {
    this.loadBasket();
    this.loadCurrentUser();
  }


  loadBasket() {
    // initialize the basket from local storage each time we refresh
    const basketId = localStorage.getItem('basket_id');
    if (basketId) {
      this.basketService.getBasket(basketId).subscribe(() => {
        console.log('initialized basket');
      }, error => {
        console.log(error);
      });
    }
  }


  loadCurrentUser() {
    const token = localStorage.getItem('token');
      this.accountService.loadCurrentUser(token).subscribe((response) => {
        console.log('loaded user');
      }, error => {
        console.log(error);
      });
  }

}
