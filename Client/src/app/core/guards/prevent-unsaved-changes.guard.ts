import { Injectable } from '@angular/core';
import { CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { ConfirmService } from '../services/confirm.service';
import { Observable } from 'rxjs';
import { EditProductComponent } from '../../admin/edit-product/edit-product.component';
import { EditProductFormComponent } from '../../admin/edit-product-form/edit-product-form.component';

@Injectable({
  providedIn: 'root'
})
export class PreventUnsavedChangesGuard implements CanDeactivate<unknown> {

  constructor(private confirmService: ConfirmService) { }

  // JavaScript default confirmation dialogue
  //canDeactivate(component: EditProductComponent): boolean {
  //  if (component.form.dirty) {
  //    return confirm('Are you sure you want to continue? Any unsaved changes will be lost.');
  //  }
  //  return true;
  //}


  canDeactivate(component: EditProductComponent): Observable<boolean> | boolean {
    console.log(component.form);
    if (component.form.dirty) {
      return this.confirmService.confirm();
    }
    return true;
  }
}
