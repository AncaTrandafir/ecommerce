import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Injectable } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';


@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

  constructor(private router: Router,
              private toastr: ToastrService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // req = outgoing request, next = response back from API

    return next.handle(req).pipe(

      catchError(error => {
        if (error) {    // check status in HttpErrorResponse

          if (error.status === 400) {
            if (error.error.errors) {
              const modalStateErrors = [];
              for (const key in error.error.errors) {
                if (error.error.errors[key]) {
                  modalStateErrors.push(error.error.errors[key])
                }
              }
             // throw modalStateErrors.flat();
            } else if (typeof (error.error) === 'object') {
              this.toastr.error(error.statusText, error.status);
            } else {
              this.toastr.error(error.error, error.status);
            }
          }

          if (error.status === 401) {
            this.toastr.error(error.error.message, error.error.statusCode)
          }

          if (error.status === 404) {
            this.router.navigateByUrl('/not-found');
          }

          if (error.status === 500) {
            const navigationExtras: NavigationExtras = { state: { error: error.error } };
            this.router.navigateByUrl('/server-error', navigationExtras);
          }
        }

        return throwError(error);
      })
    );
  }
}
