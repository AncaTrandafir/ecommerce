import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { AdminPanelComponent } from './admin-panel/admin-panel.component';
import { PreventUnsavedChangesGuard } from '../core/guards/prevent-unsaved-changes.guard';
import { EditProductFormComponent } from './edit-product-form/edit-product-form.component';
import { EditProductComponent } from './edit-product/edit-product.component';

const routes: Routes = [
  { path: '', component: AdminPanelComponent },
  { path: 'create', component: EditProductComponent, data: { breadcrumb: 'Create' } },    // canDeactivate: [PreventUnsavedChangesGuard],
  { path: 'edit/:id', component: EditProductComponent, data: { breadcrumb: 'Edit' } }
];


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
