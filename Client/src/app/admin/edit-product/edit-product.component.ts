import { Component, OnInit, ViewChild, HostListener, AfterViewInit } from '@angular/core';
import { ProductFormValues, IProduct } from '../../shared/models/product';
import { IBrand } from '../../shared/models/brand';
import { IType } from '../../shared/models/productType';
import { ShopService } from '../../shop/shop.service';
import { ActivatedRoute, Router } from '@angular/router';
import { forkJoin } from 'rxjs';
import { NgForm } from '@angular/forms';


@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.scss']
})
export class EditProductComponent implements OnInit {//AfterViewInit{
  product: IProduct;
  productFormValues: ProductFormValues;
  brands: IBrand[];
  types: IType[];

   form: NgForm;   // from child component edit-product-form
 // @ViewChild('productForm', { static: true }) form: NgForm;

  @HostListener('window:beforeunload', ['$event']) unloadNotification($event: any) {
    if (this.form.dirty) {
      $event.returnValue = true;
    }
  }


  constructor(private shopService: ShopService,
    private route: ActivatedRoute) {

    this.productFormValues = new ProductFormValues();
  }

  //ngAfterViewInit(): void {
     ngOnInit(): void {
    console.log(this.form)

    const brands = this.getBrands();
    const types = this.getTypes();

    forkJoin([types, brands]).subscribe(results => {
      this.types = results[0];
      this.brands = results[1];
    }, error => {
      console.log(error);
    }, () => {
      if (this.route.snapshot.url[0].path === 'edit') {
        this.loadProduct();
      }
    });
  }

 

  loadProduct() {
    this.shopService.getProduct(+this.route.snapshot.paramMap.get('id')).
      subscribe((response: any) => {
        const productBrandId = this.brands && this.brands.find(x => x.name === response.productBrand).id;
        const productTypeId = this.types && this.types.find(x => x.name === response.productType).id;
        this.product = response;
        this.productFormValues = { ...response, productBrandId, productTypeId };
    });
  }


  getBrands() {
    return this.shopService.getBrands();
  }


  getTypes() {
    return this.shopService.getTypes();
  }


}
