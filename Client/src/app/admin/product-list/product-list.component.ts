import { Component, OnInit } from '@angular/core';
import { IProduct } from '../../shared/Models/product';
import { ShopService } from '../../shop/shop.service';
import { AdminService } from '../admin.service';
import { ShopParams } from '../../shared/models/shopParams';
import { ConfirmService } from '../../core/services/confirm.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {

  products: IProduct[];
  totalCount: number;
  shopParams: ShopParams;

  constructor(private shopService: ShopService,
    private adminService: AdminService,
    private confirmService: ConfirmService) {
    this.shopParams = this.shopService.getShopParams();
  }

  ngOnInit(): void {
    this.getProducts();
  }


  getProducts(useCache = false) {
    this.shopService.getProducts(useCache).subscribe(response => {
      this.products = response.data;
      this.totalCount = response.count;
    }, error => {
      console.log(error);
    });
  }


  onPageChanged(event: any) {
    const params = this.shopService.getShopParams();

    if (params.pageNumber !== event) {
      params.pageNumber = event;
      this.shopService.setShopParams(params);
      this.getProducts(true);
    }
  }


  deleteProduct(id: number) {
    if (this.confirmService)
    this.confirmService.confirm('Confirm delete product', 'This cannot be undone.').subscribe(result => {
      if (result)
        this.adminService.deleteProduct(id).subscribe((response: any) => {
          this.products.splice(this.products.findIndex(p => p.id === id), 1);
          this.totalCount--;
    })
    });
  }
}
