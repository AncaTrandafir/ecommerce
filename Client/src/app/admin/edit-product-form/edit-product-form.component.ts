import { Component, OnInit, Input, ViewChild, HostListener, AfterViewInit, Output, OnChanges } from '@angular/core';
import { ProductFormValues } from '../../shared/models/product';
import { IBrand } from '../../shared/models/brand';
import { IType } from '../../shared/models/productType';
import { ActivatedRoute, Router } from '@angular/router';
import { AdminService } from '../admin.service';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { EventEmitter } from '@angular/core'

@Component({
  selector: 'app-edit-product-form',
  templateUrl: './edit-product-form.component.html',
  styleUrls: ['./edit-product-form.component.scss']
})
export class EditProductFormComponent implements OnChanges{//AfterViewInit {
  @Input() product: ProductFormValues;
  @Input() brands: IBrand[];
  @Input() types: IType[];

  @ViewChild('productForm', { static: true }) productForm: NgForm;
  @Output() public form = new EventEmitter();

  getForm() {
    this.form.emit(this.productForm);
  }
  // Access browser events:
  // Prevent user accidentally close window by prompting notification.

  @HostListener('window:beforeunload', ['$event']) unloadNotification($event: any) {
    if (this.productForm.dirty) {
      $event.returnValue = true;
    }
  }

  constructor(private route: ActivatedRoute,
    private adminService: AdminService,
    private router: Router,
    private toastrService: ToastrService) { }


  //ngAfterViewInit(): void {
  // // this.form = this.productForm;
  //  console.log("dfgdg"+this.productForm.form);
  //}

  ngOnChanges(): void {
    console.log("dfgdg" + this.productForm.form);
  }



  //ngOnInit(): void {
  //  //this.form.form = this.productForm.form;
  //  //console.log("dsfhsl" + this.productForm.form);
  //  this.form.emit(this.productForm);
  //}

  

  onSubmit(product: ProductFormValues) {
    if (this.route.snapshot.url[0].path === 'edit') {
      const updatedProduct = { ...this.product, ...product, price: +product.price };
      this.adminService.updateProduct(updatedProduct, +this.route.snapshot.paramMap.get('id')).subscribe((response: any) => {
        this.toastrService.success("Product updated succcessfully.");
        this.router.navigate(['/admin']);
      });
    } else {
      const newProduct = { ...product, price: +product.price };
      this.adminService.createProduct(newProduct).subscribe((response: any) => {
        this.toastrService.success("New product added succcessfully.");
        this.router.navigate(['/admin']);       
      });
    }
  }


  updatePrice(event: any) {
    this.product.price = event;
  }
}
