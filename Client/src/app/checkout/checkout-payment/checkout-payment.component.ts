import { Component, OnInit, Input, AfterViewInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { BasketService } from '../../basket/basket.service';
import { CheckoutService } from '../checkout.service';
import { ToastrService } from 'ngx-toastr';
import { IBasket } from '../../shared/models/basket';
import { IOrder } from '../../shared/models/order';
import { error } from '@angular/compiler/src/util';
import { Router, NavigationExtras } from '@angular/router';

declare var Stripe;

@Component({
  selector: 'app-checkout-payment',
  templateUrl: './checkout-payment.component.html',
  styleUrls: ['./checkout-payment.component.scss']
})
export class CheckoutPaymentComponent implements AfterViewInit, OnDestroy {

  @Input() checkoutForm: FormGroup;
  @ViewChild('cardNumber', { static: true }) cardNumberElement: ElementRef;
  @ViewChild('cardExpiry', { static: true }) cardExpiryElement: ElementRef;
  @ViewChild('cardCvc', { static: true }) cardCvcElement: ElementRef;

  stripe: any;    // to get access to javascript functionality
  cardNumber: any;      // type any because we use javascript and not typescript
  cardExpiry: any;
  cardCvc: any;
  cardErrors: any;

  // flags for Stripe validation with Javascript to disable the Pay btn until they are set to true
  cardNumberValid = false;
  cardExpiryValid = false;
  cardCvcValid = false;


  cardHandler = this.onChange.bind(this);
  loading = false;


  constructor(private basketService: BasketService,
              private checkoutService: CheckoutService,
              private toastr: ToastrService,
              private router: Router) { }



  ngAfterViewInit() {
    // publishable key from Stripe -> developers
    this.stripe = Stripe('pk_test_51HdsvOIwaPYHIxmeYXPs4nFexscBkSPYYB5ZunX84k1lXVRhkuHddvvVZrIKbtu3Jn3IKqqrvl2OyGk2S3AMKfqC00yZxWM2IV');
    const elements = this.stripe.elements();

    this.cardNumber = elements.create('cardNumber');
    this.cardNumber.mount(this.cardNumberElement.nativeElement);
    this.cardNumber.addEventListener('change', this.cardHandler);   // on change event call cardHandler which calls errors from Stripe, if any

    this.cardExpiry = elements.create('cardExpiry');
    this.cardExpiry.mount(this.cardExpiryElement.nativeElement);
    this.cardExpiry.addEventListener('change', this.cardHandler); 

    this.cardCvc = elements.create('cardCvc');
    this.cardCvc.mount(this.cardCvcElement.nativeElement);
    this.cardCvc.addEventListener('change', this.cardHandler); 
  }



  ngOnDestroy() {
    this.cardNumber.destroy();
    this.cardExpiry.destroy();
    this.cardCvc.destroy();
  }



  async submitOrder() {
    this.loading = true;
    const basket = this.basketService.getCurrentBasketValue();
    try {
      const createdOrder = await this.createOrder(basket);
      const paymentResult = await this.confirmPaymentWithStripe(basket);

      if (paymentResult.paymentIntent) {
        // after confirmation form Stripe that payment succeeded delete basket from Redis server
        this.basketService.deleteBasket(basket);
        const navigationExtras: NavigationExtras = { state: createdOrder };
        this.router.navigate(['checkout/success'], navigationExtras);
      } else {
        this.toastr.error(paymentResult.error.message);
      }
      this.loading = false;
    } catch (error) {
      console.log(error);
      this.loading = false;
    }
  }


  private getOrderToCreate(basket: IBasket) {
    return {
      basketId: basket.id,
      deliveryMethodId: +this.checkoutForm.get('deliveryForm').get('deliveryMethod').value,     // + -> convert to number
      shipToAddress: this.checkoutForm.get('addressForm').value
    };
  }



  onChange(event) {     // direct access to the object.error through event, using Javascript
    console.log(event);       // want to target elementType and complete flag
    if (event.error) {
      this.cardErrors = event.error.message;  // errors coming from Stripe
    } else {
      this.cardErrors = null;
    }

    switch (event.elementType) {
      case 'cardNumber':
        this.cardNumberValid = event.complete;    // when complete flag is true cardNumberValid set to true
        break;
      case 'cardExpiry':
        this.cardExpiryValid = event.complete;    
        break;
      case 'cardCvc':
        this.cardCvcValid = event.complete;    
    }
  }



  private async createOrder(basket: IBasket) {
    const orderToCreate = this.getOrderToCreate(basket);
    return this.checkoutService.createOrder(orderToCreate).toPromise();
  }


  private async confirmPaymentWithStripe(basket) {
    return this.stripe.confirmCardPayment(basket.clientSecret, {
      payment_method: {
        card: this.cardNumber,
        billing_details: {
          name: this.checkoutForm.get('paymentForm').get('nameOnCard').value
        }
      }
    });
  }
}
