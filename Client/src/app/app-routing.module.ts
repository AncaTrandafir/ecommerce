import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { TestErrorComponent } from './core/test-error/test-error.component';
import { NotFoundComponent } from './core/not-found/not-found.component';
import { ServerErrorComponent } from './core/server-error/server-error.component';
import { AuthGuard } from './core/guards/auth.guard';
import { AdminPanelComponent } from './admin/admin-panel/admin-panel.component';
import { AdminGuard } from './core/guards/admin.guard';
import { PreventUnsavedChangesGuard } from './core/guards/prevent-unsaved-changes.guard';
import { FavouriteComponent } from './shop/favourite/favourite.component';
import { AboutUsComponent } from './about-us/about-us.component';


const routes: Routes = [
  { path: '', component: HomeComponent, data: { breadcrumb: 'Home' } },
  {
    path: 'shop', loadChildren: () => import('./shop/shop.module').then(mod => mod.ShopModule),   // lazy-loading: module only loaded when path accessed
    data: { breadcrumb: 'Shop' }
  },
  {
    path: 'account', loadChildren: () => import('./account/account.module').then(mod => mod.AccountModule),
    data: {
      breadcrumb: { skip: true }
    }   // no breadcrumb for account
  },
  { path: 'test-error', component: TestErrorComponent, data: {breadcrumb: 'Test Errors'} },
  { path: 'server-error', component: ServerErrorComponent, data: { breadcrumb: 'Server Error' } },
  { path: 'not-found', component: NotFoundComponent, data: { breadcrumb: 'Not Found' } },
  { path: 'about-us', component: AboutUsComponent, data: { breadcrumb: 'About Us' } },

 // { path: 'shop', component: ShopComponent },
 // { path: 'shop/:id', component: ProductDetailsComponent },

  {
    path: '',
    runGuardsAndResolvers: 'always',
    canActivate: [AuthGuard],
    children: [
      {
        path: 'admin',
        loadChildren: () => import('./admin/admin.module')
          .then(mod => mod.AdminModule), data: { breadcrumb: 'Admin - Moderator' },
        canActivate: [AdminGuard]
      },
      {
        path: 'basket', loadChildren: () => import('./basket/basket.module').then(mod => mod.BasketModule),
        data: { breadcrumb: 'Basket' }
      },
      {
          path: 'checkout',

        loadChildren: () => import('./checkout/checkout.module').then(mod => mod.CheckoutModule),
        data: { breadcrumb: 'Checkout' }
      },
      {
        path: 'orders',
        loadChildren: () => import('./orders/orders.module').then(mod => mod.OrdersModule),
        data: { breadcrumb: 'Orders' }
      },
      {
        path: 'favourites',
        component: FavouriteComponent,
        data: { breadcrumb: 'Favourites' }
      },
    ]
  },

  { path: '**', redirectTo: 'not-found', pathMatch: 'full' } // Not found; Need to specify full to prevent an endless loop
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
