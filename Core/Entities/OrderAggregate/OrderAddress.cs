﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entities.OrderAggregate
{
  //  [Keyless]
    public class OrderAddress       // not derived from BaseEntity so use a constructor
    {                          // <=> no need for id because Order owns the Address
        public OrderAddress()
        {
        }

        public OrderAddress(string firstName, string lastName, string street, string city, string state, string zipcode)
        {
            FirstName = firstName;
            LastName = lastName;
            Street = street;
            City = city;
            State = state;
            Zipcode = zipcode;
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zipcode { get; set; }
    }
}
