﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Core.Entities.OrderAggregate
{
    // Name, price etc might change but we want to store the actual ordered values, that's why the need for a different class

    public class ProductItemOrdered     // No need for id because it is owned by the Order itself
    {
        public ProductItemOrdered()
        {
        }

        public ProductItemOrdered(int productItemId, string productName, string photoUrl)
        {
            ProductItemId = productItemId;
            ProductName = productName;
            PhotoUrl = photoUrl;
        }

        public int ProductItemId { get; set; }
        public string ProductName { get; set; }
        public string PhotoUrl { get; set; }
    }
}
