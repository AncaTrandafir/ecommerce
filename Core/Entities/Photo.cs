﻿using Core.Entities.OrderAggregate;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Core.Entities
{
    [Table("Photos")]
    public class Photo : BaseEntity
    {
        public string PhotoUrl { get; set; }
        public string FileName { get; set; }
        public bool isMain { get; set; }
        public string PublicId { get; set; }
        public Product Product { get; set; }
        public int ProductId { get; set; }
   
    }
}
