﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entities
{
    public class CustomerBasket
    {
        public CustomerBasket()
        {
        }

        public CustomerBasket(string id)
        {
            Id = id;
        }

        public string Id { get; set; }
        public List<BasketItem> Items { get; set; } = new List<BasketItem>();
        public decimal ShippingPrice { get; set; }
        public int? DeliveryMethodId { get; set; }

        // for Stripe
        public string ClientSecret { get; set; }
        public string PaymentIntentId { get; set; }     // to update payment intent if client makes changes to basket, shipping option etc

    }
}
