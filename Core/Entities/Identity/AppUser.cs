﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace Core.Entities.Identity
{
    public class AppUser : IdentityUser <int>    // instead of deriving from BaseEntity, we derive from AspNetCore Identity
    {
        public string DisplayName { get; set; }
        public Address Address { get; set; }        // 1:1 relationship between AppUser and Address
        public ICollection<AppUserRole> UserRoles { get; set; }
        public ICollection<Favourite> FavouriteProducts { get; set; }
    }
}
