﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entities.Identity
{
    public class AppUserRole : IdentityUserRole<int>         // acts as a join table
    {
        public AppUser User { get; set; }
        public AppRole Role { get; set; }
    }
}
