﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Entities
{
    public class Product : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }

        private readonly List<Photo> _photos = new List<Photo>();
        public IReadOnlyList<Photo> Photos => _photos.AsReadOnly();      // one to many relationship
        public ProductType ProductType { get; set; }
        public int ProductTypeId {get; set; }
        public ProductBrand ProductBrand { get; set; }
        public int ProductBrandId { get; set; }
        public ICollection<Favourite> FavouredByUsers { get; set; }


        public void AddPhoto(string pictureUrl, string fileName, bool isMain = false)
        {
            var photo = new Photo
            {
                FileName = fileName,
                PhotoUrl = pictureUrl
            };

            // Check if product already has a photo, if not it'll be main.
            if (_photos.Count == 0) photo.isMain = true;

            _photos.Add(photo);
        }


        public void RemovePhoto(int id)
        {
            var photo = _photos.Find(x => x.Id == id);
            _photos.Remove(photo);
        }


        public void SetMainPhoto(int id)
        {
            var currentMain = _photos.SingleOrDefault(item => item.isMain);
            foreach (var item in _photos.Where(item => item.isMain))
            {
                item.isMain = false;
            }

            var photo = _photos.Find(x => x.Id == id);
            if (photo != null)
            {
                photo.isMain = true;
                if (currentMain != null) currentMain.isMain = false;
            }
        }
    }

}
