﻿using Core.Entities.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entities
{
    public class Favourite
    {
        public AppUser SourceUser { get; set; }
        public int SourceUserId { get; set; }
        public Product FavouriteProduct { get; set; }
        public int FavouriteProductId { get; set; }
    }
}
