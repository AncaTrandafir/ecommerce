﻿using Core.Entities;
using Core.Entities.Identity;
using Core.Specifications;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    public interface IFavouriteRepository
    {
        Task<Favourite> GetUserFavourite(int sourceUserId, int favouredProductId);
        Task<AppUser> GetUserWithFavourites(int userId);
     //   Task<Product> GetFavouredProduct(int productId);
        Task<PagedList<FavouriteDto>> GetUserFavourites(FavouriteParams likesParams);
     //   void SetUserFavourite(Favourite favourite);
    }
}
