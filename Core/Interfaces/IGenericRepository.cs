﻿using Core.Entities;
using Core.Specifications;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    public interface IGenericRepository<T> where T : BaseEntity         // only classes that derive from base entity can be used as substitute for T
    {

        Task<T> GetByIdAsync(int id);
        Task<IReadOnlyList<T>> ListAllAsync();
        Task<T> GetEntityWithSpec(ISpecification<T> spec);
        Task<IReadOnlyList<T>> ListAsync(ISpecification<T> spec);
        Task<int> CountAsync(ISpecification<T> spec);

        // Add, Update, Delete are not asynchronous methods because we're not adding directly to DB,
        // rather telling EntityFramework to track them and it happens inMemory, not in SQL 
        // => Unit of Work
        void Add(T entity);
        void Update(T entity);
        void Delete(T entity);

    }
}
