﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Core.Entities;

namespace Core.Interfaces
{
    public interface IPhotoService
    {
        Task<Photo> SaveToDiskAsync(IFormFile photo);
        void DeleteFromDisk(Photo photo);
    }
}
