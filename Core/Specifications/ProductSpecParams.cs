﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Specifications
{
    public class ProductSpecParams : PaginationParams
    {
        public int? BrandId { get; set; }
        public int? TypeId { get; set; }

        public string Sort { get; set; }

        private string _search;
        public string Search {
            get => _search;
            set => _search = value.ToLower();       // lowerCase
        }


    }
}
