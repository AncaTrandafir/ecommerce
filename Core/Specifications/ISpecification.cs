﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Core.Specifications
{
    public interface ISpecification<T>
    {

        Expression<Func<T, bool>> Criteria { get; }             // where clause of the query; returns a bool
        List<Expression<Func<T, object>>> Includes { get; }     // for the includes; returns an object
        Expression<Func<T, object>> OrderBy { get; }            // for the sorting and filtering
        Expression<Func<T, object>> OrderByDescending { get; }

        // for pagination
        int Take { get; }
        int Skip { get; }
        bool IsPagingEnabled { get; }
    }
}
