﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Specifications
{
    public class FavouriteParams : PaginationParams
    {
        public int UserId { get; set; }
        public int ProductId { get; set; }
        public string Predicate { get; set; }
    }
}
