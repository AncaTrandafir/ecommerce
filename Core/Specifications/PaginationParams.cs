﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Specifications
{
    public class PaginationParams
    {
        private const int MaxPageSize = 50;
        public int PageIndex { get; set; } = 1;     // by default we always return the first page
        private int _pageSize = 6;
        public int PageSize
        {
            get => _pageSize;
            set => _pageSize = (value > MaxPageSize) ? MaxPageSize : value;
            // Client can set pageSize; if greater than MaxPageSize return max, else value.
        }
    }
}
