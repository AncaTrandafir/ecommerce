﻿//using Core.Entities;
//using Core.Entities.Identity;
//using Microsoft.AspNetCore.Identity;
//using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
//using Microsoft.EntityFrameworkCore;

//namespace Infrastructure.Identity
//{
//    public class AppIdentityDbContext : IdentityDbContext<AppUser, AppRole, int,
//        IdentityUserClaim<int>, AppUserRole, IdentityUserLogin<int>, IdentityRoleClaim<int>, IdentityUserToken<int>>
//    { 
//        public AppIdentityDbContext(DbContextOptions<AppIdentityDbContext> options) : base(options)
//        {
//        }

//        public DbSet<Favourite> Favourites { get; set; }

//        protected override void OnModelCreating(ModelBuilder builder)
//        {
//            base.OnModelCreating(builder);

//            builder.Entity<AppUser>()
//               .HasMany(ur => ur.UserRoles)
//               .WithOne(u => u.User)
//               .HasForeignKey(ur => ur.UserId)
//               .IsRequired();

//            //builder.Entity<Address>()
//            //   .HasOne(ur => ur.AppUser)
//            //   .WithMany()
//            //   .HasForeignKey(ur => ur.AppUserId);

//            builder.Entity<AppRole>()
//               .HasMany(ur => ur.UserRoles)
//               .WithOne(u => u.Role)
//               .HasForeignKey(ur => ur.RoleId)
//               .IsRequired();


//            builder.Entity<Favourite>()
//              .HasKey(k => new { k.SourceUserId, k.FavouriteProductId });   // manually configure primary key

//            builder.Entity<Favourite>()
//                .HasOne(s => s.SourceUser)
//                .WithMany(l => l.FavouriteProducts)
//                .HasForeignKey(s => s.SourceUserId)
//                .OnDelete(DeleteBehavior.NoAction);

//            builder.Entity<Favourite>()
//               .HasOne(s => s.FavouriteProduct)
//               .WithMany(l => l.FavouredByUsers)
//               .HasForeignKey(s => s.FavouriteProductId)
//               .OnDelete(DeleteBehavior.Cascade);
//        }
//    }
//}
