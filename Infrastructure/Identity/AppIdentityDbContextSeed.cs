﻿//using Core.Entities.Identity;
//using Microsoft.AspNetCore.Identity;
//using System.Threading.Tasks;
//using System.Linq;
//using System.Collections.Generic;

//namespace Infrastructure.Identity
//{
//    public class AppIdentityDbContextSeed
//    {
//        public static async Task SeedUsersAsync(UserManager<AppUser> userManager, RoleManager<AppRole> roleManager)
//        {
//            if (userManager.Users.Any()) return;

//            var user = new AppUser
//            {
//                DisplayName = "AncaTrandafir",
//                Email = "anca@gmail.com",
//                UserName = "anka",
//                Address = new Address
//                {
//                    FirstName = "Anca",
//                    LastName = "Trandafir",
//                    Street = "Calarasi 1",
//                    City = "Cluj Napoca",
//                    State = "Romania",
//                    Zipcode = "210450"
//                }
//            };

//            var roles = new List<AppRole>
//                {
//                    new AppRole{Name = "Member"},
//                    new AppRole{Name = "Admin"},
//                    new AppRole{Name = "Moderator"}
//                };

//            foreach (var role in roles)
//            {
//                await roleManager.CreateAsync(role);
//            }

//            await userManager.CreateAsync(user, "Pa$$w0rd");
//            await userManager.AddToRoleAsync(user, "Member");

//            var admin = new AppUser
//            {
//                UserName = "Admin",
//                DisplayName = "Admin",
//                Email = "admin@admin.com",
//            };

//            await userManager.CreateAsync(admin, "Pa$$w0rd");
//            await userManager.AddToRolesAsync(admin, new[] { "Admin", "Moderator" });

//            var moderator = new AppUser
//            {
//                UserName = "Moderator",
//                DisplayName = "Moderator",
//                Email = "moderator@moderator.com",
//            };

//            await userManager.CreateAsync(moderator, "Pa$$w0rd");
//            await userManager.AddToRolesAsync(admin, new[] { "Moderator" });
//        }
//    }
//}
