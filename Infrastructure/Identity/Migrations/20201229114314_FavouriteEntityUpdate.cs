﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Identity.Migrations
{
    public partial class FavouriteEntityUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Favourite_Product_FavouriteProductId",
                table: "Favourite");

            migrationBuilder.DropForeignKey(
                name: "FK_Favourite_AspNetUsers_SourceUserId",
                table: "Favourite");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Favourite",
                table: "Favourite");

            migrationBuilder.RenameTable(
                name: "Favourite",
                newName: "Favourites");

            migrationBuilder.RenameIndex(
                name: "IX_Favourite_FavouriteProductId",
                table: "Favourites",
                newName: "IX_Favourites_FavouriteProductId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Favourites",
                table: "Favourites",
                columns: new[] { "SourceUserId", "FavouriteProductId" });

            migrationBuilder.AddForeignKey(
                name: "FK_Favourites_Product_FavouriteProductId",
                table: "Favourites",
                column: "FavouriteProductId",
                principalTable: "Product",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Favourites_AspNetUsers_SourceUserId",
                table: "Favourites",
                column: "SourceUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Favourites_Product_FavouriteProductId",
                table: "Favourites");

            migrationBuilder.DropForeignKey(
                name: "FK_Favourites_AspNetUsers_SourceUserId",
                table: "Favourites");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Favourites",
                table: "Favourites");

            migrationBuilder.RenameTable(
                name: "Favourites",
                newName: "Favourite");

            migrationBuilder.RenameIndex(
                name: "IX_Favourites_FavouriteProductId",
                table: "Favourite",
                newName: "IX_Favourite_FavouriteProductId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Favourite",
                table: "Favourite",
                columns: new[] { "SourceUserId", "FavouriteProductId" });

            migrationBuilder.AddForeignKey(
                name: "FK_Favourite_Product_FavouriteProductId",
                table: "Favourite",
                column: "FavouriteProductId",
                principalTable: "Product",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Favourite_AspNetUsers_SourceUserId",
                table: "Favourite",
                column: "SourceUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id");
        }
    }
}
