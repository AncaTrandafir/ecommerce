﻿using Core.Entities;
using Core.Entities.Identity;
using Core.Interfaces;
using Core.Specifications;
using Infrastructure.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Data
{
    public class FavouriteRepository : IFavouriteRepository
    {
        private readonly StoreContext _context;
        private readonly IConfiguration _config;

        public FavouriteRepository(StoreContext context, IConfiguration config)
        {
            _context = context;
            _config = config;
        }


        //public async Task<Product> GetFavouredProduct(int productId)
        //{
        //    return await _context.Products
        //        .Include(x => x.FavouredByUsers)
        //        .FirstOrDefaultAsync(x => x.Id == productId);
        //}


        public async Task<AppUser> GetUserWithFavourites(int userId)
        {
            return await _context.Users
                .Include(x => x.FavouriteProducts)
                .FirstOrDefaultAsync(x => x.Id == userId);
        }


        public async Task<Favourite> GetUserFavourite(int sourceUserId, int favouredProductId)
        {
             return await _context.Favourites.FindAsync(sourceUserId, favouredProductId);
        }


        //public void SetUserFavourite(Favourite favourite)
        //{
        //     _context.Favourites.Add(favourite);
        //}



        public async Task<PagedList<FavouriteDto>> GetUserFavourites(FavouriteParams favouriteParams)
        {
            var favourites = _context.Favourites.AsQueryable();
            var products = _context.Products.AsQueryable();

            //if (favouriteParams.Predicate == "favourite")
            //{
            //    favourites = favourites.Where(fav => fav.SourceUserId == favouriteParams.UserId);
            //    products = favourites.Select(fav => fav.FavouriteProduct);
            //}

            if (favouriteParams.Predicate == "favouredBy")
            {
                favourites = favourites.Where(fav => fav.SourceUserId == favouriteParams.UserId);
                products = favourites.Select(fav => fav.FavouriteProduct);
            }

            var favouriteProducts = products.Select(product => new FavouriteDto
            {
               Name = product.Name,
               Price = product.Price,
               PhotoUrl = _config["ApiUrl"] + product.Photos.FirstOrDefault(p => p.isMain).PhotoUrl,     // Url resolver for photo
               Id = product.Id
            });

            return await PagedList<FavouriteDto>.CreateAsync(favouriteProducts, favouriteParams.PageIndex, favouriteParams.PageSize);
        }
    }
}
