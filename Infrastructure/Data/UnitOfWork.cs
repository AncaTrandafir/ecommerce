﻿using Core.Entities;
using Core.Interfaces;
using Pipelines.Sockets.Unofficial.Arenas;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly StoreContext _context;
        private Hashtable _repositories;

        public UnitOfWork(StoreContext context)
        {
            _context = context;
        }

        public async Task<int> Complete()
        {
            return await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public IGenericRepository<TEntity> Repository<TEntity>() where TEntity : BaseEntity
        {
            // check if we have already created another instance of another repository
            if (_repositories == null) _repositories = new Hashtable();

            // check the name of the TEntity that we pass as a parameter
            var type = typeof(TEntity).Name;

            // check if our hashtable already contains a repo with this particular type
            if (!_repositories.ContainsKey(type))       // if it doesnt't
            {
                var repositoryType = typeof(GenericRepository<>);       // we create a repoType of a genericRepository
                var repositoryInstance = Activator.CreateInstance(repositoryType.MakeGenericType(typeof(TEntity)), _context);
                // create an instance of this repository of type of TEntity and pass the dbContext that we gonna get from our UnitOfWork

                // add repository to the hashtable
                _repositories.Add(type, repositoryInstance);
            }

            // return it
            return (IGenericRepository<TEntity>)_repositories[type];
        }
    }
}
