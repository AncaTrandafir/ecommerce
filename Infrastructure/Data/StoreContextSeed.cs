﻿using Core.Entities;
using Core.Entities.Identity;
using Core.Entities.OrderAggregate;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Infrastructure.Data
{
    public class StoreContextSeed
    {

        public static async Task SeedAsync(StoreContext context, ILoggerFactory loggerFactory)
        {
            try
            {
                var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

                if (!context.ProductBrands.Any())
                {
                    var brandsPath = Path.Combine(path, @"/Data/SeedData/brands.json");
                    var brandsData = File.ReadAllText(brandsPath);
                    //var brandsData = File.ReadAllText("C:/Users/ancat/source/repos/ECommerceApp/Infrastructure/Data/SeedData/brands.json");

                    var brands = JsonSerializer.Deserialize<List<ProductBrand>>(brandsData);

                    foreach (var item in brands)
                    {
                        context.ProductBrands.Add(item);
                    }

                    await context.SaveChangesAsync();

                }



                if (!context.ProductTypes.Any())
                {
                    var typesData = File.ReadAllText(path + @"/Data/SeedData/types.json");
                    //var typesData = File.ReadAllText("C:/Users/ancat/source/repos/ECommerceApp/Infrastructure/Data/SeedData/types.json");

                    var types = JsonSerializer.Deserialize<List<ProductType>>(typesData);

                    foreach (var item in types)
                    {
                        context.ProductTypes.Add(item);
                    }

                    await context.SaveChangesAsync();

                }



                if (!context.Products.Any())
                {
                    var productsData = File.ReadAllText(path + @"/Data/SeedData/products.json");
                    //var productsData = File.ReadAllText("C:/Users/ancat/source/repos/ECommerceApp/Infrastructure/Data/SeedData/products.json");

                    var products = JsonSerializer.Deserialize<List<ProductSeedModel>>(productsData);

                    foreach (var item in products)
                    {
                        var pictureFileName = item.PhotoUrl.Substring(16);
                        var product = new Product
                        {
                            Name = item.Name,
                            Description = item.Description,
                            Price = item.Price,
                            ProductBrandId = item.ProductBrandId,
                            ProductTypeId = item.ProductTypeId
                        };
                        product.AddPhoto(item.PhotoUrl, pictureFileName);
                        context.Products.Add(product);
                    }

                    await context.SaveChangesAsync();

                }



                if (!context.DeliveryMethods.Any())
                {
                    var dmData = File.ReadAllText(path + @"/Data/SeedData/delivery.json");
                    //var dmData = File.ReadAllText("C:/Users/ancat/source/repos/ECommerceApp/Infrastructure/Data/SeedData/delivery.json");

                    var methods = JsonSerializer.Deserialize<List<DeliveryMethod>>(dmData);

                    foreach (var item in methods)
                    {
                        context.DeliveryMethods.Add(item);
                    }

                    await context.SaveChangesAsync();
                }
            }

            catch (Exception ex)
            {
                var logger = loggerFactory.CreateLogger<StoreContextSeed>();
                logger.LogError(ex.Message);
            }
        }



        public static async Task SeedUsersAsync(UserManager<AppUser> userManager, RoleManager<AppRole> roleManager, ILoggerFactory loggerFactory)
        {
            try
            {
                if (userManager.Users.Any()) return;

                var user = new AppUser
                {
                    DisplayName = "AncaTrandafir",
                    Email = "anca@gmail.com",
                    UserName = "anka",
                    Address = new Address
                    {
                        FirstName = "Anca",
                        LastName = "Trandafir",
                        Street = "Calarasi 1",
                        City = "Cluj Napoca",
                        State = "Romania",
                        Zipcode = "210450"
                    },
                    EmailConfirmed = true
                };

                var roles = new List<AppRole>
                    {
                        new AppRole{Name = "Member"},
                        new AppRole{Name = "Admin"},
                        new AppRole{Name = "Moderator"}
                    };

                foreach (var role in roles)
                {
                    await roleManager.CreateAsync(role);
                }

                await userManager.CreateAsync(user, "Pa$$w0rd");
                var token = await userManager.GenerateEmailConfirmationTokenAsync(user);
                await userManager.ConfirmEmailAsync(user, token);
                await userManager.AddToRoleAsync(user, "Member");

                var admin = new AppUser
                {
                    UserName = "Admin",
                    DisplayName = "Admin",
                    Email = "admin@admin.com",
                    EmailConfirmed = true
                };

                // Create Admin User and confirm email flag to true
                await userManager.CreateAsync(admin, "Pa$$w0rd");
                var tokenAdmin = await userManager.GenerateEmailConfirmationTokenAsync(admin);
                await userManager.ConfirmEmailAsync(admin, tokenAdmin);
                await userManager.AddToRolesAsync(admin, new[] { "Admin", "Moderator" });

                var moderator = new AppUser
                {
                    UserName = "Moderator",
                    DisplayName = "Moderator",
                    Email = "moderator@moderator.com",
                    EmailConfirmed = true
                };

                await userManager.CreateAsync(moderator, "Pa$$w0rd");
                var tokenModerator = await userManager.GenerateEmailConfirmationTokenAsync(moderator);
                await userManager.ConfirmEmailAsync(moderator, tokenModerator);
                await userManager.AddToRolesAsync(moderator, new[] { "Moderator" });


            }

            catch (Exception ex)
            {
                var logger = loggerFactory.CreateLogger<StoreContextSeed>();
                logger.LogError(ex.Message);
            }
        }

    }
}
