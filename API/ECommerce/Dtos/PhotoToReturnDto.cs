﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Dtos
{
    public class PhotoToReturnDto       // to avoid infinite cycle
    {
        public int Id { get; set; }
        public string PhotoUrl { get; set; }
        public string FileName { get; set; }
        public bool IsMain { get; set; }
    }
}
