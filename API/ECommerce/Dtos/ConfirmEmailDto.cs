﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Dtos
{
    public class ConfirmEmailDto
    {
        public string userid { get; set; }
        public string token { get; set; }
    }
}
