﻿using Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Dtos
{
    public class ProductToReturnDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public string PhotoUrl { get; set; }       // for main photo
        public IEnumerable<PhotoToReturnDto> Photos { get; set; }       // photoDto to avoid infinite cycle
        public string ProductType { get; set; }         // to get ProductType flattened and not as an object
        public string ProductBrand { get; set; }

        public IEnumerable<Favourite> FavouredByUsers { get; set; }

    }
}
