(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["shop-shop-module-ngfactory"],{

/***/ "./node_modules/@kolkov/ngx-gallery/kolkov-ngx-gallery.ngfactory.js":
/*!**************************************************************************!*\
  !*** ./node_modules/@kolkov/ngx-gallery/kolkov-ngx-gallery.ngfactory.js ***!
  \**************************************************************************/
/*! exports provided: NgxGalleryModuleNgFactory, RenderType_NgxGalleryComponent, View_NgxGalleryComponent_0, View_NgxGalleryComponent_Host_0, NgxGalleryComponentNgFactory, RenderType_NgxGalleryThumbnailsComponent, View_NgxGalleryThumbnailsComponent_0, View_NgxGalleryThumbnailsComponent_Host_0, NgxGalleryThumbnailsComponentNgFactory, RenderType_NgxGalleryPreviewComponent, View_NgxGalleryPreviewComponent_0, View_NgxGalleryPreviewComponent_Host_0, NgxGalleryPreviewComponentNgFactory, RenderType_NgxGalleryArrowsComponent, View_NgxGalleryArrowsComponent_0, View_NgxGalleryArrowsComponent_Host_0, NgxGalleryArrowsComponentNgFactory, RenderType_NgxGalleryBulletsComponent, View_NgxGalleryBulletsComponent_0, View_NgxGalleryBulletsComponent_Host_0, NgxGalleryBulletsComponentNgFactory, RenderType_ɵa, View_ɵa_0, View_ɵa_Host_0, ɵaNgFactory, RenderType_ɵb, View_ɵb_0, View_ɵb_Host_0, ɵbNgFactory */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgxGalleryModuleNgFactory", function() { return NgxGalleryModuleNgFactory; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RenderType_NgxGalleryComponent", function() { return RenderType_NgxGalleryComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "View_NgxGalleryComponent_0", function() { return View_NgxGalleryComponent_0; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "View_NgxGalleryComponent_Host_0", function() { return View_NgxGalleryComponent_Host_0; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgxGalleryComponentNgFactory", function() { return NgxGalleryComponentNgFactory; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RenderType_NgxGalleryThumbnailsComponent", function() { return RenderType_NgxGalleryThumbnailsComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "View_NgxGalleryThumbnailsComponent_0", function() { return View_NgxGalleryThumbnailsComponent_0; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "View_NgxGalleryThumbnailsComponent_Host_0", function() { return View_NgxGalleryThumbnailsComponent_Host_0; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgxGalleryThumbnailsComponentNgFactory", function() { return NgxGalleryThumbnailsComponentNgFactory; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RenderType_NgxGalleryPreviewComponent", function() { return RenderType_NgxGalleryPreviewComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "View_NgxGalleryPreviewComponent_0", function() { return View_NgxGalleryPreviewComponent_0; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "View_NgxGalleryPreviewComponent_Host_0", function() { return View_NgxGalleryPreviewComponent_Host_0; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgxGalleryPreviewComponentNgFactory", function() { return NgxGalleryPreviewComponentNgFactory; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RenderType_NgxGalleryArrowsComponent", function() { return RenderType_NgxGalleryArrowsComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "View_NgxGalleryArrowsComponent_0", function() { return View_NgxGalleryArrowsComponent_0; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "View_NgxGalleryArrowsComponent_Host_0", function() { return View_NgxGalleryArrowsComponent_Host_0; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgxGalleryArrowsComponentNgFactory", function() { return NgxGalleryArrowsComponentNgFactory; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RenderType_NgxGalleryBulletsComponent", function() { return RenderType_NgxGalleryBulletsComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "View_NgxGalleryBulletsComponent_0", function() { return View_NgxGalleryBulletsComponent_0; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "View_NgxGalleryBulletsComponent_Host_0", function() { return View_NgxGalleryBulletsComponent_Host_0; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgxGalleryBulletsComponentNgFactory", function() { return NgxGalleryBulletsComponentNgFactory; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RenderType_ɵa", function() { return RenderType_ɵa; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "View_ɵa_0", function() { return View_ɵa_0; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "View_ɵa_Host_0", function() { return View_ɵa_Host_0; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵaNgFactory", function() { return ɵaNgFactory; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RenderType_ɵb", function() { return RenderType_ɵb; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "View_ɵb_0", function() { return View_ɵb_0; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "View_ɵb_Host_0", function() { return View_ɵb_Host_0; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵbNgFactory", function() { return ɵbNgFactory; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @kolkov/ngx-gallery */ "./node_modules/@kolkov/ngx-gallery/fesm2015/kolkov-ngx-gallery.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes,extraRequire}
 * tslint:disable
 */ 




var NgxGalleryModuleNgFactory = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵcmf"](_kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_1__["NgxGalleryModule"], [], function (_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmod"]([_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](512, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ComponentFactoryResolver"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵCodegenComponentFactoryResolver"], [[8, []], [3, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ComponentFactoryResolver"]], _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModuleRef"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgLocalization"], _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgLocaleLocalization"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["LOCALE_ID"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["HAMMER_GESTURE_CONFIG"], _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_1__["CustomHammerConfig"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_1__["NgxGalleryModule"], _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_1__["NgxGalleryModule"], [])]); });

var styles_NgxGalleryComponent = [":host{display:inline-block}:host>*{float:left}.ngx-gallery-layout{width:100%;height:100%;display:flex;flex-direction:column}.ngx-gallery-layout.thumbnails-top ngx-gallery-image{order:2}.ngx-gallery-layout.thumbnails-bottom ngx-gallery-image,.ngx-gallery-layout.thumbnails-top ngx-gallery-thumbnails{order:1}.ngx-gallery-layout.thumbnails-bottom ngx-gallery-thumbnails{order:2}*{box-sizing:border-box}.ngx-gallery-icon{color:#fff;position:absolute;display:inline-block}.ngx-gallery-icon .ngx-gallery-icon-content{display:block}ngx-gallery-preview{font-size:25px}ngx-gallery-preview .ngx-gallery-icon{z-index:2000}.ngx-gallery-clickable{cursor:pointer}.ngx-gallery-icons-wrapper .ngx-gallery-icon{position:relative;margin-right:5px;margin-top:5px;font-size:20px;cursor:pointer}.ngx-gallery-icons-wrapper{float:right}"];
var RenderType_NgxGalleryComponent = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵcrt"]({ encapsulation: 2, styles: styles_NgxGalleryComponent, data: {} });

function View_NgxGalleryComponent_1(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](0, 0, null, null, 1, "ngx-gallery-image", [], [[4, "height", null]], [[null, "imageClick"], [null, "activeChange"], [null, "mouseenter"], [null, "mouseleave"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("mouseenter" === en)) {
        var pd_0 = (_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵnov"](_v, 1).onMouseEnter() !== false);
        ad = (pd_0 && ad);
    } if (("mouseleave" === en)) {
        var pd_1 = (_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵnov"](_v, 1).onMouseLeave() !== false);
        ad = (pd_1 && ad);
    } if (("imageClick" === en)) {
        var pd_2 = (_co.openPreview($event) !== false);
        ad = (pd_2 && ad);
    } if (("activeChange" === en)) {
        var pd_3 = (_co.selectFromImage($event) !== false);
        ad = (pd_3 && ad);
    } return ad; }, View_ɵa_0, RenderType_ɵa)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](1, 638976, [[2, 4]], 0, _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_1__["ɵa"], [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["DomSanitizer"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"], _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_1__["NgxGalleryService"]], { images: [0, "images"], clickable: [1, "clickable"], selectedIndex: [2, "selectedIndex"], arrows: [3, "arrows"], arrowsAutoHide: [4, "arrowsAutoHide"], swipe: [5, "swipe"], animation: [6, "animation"], size: [7, "size"], arrowPrevIcon: [8, "arrowPrevIcon"], arrowNextIcon: [9, "arrowNextIcon"], autoPlay: [10, "autoPlay"], autoPlayInterval: [11, "autoPlayInterval"], autoPlayPauseOnHover: [12, "autoPlayPauseOnHover"], infinityMove: [13, "infinityMove"], lazyLoading: [14, "lazyLoading"], actions: [15, "actions"], descriptions: [16, "descriptions"], showDescription: [17, "showDescription"], bullets: [18, "bullets"] }, { imageClick: "imageClick", activeChange: "activeChange" })], function (_ck, _v) { var _co = _v.component; var currVal_1 = _co.mediumImages; var currVal_2 = ((_co.currentOptions == null) ? null : _co.currentOptions.preview); var currVal_3 = _co.selectedIndex; var currVal_4 = ((_co.currentOptions == null) ? null : _co.currentOptions.imageArrows); var currVal_5 = ((_co.currentOptions == null) ? null : _co.currentOptions.imageArrowsAutoHide); var currVal_6 = ((_co.currentOptions == null) ? null : _co.currentOptions.imageSwipe); var currVal_7 = ((_co.currentOptions == null) ? null : _co.currentOptions.imageAnimation); var currVal_8 = ((_co.currentOptions == null) ? null : _co.currentOptions.imageSize); var currVal_9 = ((_co.currentOptions == null) ? null : _co.currentOptions.arrowPrevIcon); var currVal_10 = ((_co.currentOptions == null) ? null : _co.currentOptions.arrowNextIcon); var currVal_11 = ((_co.currentOptions == null) ? null : _co.currentOptions.imageAutoPlay); var currVal_12 = ((_co.currentOptions == null) ? null : _co.currentOptions.imageAutoPlayInterval); var currVal_13 = ((_co.currentOptions == null) ? null : _co.currentOptions.imageAutoPlayPauseOnHover); var currVal_14 = ((_co.currentOptions == null) ? null : _co.currentOptions.imageInfinityMove); var currVal_15 = ((_co.currentOptions == null) ? null : _co.currentOptions.lazyLoading); var currVal_16 = ((_co.currentOptions == null) ? null : _co.currentOptions.imageActions); var currVal_17 = _co.descriptions; var currVal_18 = ((_co.currentOptions == null) ? null : _co.currentOptions.imageDescription); var currVal_19 = ((_co.currentOptions == null) ? null : _co.currentOptions.imageBullets); _ck(_v, 1, 1, [currVal_1, currVal_2, currVal_3, currVal_4, currVal_5, currVal_6, currVal_7, currVal_8, currVal_9, currVal_10, currVal_11, currVal_12, currVal_13, currVal_14, currVal_15, currVal_16, currVal_17, currVal_18, currVal_19]); }, function (_ck, _v) { var _co = _v.component; var currVal_0 = _co.getImageHeight(); _ck(_v, 0, 0, currVal_0); }); }
function View_NgxGalleryComponent_2(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](0, 0, null, null, 2, "ngx-gallery-thumbnails", [], [[4, "marginTop", null], [4, "marginBottom", null], [4, "height", null]], [[null, "activeChange"], [null, "mouseenter"], [null, "mouseleave"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("mouseenter" === en)) {
        var pd_0 = (_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵnov"](_v, 2).onMouseEnter() !== false);
        ad = (pd_0 && ad);
    } if (("mouseleave" === en)) {
        var pd_1 = (_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵnov"](_v, 2).onMouseLeave() !== false);
        ad = (pd_1 && ad);
    } if (("activeChange" === en)) {
        var pd_2 = (_co.selectFromThumbnails($event) !== false);
        ad = (pd_2 && ad);
    } return ad; }, View_NgxGalleryThumbnailsComponent_0, RenderType_NgxGalleryThumbnailsComponent)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](1, 278528, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgClass"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["IterableDiffers"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["KeyValueDiffers"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"]], { ngClass: [0, "ngClass"] }, null), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](2, 638976, [[3, 4]], 0, _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_1__["NgxGalleryThumbnailsComponent"], [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["DomSanitizer"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"], _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_1__["NgxGalleryService"]], { images: [0, "images"], links: [1, "links"], labels: [2, "labels"], linkTarget: [3, "linkTarget"], columns: [4, "columns"], rows: [5, "rows"], arrows: [6, "arrows"], arrowsAutoHide: [7, "arrowsAutoHide"], margin: [8, "margin"], selectedIndex: [9, "selectedIndex"], clickable: [10, "clickable"], swipe: [11, "swipe"], size: [12, "size"], arrowPrevIcon: [13, "arrowPrevIcon"], arrowNextIcon: [14, "arrowNextIcon"], moveSize: [15, "moveSize"], order: [16, "order"], remainingCount: [17, "remainingCount"], lazyLoading: [18, "lazyLoading"], actions: [19, "actions"] }, { activeChange: "activeChange" })], function (_ck, _v) { var _co = _v.component; var currVal_3 = ((_co.currentOptions == null) ? null : _co.currentOptions.thumbnailClasses); _ck(_v, 1, 0, currVal_3); var currVal_4 = _co.smallImages; var currVal_5 = (((_co.currentOptions == null) ? null : _co.currentOptions.thumbnailsAsLinks) ? _co.links : _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵEMPTY_ARRAY"]); var currVal_6 = _co.labels; var currVal_7 = ((_co.currentOptions == null) ? null : _co.currentOptions.linkTarget); var currVal_8 = ((_co.currentOptions == null) ? null : _co.currentOptions.thumbnailsColumns); var currVal_9 = ((_co.currentOptions == null) ? null : _co.currentOptions.thumbnailsRows); var currVal_10 = ((_co.currentOptions == null) ? null : _co.currentOptions.thumbnailsArrows); var currVal_11 = ((_co.currentOptions == null) ? null : _co.currentOptions.thumbnailsArrowsAutoHide); var currVal_12 = ((_co.currentOptions == null) ? null : _co.currentOptions.thumbnailMargin); var currVal_13 = _co.selectedIndex; var currVal_14 = (((_co.currentOptions == null) ? null : _co.currentOptions.image) || ((_co.currentOptions == null) ? null : _co.currentOptions.preview)); var currVal_15 = ((_co.currentOptions == null) ? null : _co.currentOptions.thumbnailsSwipe); var currVal_16 = ((_co.currentOptions == null) ? null : _co.currentOptions.thumbnailSize); var currVal_17 = ((_co.currentOptions == null) ? null : _co.currentOptions.arrowPrevIcon); var currVal_18 = ((_co.currentOptions == null) ? null : _co.currentOptions.arrowNextIcon); var currVal_19 = ((_co.currentOptions == null) ? null : _co.currentOptions.thumbnailsMoveSize); var currVal_20 = ((_co.currentOptions == null) ? null : _co.currentOptions.thumbnailsOrder); var currVal_21 = ((_co.currentOptions == null) ? null : _co.currentOptions.thumbnailsRemainingCount); var currVal_22 = ((_co.currentOptions == null) ? null : _co.currentOptions.lazyLoading); var currVal_23 = ((_co.currentOptions == null) ? null : _co.currentOptions.thumbnailActions); _ck(_v, 2, 1, [currVal_4, currVal_5, currVal_6, currVal_7, currVal_8, currVal_9, currVal_10, currVal_11, currVal_12, currVal_13, currVal_14, currVal_15, currVal_16, currVal_17, currVal_18, currVal_19, currVal_20, currVal_21, currVal_22, currVal_23]); }, function (_ck, _v) { var _co = _v.component; var currVal_0 = _co.getThumbnailsMarginTop(); var currVal_1 = _co.getThumbnailsMarginBottom(); var currVal_2 = _co.getThumbnailsHeight(); _ck(_v, 0, 0, currVal_0, currVal_1, currVal_2); }); }
function View_NgxGalleryComponent_0(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵvid"](2, [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵqud"](671088640, 1, { preview: 0 }), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵqud"](671088640, 2, { image: 0 }), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵqud"](671088640, 3, { thumbnails: 0 }), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](3, 0, null, null, 6, "div", [], [[8, "className", 0]], null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵand"](16777216, null, null, 1, null, View_NgxGalleryComponent_1)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](5, 16384, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵand"](16777216, null, null, 1, null, View_NgxGalleryComponent_2)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](7, 16384, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](8, 0, null, null, 1, "ngx-gallery-preview", [], [[2, "ngx-gallery-active", null]], [[null, "previewClose"], [null, "previewOpen"], [null, "activeChange"], [null, "mouseenter"], [null, "mouseleave"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("mouseenter" === en)) {
        var pd_0 = (_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵnov"](_v, 9).onMouseEnter() !== false);
        ad = (pd_0 && ad);
    } if (("mouseleave" === en)) {
        var pd_1 = (_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵnov"](_v, 9).onMouseLeave() !== false);
        ad = (pd_1 && ad);
    } if (("previewClose" === en)) {
        var pd_2 = (_co.onPreviewClose() !== false);
        ad = (pd_2 && ad);
    } if (("previewOpen" === en)) {
        var pd_3 = (_co.onPreviewOpen() !== false);
        ad = (pd_3 && ad);
    } if (("activeChange" === en)) {
        var pd_4 = (_co.previewSelect($event) !== false);
        ad = (pd_4 && ad);
    } return ad; }, View_NgxGalleryPreviewComponent_0, RenderType_NgxGalleryPreviewComponent)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](9, 770048, [[1, 4]], 0, _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_1__["NgxGalleryPreviewComponent"], [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["DomSanitizer"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"], _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_1__["NgxGalleryService"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]], { images: [0, "images"], descriptions: [1, "descriptions"], showDescription: [2, "showDescription"], arrows: [3, "arrows"], arrowsAutoHide: [4, "arrowsAutoHide"], swipe: [5, "swipe"], fullscreen: [6, "fullscreen"], forceFullscreen: [7, "forceFullscreen"], closeOnClick: [8, "closeOnClick"], closeOnEsc: [9, "closeOnEsc"], keyboardNavigation: [10, "keyboardNavigation"], arrowPrevIcon: [11, "arrowPrevIcon"], arrowNextIcon: [12, "arrowNextIcon"], closeIcon: [13, "closeIcon"], fullscreenIcon: [14, "fullscreenIcon"], spinnerIcon: [15, "spinnerIcon"], autoPlay: [16, "autoPlay"], autoPlayInterval: [17, "autoPlayInterval"], autoPlayPauseOnHover: [18, "autoPlayPauseOnHover"], infinityMove: [19, "infinityMove"], zoom: [20, "zoom"], zoomStep: [21, "zoomStep"], zoomMax: [22, "zoomMax"], zoomMin: [23, "zoomMin"], zoomInIcon: [24, "zoomInIcon"], zoomOutIcon: [25, "zoomOutIcon"], animation: [26, "animation"], actions: [27, "actions"], rotate: [28, "rotate"], rotateLeftIcon: [29, "rotateLeftIcon"], rotateRightIcon: [30, "rotateRightIcon"], download: [31, "download"], downloadIcon: [32, "downloadIcon"], bullets: [33, "bullets"] }, { previewOpen: "previewOpen", previewClose: "previewClose", activeChange: "activeChange" })], function (_ck, _v) { var _co = _v.component; var currVal_1 = ((_co.currentOptions == null) ? null : _co.currentOptions.image); _ck(_v, 5, 0, currVal_1); var currVal_2 = ((_co.currentOptions == null) ? null : _co.currentOptions.thumbnails); _ck(_v, 7, 0, currVal_2); var currVal_4 = _co.bigImages; var currVal_5 = _co.descriptions; var currVal_6 = ((_co.currentOptions == null) ? null : _co.currentOptions.previewDescription); var currVal_7 = ((_co.currentOptions == null) ? null : _co.currentOptions.previewArrows); var currVal_8 = ((_co.currentOptions == null) ? null : _co.currentOptions.previewArrowsAutoHide); var currVal_9 = ((_co.currentOptions == null) ? null : _co.currentOptions.previewSwipe); var currVal_10 = ((_co.currentOptions == null) ? null : _co.currentOptions.previewFullscreen); var currVal_11 = ((_co.currentOptions == null) ? null : _co.currentOptions.previewForceFullscreen); var currVal_12 = ((_co.currentOptions == null) ? null : _co.currentOptions.previewCloseOnClick); var currVal_13 = ((_co.currentOptions == null) ? null : _co.currentOptions.previewCloseOnEsc); var currVal_14 = ((_co.currentOptions == null) ? null : _co.currentOptions.previewKeyboardNavigation); var currVal_15 = ((_co.currentOptions == null) ? null : _co.currentOptions.arrowPrevIcon); var currVal_16 = ((_co.currentOptions == null) ? null : _co.currentOptions.arrowNextIcon); var currVal_17 = ((_co.currentOptions == null) ? null : _co.currentOptions.closeIcon); var currVal_18 = ((_co.currentOptions == null) ? null : _co.currentOptions.fullscreenIcon); var currVal_19 = ((_co.currentOptions == null) ? null : _co.currentOptions.spinnerIcon); var currVal_20 = ((_co.currentOptions == null) ? null : _co.currentOptions.previewAutoPlay); var currVal_21 = ((_co.currentOptions == null) ? null : _co.currentOptions.previewAutoPlayInterval); var currVal_22 = ((_co.currentOptions == null) ? null : _co.currentOptions.previewAutoPlayPauseOnHover); var currVal_23 = ((_co.currentOptions == null) ? null : _co.currentOptions.previewInfinityMove); var currVal_24 = ((_co.currentOptions == null) ? null : _co.currentOptions.previewZoom); var currVal_25 = ((_co.currentOptions == null) ? null : _co.currentOptions.previewZoomStep); var currVal_26 = ((_co.currentOptions == null) ? null : _co.currentOptions.previewZoomMax); var currVal_27 = ((_co.currentOptions == null) ? null : _co.currentOptions.previewZoomMin); var currVal_28 = ((_co.currentOptions == null) ? null : _co.currentOptions.zoomInIcon); var currVal_29 = ((_co.currentOptions == null) ? null : _co.currentOptions.zoomOutIcon); var currVal_30 = ((_co.currentOptions == null) ? null : _co.currentOptions.previewAnimation); var currVal_31 = ((_co.currentOptions == null) ? null : _co.currentOptions.actions); var currVal_32 = ((_co.currentOptions == null) ? null : _co.currentOptions.previewRotate); var currVal_33 = ((_co.currentOptions == null) ? null : _co.currentOptions.rotateLeftIcon); var currVal_34 = ((_co.currentOptions == null) ? null : _co.currentOptions.rotateRightIcon); var currVal_35 = ((_co.currentOptions == null) ? null : _co.currentOptions.previewDownload); var currVal_36 = ((_co.currentOptions == null) ? null : _co.currentOptions.downloadIcon); var currVal_37 = ((_co.currentOptions == null) ? null : _co.currentOptions.previewBullets); _ck(_v, 9, 1, [currVal_4, currVal_5, currVal_6, currVal_7, currVal_8, currVal_9, currVal_10, currVal_11, currVal_12, currVal_13, currVal_14, currVal_15, currVal_16, currVal_17, currVal_18, currVal_19, currVal_20, currVal_21, currVal_22, currVal_23, currVal_24, currVal_25, currVal_26, currVal_27, currVal_28, currVal_29, currVal_30, currVal_31, currVal_32, currVal_33, currVal_34, currVal_35, currVal_36, currVal_37]); }, function (_ck, _v) { var _co = _v.component; var currVal_0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵinlineInterpolate"](1, "ngx-gallery-layout ", ((_co.currentOptions == null) ? null : _co.currentOptions.layout), ""); _ck(_v, 3, 0, currVal_0); var currVal_3 = _co.previewEnabled; _ck(_v, 8, 0, currVal_3); }); }
function View_NgxGalleryComponent_Host_0(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](0, 0, null, null, 2, "ngx-gallery", [], [[4, "width", null], [4, "height", null], [4, "transform", null]], [["window", "resize"]], function (_v, en, $event) { var ad = true; if (("window:resize" === en)) {
        var pd_0 = (_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵnov"](_v, 2).onResize() !== false);
        ad = (pd_0 && ad);
    } return ad; }, View_NgxGalleryComponent_0, RenderType_NgxGalleryComponent)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵprd"](512, null, _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_1__["NgxGalleryService"], _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_1__["NgxGalleryService"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](2, 4571136, null, 0, _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_1__["NgxGalleryComponent"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"], _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_1__["NgxGalleryService"]], null, null)], function (_ck, _v) { _ck(_v, 2, 0); }, function (_ck, _v) { var currVal_0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵnov"](_v, 2).width; var currVal_1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵnov"](_v, 2).height; var currVal_2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵnov"](_v, 2).left; _ck(_v, 0, 0, currVal_0, currVal_1, currVal_2); }); }
var NgxGalleryComponentNgFactory = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵccf"]("ngx-gallery", _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_1__["NgxGalleryComponent"], View_NgxGalleryComponent_Host_0, { options: "options", images: "images" }, { imagesReady: "imagesReady", change: "change", previewOpen: "previewOpen", previewClose: "previewClose", previewChange: "previewChange" }, []);

var styles_NgxGalleryThumbnailsComponent = ["@charset \"UTF-8\";[_nghost-%COMP%]{width:100%;display:inline-block;position:relative;font-size:25px}.ngx-gallery-thumbnails-wrapper[_ngcontent-%COMP%]{width:100%;height:100%;position:absolute;overflow:hidden}.ngx-gallery-thumbnails[_ngcontent-%COMP%]{height:100%;width:100%;position:absolute;left:0;transform:translateX(0);transition:transform .5s ease-in-out;will-change:transform}.ngx-gallery-thumbnails[_ngcontent-%COMP%]   .ngx-gallery-thumbnail[_ngcontent-%COMP%]{position:absolute;height:100%;background-position:center;background-repeat:no-repeat;text-decoration:none;border:1px double #000}.ngx-gallery-thumbnails[_ngcontent-%COMP%]   .ngx-gallery-thumbnail[_ngcontent-%COMP%]   .ngx-gallery-thumbnail-video[_ngcontent-%COMP%]::after{content:\"\uF144\";display:block;position:absolute;background:#0000;height:100%;width:100%;left:0;top:calc(50% - 20px);font-size:40px;color:#fff;margin:0;padding:0;font-family:fontawesome;text-shadow:0 4px 3px rgba(0,0,0,.4),0 8px 13px rgba(0,0,0,.1),0 18px 23px rgba(0,0,0,.1)}.ngx-gallery-thumbnails[_ngcontent-%COMP%]   .ngx-gallery-thumbnail[_ngcontent-%COMP%]   .img[_ngcontent-%COMP%]{background-size:cover;height:100%}.ngx-gallery-thumbnails[_ngcontent-%COMP%]   .ngx-gallery-thumbnail.ngx-gallery-active[_ngcontent-%COMP%]{border:1px double #cc4548}.ngx-gallery-thumbnail-size-cover[_ngcontent-%COMP%]   .ngx-gallery-thumbnails[_ngcontent-%COMP%]   .ngx-gallery-thumbnail[_ngcontent-%COMP%]{background-size:cover}.ngx-gallery-thumbnail-size-contain[_ngcontent-%COMP%]   .ngx-gallery-thumbnails[_ngcontent-%COMP%]   .ngx-gallery-thumbnail[_ngcontent-%COMP%]{background-size:contain}.ngx-gallery-remaining-count-overlay[_ngcontent-%COMP%]{width:100%;height:100%;position:absolute;left:0;top:0;background-color:rgba(0,0,0,.4)}.ngx-gallery-remaining-count[_ngcontent-%COMP%]{position:absolute;top:50%;left:50%;transform:translate(-50%,-50%);color:#fff;font-size:30px}"];
var RenderType_NgxGalleryThumbnailsComponent = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵcrt"]({ encapsulation: 0, styles: styles_NgxGalleryThumbnailsComponent, data: {} });

function View_NgxGalleryThumbnailsComponent_2(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](0, 0, null, null, 2, "div", [["class", "ngx-gallery-thumbnail"], ["style", "width: 100%; height: 100%; position:absolute;"]], [[4, "background-image", null]], null, null, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](1, 278528, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgClass"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["IterableDiffers"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["KeyValueDiffers"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"]], { klass: [0, "klass"], ngClass: [1, "ngClass"] }, null), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵpod"](2, { "ngx-gallery-active": 0, "ngx-gallery-clickable": 1 })], function (_ck, _v) { var _co = _v.component; var currVal_1 = "ngx-gallery-thumbnail"; var currVal_2 = _ck(_v, 2, 0, (_v.parent.context.index == _co.selectedIndex), _co.clickable); _ck(_v, 1, 0, currVal_1, currVal_2); }, function (_ck, _v) { var _co = _v.component; var currVal_0 = _co.getSafeUrl(_v.parent.context.$implicit); _ck(_v, 0, 0, currVal_0); }); }
function View_NgxGalleryThumbnailsComponent_3(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](0, 0, null, null, 5, "div", [["class", "ngx-gallery-thumbnail-video"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](1, 0, null, null, 4, "video", [["class", "ngx-gallery-thumbnail"], ["style", "width: 100%; height: 100%; position:absolute; left:0; background:#000;"]], null, null, null, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](2, 278528, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgClass"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["IterableDiffers"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["KeyValueDiffers"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"]], { klass: [0, "klass"], ngClass: [1, "ngClass"] }, null), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵpod"](3, { "ngx-gallery-active": 0, "ngx-gallery-clickable": 1 }), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](4, 0, null, null, 0, "source", [], [[8, "src", 4]], null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵted"](-1, null, [" Your browser does not support the video tag. "]))], function (_ck, _v) { var _co = _v.component; var currVal_0 = "ngx-gallery-thumbnail"; var currVal_1 = _ck(_v, 3, 0, (_v.parent.context.index == _co.selectedIndex), _co.clickable); _ck(_v, 2, 0, currVal_0, currVal_1); }, function (_ck, _v) { var currVal_2 = _v.parent.context.$implicit; _ck(_v, 4, 0, currVal_2); }); }
function View_NgxGalleryThumbnailsComponent_4(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](0, 0, null, null, 1, "ngx-gallery-action", [], null, [[null, "closeClick"]], function (_v, en, $event) { var ad = true; if (("closeClick" === en)) {
        var pd_0 = (_v.context.$implicit.onClick($event, _v.parent.context.index) !== false);
        ad = (pd_0 && ad);
    } return ad; }, View_ɵb_0, RenderType_ɵb)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](1, 114688, null, 0, _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_1__["ɵb"], [], { icon: [0, "icon"], disabled: [1, "disabled"], titleText: [2, "titleText"] }, { closeClick: "closeClick" })], function (_ck, _v) { var currVal_0 = _v.context.$implicit.icon; var currVal_1 = _v.context.$implicit.disabled; var currVal_2 = _v.context.$implicit.titleText; _ck(_v, 1, 0, currVal_0, currVal_1, currVal_2); }, null); }
function View_NgxGalleryThumbnailsComponent_5(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](0, 0, null, null, 2, "div", [["class", "ngx-gallery-remaining-count-overlay"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](1, 0, null, null, 1, "span", [["class", "ngx-gallery-remaining-count"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵted"](2, null, ["+", ""]))], null, function (_ck, _v) { var _co = _v.component; var currVal_0 = _co.remainingCountValue; _ck(_v, 2, 0, currVal_0); }); }
function View_NgxGalleryThumbnailsComponent_1(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](0, 0, null, null, 11, "a", [["class", "ngx-gallery-thumbnail"]], [[8, "href", 4], [8, "target", 0], [4, "width", null], [4, "height", null], [4, "left", null], [4, "top", null], [1, "aria-label", 0]], [[null, "click"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("click" === en)) {
        var pd_0 = (_co.handleClick($event, _v.context.index) !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](1, 278528, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgClass"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["IterableDiffers"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["KeyValueDiffers"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"]], { klass: [0, "klass"], ngClass: [1, "ngClass"] }, null), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵpod"](2, { "ngx-gallery-active": 0, "ngx-gallery-clickable": 1 }), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵand"](16777216, null, null, 1, null, View_NgxGalleryThumbnailsComponent_2)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](4, 16384, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵand"](16777216, null, null, 1, null, View_NgxGalleryThumbnailsComponent_3)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](6, 16384, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](7, 0, null, null, 2, "div", [["class", "ngx-gallery-icons-wrapper"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵand"](16777216, null, null, 1, null, View_NgxGalleryThumbnailsComponent_4)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](9, 278528, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgForOf"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["IterableDiffers"]], { ngForOf: [0, "ngForOf"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵand"](16777216, null, null, 1, null, View_NgxGalleryThumbnailsComponent_5)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](11, 16384, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null)], function (_ck, _v) { var _co = _v.component; var currVal_7 = "ngx-gallery-thumbnail"; var currVal_8 = _ck(_v, 2, 0, (_v.context.index == _co.selectedIndex), _co.clickable); _ck(_v, 1, 0, currVal_7, currVal_8); var currVal_9 = (_co.getFileType(_v.context.$implicit) == "image"); _ck(_v, 4, 0, currVal_9); var currVal_10 = (_co.getFileType(_v.context.$implicit) == "video"); _ck(_v, 6, 0, currVal_10); var currVal_11 = _co.actions; _ck(_v, 9, 0, currVal_11); var currVal_12 = ((_co.remainingCount && _co.remainingCountValue) && (_v.context.index == ((_co.rows * _co.columns) - 1))); _ck(_v, 11, 0, currVal_12); }, function (_ck, _v) { var _co = _v.component; var currVal_0 = (_co.hasLink(_v.context.index) ? _co.links[_v.context.index] : "#"); var currVal_1 = _co.linkTarget; var currVal_2 = _co.getThumbnailWidth(); var currVal_3 = _co.getThumbnailHeight(); var currVal_4 = _co.getThumbnailLeft(_v.context.index); var currVal_5 = _co.getThumbnailTop(_v.context.index); var currVal_6 = _co.labels[_v.context.index]; _ck(_v, 0, 0, currVal_0, currVal_1, currVal_2, currVal_3, currVal_4, currVal_5, currVal_6); }); }
function View_NgxGalleryThumbnailsComponent_6(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](0, 0, null, null, 1, "ngx-gallery-arrows", [], null, [[null, "prevClick"], [null, "nextClick"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("prevClick" === en)) {
        var pd_0 = (_co.moveLeft() !== false);
        ad = (pd_0 && ad);
    } if (("nextClick" === en)) {
        var pd_1 = (_co.moveRight() !== false);
        ad = (pd_1 && ad);
    } return ad; }, View_NgxGalleryArrowsComponent_0, RenderType_NgxGalleryArrowsComponent)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](1, 114688, null, 0, _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_1__["NgxGalleryArrowsComponent"], [], { prevDisabled: [0, "prevDisabled"], nextDisabled: [1, "nextDisabled"], arrowPrevIcon: [2, "arrowPrevIcon"], arrowNextIcon: [3, "arrowNextIcon"] }, { prevClick: "prevClick", nextClick: "nextClick" })], function (_ck, _v) { var _co = _v.component; var currVal_0 = !_co.canMoveLeft(); var currVal_1 = !_co.canMoveRight(); var currVal_2 = _co.arrowPrevIcon; var currVal_3 = _co.arrowNextIcon; _ck(_v, 1, 0, currVal_0, currVal_1, currVal_2, currVal_3); }, null); }
function View_NgxGalleryThumbnailsComponent_0(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵvid"](2, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](0, 0, null, null, 3, "div", [], [[8, "className", 0]], null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](1, 0, null, null, 2, "div", [["class", "ngx-gallery-thumbnails"]], [[4, "transform", null], [4, "marginLeft", null]], null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵand"](16777216, null, null, 1, null, View_NgxGalleryThumbnailsComponent_1)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](3, 278528, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgForOf"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["IterableDiffers"]], { ngForOf: [0, "ngForOf"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵand"](16777216, null, null, 1, null, View_NgxGalleryThumbnailsComponent_6)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](5, 16384, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null)], function (_ck, _v) { var _co = _v.component; var currVal_3 = _co.getImages(); _ck(_v, 3, 0, currVal_3); var currVal_4 = _co.canShowArrows(); _ck(_v, 5, 0, currVal_4); }, function (_ck, _v) { var _co = _v.component; var currVal_0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵinlineInterpolate"](1, "ngx-gallery-thumbnails-wrapper ngx-gallery-thumbnail-size-", _co.size, ""); _ck(_v, 0, 0, currVal_0); var currVal_1 = (("translateX(" + _co.thumbnailsLeft) + ")"); var currVal_2 = _co.thumbnailsMarginLeft; _ck(_v, 1, 0, currVal_1, currVal_2); }); }
function View_NgxGalleryThumbnailsComponent_Host_0(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](0, 0, null, null, 1, "ngx-gallery-thumbnails", [], null, [[null, "mouseenter"], [null, "mouseleave"]], function (_v, en, $event) { var ad = true; if (("mouseenter" === en)) {
        var pd_0 = (_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵnov"](_v, 1).onMouseEnter() !== false);
        ad = (pd_0 && ad);
    } if (("mouseleave" === en)) {
        var pd_1 = (_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵnov"](_v, 1).onMouseLeave() !== false);
        ad = (pd_1 && ad);
    } return ad; }, View_NgxGalleryThumbnailsComponent_0, RenderType_NgxGalleryThumbnailsComponent)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](1, 638976, null, 0, _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_1__["NgxGalleryThumbnailsComponent"], [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["DomSanitizer"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"], _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_1__["NgxGalleryService"]], null, null)], function (_ck, _v) { _ck(_v, 1, 0); }, null); }
var NgxGalleryThumbnailsComponentNgFactory = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵccf"]("ngx-gallery-thumbnails", _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_1__["NgxGalleryThumbnailsComponent"], View_NgxGalleryThumbnailsComponent_Host_0, { images: "images", links: "links", labels: "labels", linkTarget: "linkTarget", columns: "columns", rows: "rows", arrows: "arrows", arrowsAutoHide: "arrowsAutoHide", margin: "margin", selectedIndex: "selectedIndex", clickable: "clickable", swipe: "swipe", size: "size", arrowPrevIcon: "arrowPrevIcon", arrowNextIcon: "arrowNextIcon", moveSize: "moveSize", order: "order", remainingCount: "remainingCount", lazyLoading: "lazyLoading", actions: "actions" }, { activeChange: "activeChange" }, []);

var styles_NgxGalleryPreviewComponent = [".ngx-gallery-active[_nghost-%COMP%]{width:100%;height:100%;position:fixed;left:0;top:0;background:rgba(0,0,0,.7);z-index:10000;display:inline-block;font-size:50px!important}[_nghost-%COMP%]{display:none;font-size:50px!important}[_nghost-%COMP%]   .ngx-gallery-arrow[_ngcontent-%COMP%]{font-size:50px!important}ngx-gallery-bullets[_ngcontent-%COMP%]{height:5%;align-items:center;padding:0}.ngx-gallery-preview-img[_ngcontent-%COMP%]{opacity:0;max-width:90%;max-height:90%;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;transition:transform .5s}.ngx-gallery-preview-img.animation[_ngcontent-%COMP%]{transition:opacity .5s linear,transform .5s}.ngx-gallery-preview-img.ngx-gallery-active[_ngcontent-%COMP%]{opacity:1}.ngx-gallery-preview-img.ngx-gallery-grab[_ngcontent-%COMP%]{cursor:-webkit-grab;cursor:grab}.ngx-gallery-icon.ngx-gallery-spinner[_ngcontent-%COMP%]{font-size:50px;left:0;display:inline-block}[_nghost-%COMP%]   .ngx-gallery-preview-top[_ngcontent-%COMP%]{position:absolute;width:100%;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;font-size:25px}.ngx-gallery-preview-icons[_ngcontent-%COMP%]{float:right}.ngx-gallery-preview-icons[_ngcontent-%COMP%]   .ngx-gallery-icon[_ngcontent-%COMP%]{position:relative;margin-right:10px;margin-top:10px;font-size:25px;cursor:pointer;text-decoration:none}.ngx-gallery-preview-icons[_ngcontent-%COMP%]   .ngx-gallery-icon.ngx-gallery-icon-disabled[_ngcontent-%COMP%]{cursor:default;opacity:.4}.ngx-spinner-wrapper[_ngcontent-%COMP%]{width:50px;height:50px;display:none}.ngx-spinner-wrapper.ngx-gallery-active[_ngcontent-%COMP%]{display:inline-block}.ngx-gallery-center[_ngcontent-%COMP%]{position:absolute;left:0;right:0;bottom:0;margin:auto;top:0}.ngx-gallery-preview-text[_ngcontent-%COMP%]{width:100%;background:rgba(0,0,0,.7);padding:10px;text-align:center;color:#fff;font-size:16px;flex:0 1 auto;z-index:10}.ngx-gallery-preview-wrapper[_ngcontent-%COMP%]{width:100%;height:100%;display:flex;flex-flow:column}.ngx-gallery-preview-img-wrapper[_ngcontent-%COMP%]{flex:1 1 auto;position:relative}"];
var RenderType_NgxGalleryPreviewComponent = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵcrt"]({ encapsulation: 0, styles: styles_NgxGalleryPreviewComponent, data: {} });

function View_NgxGalleryPreviewComponent_1(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](0, 0, null, null, 1, "ngx-gallery-arrows", [], null, [[null, "prevClick"], [null, "nextClick"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("prevClick" === en)) {
        var pd_0 = (_co.showPrev() !== false);
        ad = (pd_0 && ad);
    } if (("nextClick" === en)) {
        var pd_1 = (_co.showNext() !== false);
        ad = (pd_1 && ad);
    } return ad; }, View_NgxGalleryArrowsComponent_0, RenderType_NgxGalleryArrowsComponent)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](1, 114688, null, 0, _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_1__["NgxGalleryArrowsComponent"], [], { prevDisabled: [0, "prevDisabled"], nextDisabled: [1, "nextDisabled"], arrowPrevIcon: [2, "arrowPrevIcon"], arrowNextIcon: [3, "arrowNextIcon"] }, { prevClick: "prevClick", nextClick: "nextClick" })], function (_ck, _v) { var _co = _v.component; var currVal_0 = !_co.canShowPrev(); var currVal_1 = !_co.canShowNext(); var currVal_2 = _co.arrowPrevIcon; var currVal_3 = _co.arrowNextIcon; _ck(_v, 1, 0, currVal_0, currVal_1, currVal_2, currVal_3); }, null); }
function View_NgxGalleryPreviewComponent_2(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](0, 0, null, null, 1, "ngx-gallery-action", [], null, [[null, "closeClick"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("closeClick" === en)) {
        var pd_0 = (_v.context.$implicit.onClick($event, _co.index) !== false);
        ad = (pd_0 && ad);
    } return ad; }, View_ɵb_0, RenderType_ɵb)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](1, 114688, null, 0, _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_1__["ɵb"], [], { icon: [0, "icon"], disabled: [1, "disabled"], titleText: [2, "titleText"] }, { closeClick: "closeClick" })], function (_ck, _v) { var currVal_0 = _v.context.$implicit.icon; var currVal_1 = _v.context.$implicit.disabled; var currVal_2 = _v.context.$implicit.titleText; _ck(_v, 1, 0, currVal_0, currVal_1, currVal_2); }, null); }
function View_NgxGalleryPreviewComponent_3(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](0, 0, null, null, 1, "a", [["aria-hidden", "true"], ["class", "ngx-gallery-icon"], ["download", ""]], [[8, "href", 4]], null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](1, 0, null, null, 0, "i", [], [[8, "className", 0]], null, null, null, null))], null, function (_ck, _v) { var _co = _v.component; var currVal_0 = _co.src; _ck(_v, 0, 0, currVal_0); var currVal_1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵinlineInterpolate"](1, "ngx-gallery-icon-content ", _co.downloadIcon, ""); _ck(_v, 1, 0, currVal_1); }); }
function View_NgxGalleryPreviewComponent_4(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](0, 0, null, null, 1, "ngx-gallery-action", [], null, [[null, "closeClick"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("closeClick" === en)) {
        var pd_0 = (_co.zoomOut() !== false);
        ad = (pd_0 && ad);
    } return ad; }, View_ɵb_0, RenderType_ɵb)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](1, 114688, null, 0, _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_1__["ɵb"], [], { icon: [0, "icon"], disabled: [1, "disabled"] }, { closeClick: "closeClick" })], function (_ck, _v) { var _co = _v.component; var currVal_0 = _co.zoomOutIcon; var currVal_1 = !_co.canZoomOut(); _ck(_v, 1, 0, currVal_0, currVal_1); }, null); }
function View_NgxGalleryPreviewComponent_5(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](0, 0, null, null, 1, "ngx-gallery-action", [], null, [[null, "closeClick"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("closeClick" === en)) {
        var pd_0 = (_co.zoomIn() !== false);
        ad = (pd_0 && ad);
    } return ad; }, View_ɵb_0, RenderType_ɵb)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](1, 114688, null, 0, _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_1__["ɵb"], [], { icon: [0, "icon"], disabled: [1, "disabled"] }, { closeClick: "closeClick" })], function (_ck, _v) { var _co = _v.component; var currVal_0 = _co.zoomInIcon; var currVal_1 = !_co.canZoomIn(); _ck(_v, 1, 0, currVal_0, currVal_1); }, null); }
function View_NgxGalleryPreviewComponent_6(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](0, 0, null, null, 1, "ngx-gallery-action", [], null, [[null, "closeClick"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("closeClick" === en)) {
        var pd_0 = (_co.rotateLeft() !== false);
        ad = (pd_0 && ad);
    } return ad; }, View_ɵb_0, RenderType_ɵb)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](1, 114688, null, 0, _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_1__["ɵb"], [], { icon: [0, "icon"] }, { closeClick: "closeClick" })], function (_ck, _v) { var _co = _v.component; var currVal_0 = _co.rotateLeftIcon; _ck(_v, 1, 0, currVal_0); }, null); }
function View_NgxGalleryPreviewComponent_7(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](0, 0, null, null, 1, "ngx-gallery-action", [], null, [[null, "closeClick"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("closeClick" === en)) {
        var pd_0 = (_co.rotateRight() !== false);
        ad = (pd_0 && ad);
    } return ad; }, View_ɵb_0, RenderType_ɵb)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](1, 114688, null, 0, _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_1__["ɵb"], [], { icon: [0, "icon"] }, { closeClick: "closeClick" })], function (_ck, _v) { var _co = _v.component; var currVal_0 = _co.rotateRightIcon; _ck(_v, 1, 0, currVal_0); }, null); }
function View_NgxGalleryPreviewComponent_8(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](0, 0, null, null, 1, "ngx-gallery-action", [], null, [[null, "closeClick"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("closeClick" === en)) {
        var pd_0 = (_co.manageFullscreen() !== false);
        ad = (pd_0 && ad);
    } return ad; }, View_ɵb_0, RenderType_ɵb)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](1, 114688, null, 0, _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_1__["ɵb"], [], { icon: [0, "icon"] }, { closeClick: "closeClick" })], function (_ck, _v) { var _co = _v.component; var currVal_0 = ("ngx-gallery-fullscreen " + _co.fullscreenIcon); _ck(_v, 1, 0, currVal_0); }, null); }
function View_NgxGalleryPreviewComponent_9(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](0, 0, [[1, 0], ["previewImage", 1]], null, 0, "img", [["class", "ngx-gallery-preview-img ngx-gallery-center"]], [[8, "src", 4], [2, "ngx-gallery-active", null], [2, "animation", null], [2, "ngx-gallery-grab", null], [4, "transform", null], [4, "left", null], [4, "top", null]], [[null, "click"], [null, "mouseenter"], [null, "mouseleave"], [null, "mousedown"], [null, "touchstart"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("click" === en)) {
        var pd_0 = ($event.stopPropagation() !== false);
        ad = (pd_0 && ad);
    } if (("mouseenter" === en)) {
        var pd_1 = (_co.imageMouseEnter() !== false);
        ad = (pd_1 && ad);
    } if (("mouseleave" === en)) {
        var pd_2 = (_co.imageMouseLeave() !== false);
        ad = (pd_2 && ad);
    } if (("mousedown" === en)) {
        var pd_3 = (_co.mouseDownHandler($event) !== false);
        ad = (pd_3 && ad);
    } if (("touchstart" === en)) {
        var pd_4 = (_co.mouseDownHandler($event) !== false);
        ad = (pd_4 && ad);
    } return ad; }, null, null))], null, function (_ck, _v) { var _co = _v.component; var currVal_0 = _co.src; var currVal_1 = !_co.loading; var currVal_2 = _co.animation; var currVal_3 = _co.canDragOnZoom(); var currVal_4 = _co.getTransform(); var currVal_5 = (_co.positionLeft + "px"); var currVal_6 = (_co.positionTop + "px"); _ck(_v, 0, 0, currVal_0, currVal_1, currVal_2, currVal_3, currVal_4, currVal_5, currVal_6); }); }
function View_NgxGalleryPreviewComponent_10(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](0, 0, [[1, 0], ["previewImage", 1]], null, 2, "video", [["class", "ngx-gallery-preview-img ngx-gallery-center"], ["controls", ""], ["style", "width: 100%; height: 100%;"]], [[2, "ngx-gallery-active", null], [2, "animation", null], [2, "ngx-gallery-grab", null], [4, "transform", null], [4, "left", null], [4, "top", null]], [[null, "click"], [null, "mouseenter"], [null, "mouseleave"], [null, "mousedown"], [null, "touchstart"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("click" === en)) {
        var pd_0 = ($event.stopPropagation() !== false);
        ad = (pd_0 && ad);
    } if (("mouseenter" === en)) {
        var pd_1 = (_co.imageMouseEnter() !== false);
        ad = (pd_1 && ad);
    } if (("mouseleave" === en)) {
        var pd_2 = (_co.imageMouseLeave() !== false);
        ad = (pd_2 && ad);
    } if (("mousedown" === en)) {
        var pd_3 = (_co.mouseDownHandler($event) !== false);
        ad = (pd_3 && ad);
    } if (("touchstart" === en)) {
        var pd_4 = (_co.mouseDownHandler($event) !== false);
        ad = (pd_4 && ad);
    } return ad; }, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](1, 0, null, null, 0, "source", [], [[8, "src", 4]], null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵted"](-1, null, [" Your browser does not support the video tag. "]))], null, function (_ck, _v) { var _co = _v.component; var currVal_0 = !_co.loading; var currVal_1 = _co.animation; var currVal_2 = _co.canDragOnZoom(); var currVal_3 = _co.getTransform(); var currVal_4 = (_co.positionLeft + "px"); var currVal_5 = (_co.positionTop + "px"); _ck(_v, 0, 0, currVal_0, currVal_1, currVal_2, currVal_3, currVal_4, currVal_5); var currVal_6 = _co.src; _ck(_v, 1, 0, currVal_6); }); }
function View_NgxGalleryPreviewComponent_11(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](0, 0, null, null, 1, "ngx-gallery-bullets", [], null, [[null, "bulletChange"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("bulletChange" === en)) {
        var pd_0 = (_co.showAtIndex($event) !== false);
        ad = (pd_0 && ad);
    } return ad; }, View_NgxGalleryBulletsComponent_0, RenderType_NgxGalleryBulletsComponent)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](1, 114688, null, 0, _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_1__["NgxGalleryBulletsComponent"], [], { count: [0, "count"], active: [1, "active"] }, { bulletChange: "bulletChange" })], function (_ck, _v) { var _co = _v.component; var currVal_0 = _co.images.length; var currVal_1 = _co.index; _ck(_v, 1, 0, currVal_0, currVal_1); }, null); }
function View_NgxGalleryPreviewComponent_12(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](0, 0, null, null, 0, "div", [["class", "ngx-gallery-preview-text"]], [[8, "innerHTML", 1]], [[null, "click"]], function (_v, en, $event) { var ad = true; if (("click" === en)) {
        var pd_0 = ($event.stopPropagation() !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null))], null, function (_ck, _v) { var _co = _v.component; var currVal_0 = _co.description; _ck(_v, 0, 0, currVal_0); }); }
function View_NgxGalleryPreviewComponent_0(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵvid"](2, [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵqud"](671088640, 1, { previewImage: 0 }), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵand"](16777216, null, null, 1, null, View_NgxGalleryPreviewComponent_1)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](2, 16384, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](3, 0, null, null, 17, "div", [["class", "ngx-gallery-preview-top"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](4, 0, null, null, 16, "div", [["class", "ngx-gallery-preview-icons"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵand"](16777216, null, null, 1, null, View_NgxGalleryPreviewComponent_2)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](6, 278528, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgForOf"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["IterableDiffers"]], { ngForOf: [0, "ngForOf"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵand"](16777216, null, null, 1, null, View_NgxGalleryPreviewComponent_3)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](8, 16384, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵand"](16777216, null, null, 1, null, View_NgxGalleryPreviewComponent_4)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](10, 16384, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵand"](16777216, null, null, 1, null, View_NgxGalleryPreviewComponent_5)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](12, 16384, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵand"](16777216, null, null, 1, null, View_NgxGalleryPreviewComponent_6)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](14, 16384, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵand"](16777216, null, null, 1, null, View_NgxGalleryPreviewComponent_7)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](16, 16384, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵand"](16777216, null, null, 1, null, View_NgxGalleryPreviewComponent_8)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](18, 16384, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](19, 0, null, null, 1, "ngx-gallery-action", [], null, [[null, "closeClick"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("closeClick" === en)) {
        var pd_0 = (_co.close() !== false);
        ad = (pd_0 && ad);
    } return ad; }, View_ɵb_0, RenderType_ɵb)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](20, 114688, null, 0, _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_1__["ɵb"], [], { icon: [0, "icon"] }, { closeClick: "closeClick" }), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](21, 0, null, null, 1, "div", [["class", "ngx-spinner-wrapper ngx-gallery-center"]], [[2, "ngx-gallery-active", null]], null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](22, 0, null, null, 0, "i", [["aria-hidden", "true"]], [[8, "className", 0]], null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](23, 0, null, null, 9, "div", [["class", "ngx-gallery-preview-wrapper"]], null, [[null, "click"], [null, "mouseup"], [null, "mousemove"], [null, "touchend"], [null, "touchmove"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("click" === en)) {
        var pd_0 = ((_co.closeOnClick && _co.close()) !== false);
        ad = (pd_0 && ad);
    } if (("mouseup" === en)) {
        var pd_1 = (_co.mouseUpHandler($event) !== false);
        ad = (pd_1 && ad);
    } if (("mousemove" === en)) {
        var pd_2 = (_co.mouseMoveHandler($event) !== false);
        ad = (pd_2 && ad);
    } if (("touchend" === en)) {
        var pd_3 = (_co.mouseUpHandler($event) !== false);
        ad = (pd_3 && ad);
    } if (("touchmove" === en)) {
        var pd_4 = (_co.mouseMoveHandler($event) !== false);
        ad = (pd_4 && ad);
    } return ad; }, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](24, 0, null, null, 6, "div", [["class", "ngx-gallery-preview-img-wrapper"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵand"](16777216, null, null, 1, null, View_NgxGalleryPreviewComponent_9)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](26, 16384, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵand"](16777216, null, null, 1, null, View_NgxGalleryPreviewComponent_10)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](28, 16384, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵand"](16777216, null, null, 1, null, View_NgxGalleryPreviewComponent_11)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](30, 16384, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵand"](16777216, null, null, 1, null, View_NgxGalleryPreviewComponent_12)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](32, 16384, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null)], function (_ck, _v) { var _co = _v.component; var currVal_0 = _co.arrows; _ck(_v, 2, 0, currVal_0); var currVal_1 = _co.actions; _ck(_v, 6, 0, currVal_1); var currVal_2 = (_co.download && _co.src); _ck(_v, 8, 0, currVal_2); var currVal_3 = _co.zoom; _ck(_v, 10, 0, currVal_3); var currVal_4 = _co.zoom; _ck(_v, 12, 0, currVal_4); var currVal_5 = _co.rotate; _ck(_v, 14, 0, currVal_5); var currVal_6 = _co.rotate; _ck(_v, 16, 0, currVal_6); var currVal_7 = _co.fullscreen; _ck(_v, 18, 0, currVal_7); var currVal_8 = ("ngx-gallery-close " + _co.closeIcon); _ck(_v, 20, 0, currVal_8); var currVal_11 = (_co.src && (_co.type == "image")); _ck(_v, 26, 0, currVal_11); var currVal_12 = (_co.src && (_co.type == "video")); _ck(_v, 28, 0, currVal_12); var currVal_13 = _co.bullets; _ck(_v, 30, 0, currVal_13); var currVal_14 = (_co.showDescription && _co.description); _ck(_v, 32, 0, currVal_14); }, function (_ck, _v) { var _co = _v.component; var currVal_9 = _co.showSpinner; _ck(_v, 21, 0, currVal_9); var currVal_10 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵinlineInterpolate"](1, "ngx-gallery-icon ngx-gallery-spinner ", _co.spinnerIcon, ""); _ck(_v, 22, 0, currVal_10); }); }
function View_NgxGalleryPreviewComponent_Host_0(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](0, 0, null, null, 1, "ngx-gallery-preview", [], null, [[null, "mouseenter"], [null, "mouseleave"]], function (_v, en, $event) { var ad = true; if (("mouseenter" === en)) {
        var pd_0 = (_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵnov"](_v, 1).onMouseEnter() !== false);
        ad = (pd_0 && ad);
    } if (("mouseleave" === en)) {
        var pd_1 = (_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵnov"](_v, 1).onMouseLeave() !== false);
        ad = (pd_1 && ad);
    } return ad; }, View_NgxGalleryPreviewComponent_0, RenderType_NgxGalleryPreviewComponent)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](1, 770048, null, 0, _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_1__["NgxGalleryPreviewComponent"], [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["DomSanitizer"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"], _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_1__["NgxGalleryService"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]], null, null)], function (_ck, _v) { _ck(_v, 1, 0); }, null); }
var NgxGalleryPreviewComponentNgFactory = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵccf"]("ngx-gallery-preview", _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_1__["NgxGalleryPreviewComponent"], View_NgxGalleryPreviewComponent_Host_0, { images: "images", descriptions: "descriptions", showDescription: "showDescription", arrows: "arrows", arrowsAutoHide: "arrowsAutoHide", swipe: "swipe", fullscreen: "fullscreen", forceFullscreen: "forceFullscreen", closeOnClick: "closeOnClick", closeOnEsc: "closeOnEsc", keyboardNavigation: "keyboardNavigation", arrowPrevIcon: "arrowPrevIcon", arrowNextIcon: "arrowNextIcon", closeIcon: "closeIcon", fullscreenIcon: "fullscreenIcon", spinnerIcon: "spinnerIcon", autoPlay: "autoPlay", autoPlayInterval: "autoPlayInterval", autoPlayPauseOnHover: "autoPlayPauseOnHover", infinityMove: "infinityMove", zoom: "zoom", zoomStep: "zoomStep", zoomMax: "zoomMax", zoomMin: "zoomMin", zoomInIcon: "zoomInIcon", zoomOutIcon: "zoomOutIcon", animation: "animation", actions: "actions", rotate: "rotate", rotateLeftIcon: "rotateLeftIcon", rotateRightIcon: "rotateRightIcon", download: "download", downloadIcon: "downloadIcon", bullets: "bullets" }, { previewOpen: "previewOpen", previewClose: "previewClose", activeChange: "activeChange" }, []);

var styles_NgxGalleryArrowsComponent = [".ngx-gallery-arrow-wrapper[_ngcontent-%COMP%]{position:absolute;height:100%;width:1px;display:table;table-layout:fixed}.ngx-gallery-preview-img-wrapper[_ngcontent-%COMP%]   .ngx-gallery-arrow-wrapper[_ngcontent-%COMP%]{z-index:10001}.ngx-gallery-arrow-left[_ngcontent-%COMP%]{left:0}.ngx-gallery-arrow-right[_ngcontent-%COMP%]{right:0}.ngx-gallery-arrow[_ngcontent-%COMP%]{top:50%;transform:translateY(-50%);cursor:pointer}.ngx-gallery-arrow.ngx-gallery-disabled[_ngcontent-%COMP%]{opacity:.6;cursor:default}.ngx-gallery-arrow-left[_ngcontent-%COMP%]   .ngx-gallery-arrow[_ngcontent-%COMP%]{left:10px}.ngx-gallery-arrow-right[_ngcontent-%COMP%]   .ngx-gallery-arrow[_ngcontent-%COMP%]{right:10px}"];
var RenderType_NgxGalleryArrowsComponent = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵcrt"]({ encapsulation: 0, styles: styles_NgxGalleryArrowsComponent, data: {} });

function View_NgxGalleryArrowsComponent_0(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵvid"](2, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](0, 0, null, null, 2, "div", [["class", "ngx-gallery-arrows-wrapper ngx-gallery-arrow-left"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](1, 0, null, null, 1, "div", [["aria-hidden", "true"], ["class", "ngx-gallery-icon ngx-gallery-arrow"]], [[2, "ngx-gallery-disabled", null]], [[null, "click"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("click" === en)) {
        var pd_0 = (_co.handlePrevClick() !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](2, 0, null, null, 0, "i", [], [[8, "className", 0]], null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](3, 0, null, null, 2, "div", [["class", "ngx-gallery-arrows-wrapper ngx-gallery-arrow-right"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](4, 0, null, null, 1, "div", [["aria-hidden", "true"], ["class", "ngx-gallery-icon ngx-gallery-arrow"]], [[2, "ngx-gallery-disabled", null]], [[null, "click"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("click" === en)) {
        var pd_0 = (_co.handleNextClick() !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](5, 0, null, null, 0, "i", [], [[8, "className", 0]], null, null, null, null))], null, function (_ck, _v) { var _co = _v.component; var currVal_0 = _co.prevDisabled; _ck(_v, 1, 0, currVal_0); var currVal_1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵinlineInterpolate"](1, "ngx-gallery-icon-content ", _co.arrowPrevIcon, ""); _ck(_v, 2, 0, currVal_1); var currVal_2 = _co.nextDisabled; _ck(_v, 4, 0, currVal_2); var currVal_3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵinlineInterpolate"](1, "ngx-gallery-icon-content ", _co.arrowNextIcon, ""); _ck(_v, 5, 0, currVal_3); }); }
function View_NgxGalleryArrowsComponent_Host_0(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](0, 0, null, null, 1, "ngx-gallery-arrows", [], null, null, null, View_NgxGalleryArrowsComponent_0, RenderType_NgxGalleryArrowsComponent)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](1, 114688, null, 0, _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_1__["NgxGalleryArrowsComponent"], [], null, null)], function (_ck, _v) { _ck(_v, 1, 0); }, null); }
var NgxGalleryArrowsComponentNgFactory = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵccf"]("ngx-gallery-arrows", _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_1__["NgxGalleryArrowsComponent"], View_NgxGalleryArrowsComponent_Host_0, { prevDisabled: "prevDisabled", nextDisabled: "nextDisabled", arrowPrevIcon: "arrowPrevIcon", arrowNextIcon: "arrowNextIcon" }, { prevClick: "prevClick", nextClick: "nextClick" }, []);

var styles_NgxGalleryBulletsComponent = ["[_nghost-%COMP%]{position:absolute;z-index:2000;display:inline-flex;left:50%;transform:translateX(-50%);bottom:0;padding:10px}.ngx-gallery-bullet[_ngcontent-%COMP%]{width:10px;height:10px;border-radius:50%;cursor:pointer;background:#fff}.ngx-gallery-bullet[_ngcontent-%COMP%]:not(:first-child){margin-left:5px}.ngx-gallery-bullet.ngx-gallery-active[_ngcontent-%COMP%], .ngx-gallery-bullet[_ngcontent-%COMP%]:hover{background:#000}"];
var RenderType_NgxGalleryBulletsComponent = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵcrt"]({ encapsulation: 0, styles: styles_NgxGalleryBulletsComponent, data: {} });

function View_NgxGalleryBulletsComponent_1(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](0, 0, null, null, 2, "div", [["class", "ngx-gallery-bullet"]], null, [[null, "click"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("click" === en)) {
        var pd_0 = (_co.handleChange($event, _v.context.index) !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](1, 278528, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgClass"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["IterableDiffers"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["KeyValueDiffers"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"]], { klass: [0, "klass"], ngClass: [1, "ngClass"] }, null), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵpod"](2, { "ngx-gallery-active": 0 })], function (_ck, _v) { var _co = _v.component; var currVal_0 = "ngx-gallery-bullet"; var currVal_1 = _ck(_v, 2, 0, (_v.context.index == _co.active)); _ck(_v, 1, 0, currVal_0, currVal_1); }, null); }
function View_NgxGalleryBulletsComponent_0(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵvid"](2, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵand"](16777216, null, null, 1, null, View_NgxGalleryBulletsComponent_1)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](1, 278528, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgForOf"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["IterableDiffers"]], { ngForOf: [0, "ngForOf"] }, null)], function (_ck, _v) { var _co = _v.component; var currVal_0 = _co.getBullets(); _ck(_v, 1, 0, currVal_0); }, null); }
function View_NgxGalleryBulletsComponent_Host_0(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](0, 0, null, null, 1, "ngx-gallery-bullets", [], null, null, null, View_NgxGalleryBulletsComponent_0, RenderType_NgxGalleryBulletsComponent)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](1, 114688, null, 0, _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_1__["NgxGalleryBulletsComponent"], [], null, null)], function (_ck, _v) { _ck(_v, 1, 0); }, null); }
var NgxGalleryBulletsComponentNgFactory = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵccf"]("ngx-gallery-bullets", _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_1__["NgxGalleryBulletsComponent"], View_NgxGalleryBulletsComponent_Host_0, { count: "count", active: "active" }, { bulletChange: "bulletChange" }, []);

var styles_ɵa = ["[_nghost-%COMP%]{width:100%;display:inline-block;position:relative;font-size:25px}.ngx-gallery-image-wrapper[_ngcontent-%COMP%]{width:100%;height:100%;position:absolute;left:0;top:0;overflow:hidden}.ngx-gallery-image[_ngcontent-%COMP%]{background-position:center;background-repeat:no-repeat;height:100%;width:100%;position:absolute;top:0}.ngx-gallery-image-size-cover[_ngcontent-%COMP%]   .ngx-gallery-image[_ngcontent-%COMP%]{background-size:cover}.ngx-gallery-image-size-contain[_ngcontent-%COMP%]   .ngx-gallery-image[_ngcontent-%COMP%]{background-size:contain}.ngx-gallery-animation-fade[_ngcontent-%COMP%]   .ngx-gallery-image[_ngcontent-%COMP%]{left:0;opacity:0;transition:.5s ease-in-out}.ngx-gallery-animation-fade[_ngcontent-%COMP%]   .ngx-gallery-image.ngx-gallery-active[_ngcontent-%COMP%]{opacity:1}.ngx-gallery-animation-slide[_ngcontent-%COMP%]   .ngx-gallery-image[_ngcontent-%COMP%]{transition:.5s ease-in-out}.ngx-gallery-animation-slide[_ngcontent-%COMP%]   .ngx-gallery-image.ngx-gallery-active[_ngcontent-%COMP%]{transform:translateX(0)}.ngx-gallery-animation-slide[_ngcontent-%COMP%]   .ngx-gallery-image.ngx-gallery-inactive-left[_ngcontent-%COMP%]{transform:translateX(-100%)}.ngx-gallery-animation-slide[_ngcontent-%COMP%]   .ngx-gallery-image.ngx-gallery-inactive-right[_ngcontent-%COMP%]{transform:translateX(100%)}.ngx-gallery-animation-rotate[_ngcontent-%COMP%]   .ngx-gallery-image[_ngcontent-%COMP%]{transition:1s;transform:scale(3.5,3.5) rotate(90deg);left:0;opacity:0}.ngx-gallery-animation-rotate[_ngcontent-%COMP%]   .ngx-gallery-image.ngx-gallery-active[_ngcontent-%COMP%]{transform:scale(1,1) rotate(0);opacity:1}.ngx-gallery-animation-zoom[_ngcontent-%COMP%]   .ngx-gallery-image[_ngcontent-%COMP%]{transition:1s;transform:scale(2.5,2.5);left:0;opacity:0}.ngx-gallery-animation-zoom[_ngcontent-%COMP%]   .ngx-gallery-image.ngx-gallery-active[_ngcontent-%COMP%]{transform:scale(1,1);opacity:1}.ngx-gallery-image-text[_ngcontent-%COMP%]{width:100%;background:rgba(0,0,0,.7);padding:10px;text-align:center;color:#fff;font-size:16px;position:absolute;bottom:0;z-index:10}"];
var RenderType_ɵa = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵcrt"]({ encapsulation: 0, styles: styles_ɵa, data: {} });

function View_ɵa_3(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](0, 0, null, null, 1, "ngx-gallery-action", [], null, [[null, "closeClick"]], function (_v, en, $event) { var ad = true; if (("closeClick" === en)) {
        var pd_0 = (_v.context.$implicit.onClick($event, _v.parent.parent.context.$implicit.index) !== false);
        ad = (pd_0 && ad);
    } return ad; }, View_ɵb_0, RenderType_ɵb)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](1, 114688, null, 0, _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_1__["ɵb"], [], { icon: [0, "icon"], disabled: [1, "disabled"], titleText: [2, "titleText"] }, { closeClick: "closeClick" })], function (_ck, _v) { var currVal_0 = _v.context.$implicit.icon; var currVal_1 = _v.context.$implicit.disabled; var currVal_2 = _v.context.$implicit.titleText; _ck(_v, 1, 0, currVal_0, currVal_1, currVal_2); }, null); }
function View_ɵa_4(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](0, 0, null, null, 0, "div", [["class", "ngx-gallery-image-text"]], [[8, "innerHTML", 1]], [[null, "click"]], function (_v, en, $event) { var ad = true; if (("click" === en)) {
        var pd_0 = ($event.stopPropagation() !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null))], null, function (_ck, _v) { var _co = _v.component; var currVal_0 = _co.descriptions[_v.parent.parent.context.$implicit.index]; _ck(_v, 0, 0, currVal_0); }); }
function View_ɵa_2(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](0, 0, null, null, 7, "div", [["class", "ngx-gallery-image"]], [[4, "background-image", null]], [[null, "click"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("click" === en)) {
        var pd_0 = (_co.handleClick($event, _v.parent.context.$implicit.index) !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](1, 278528, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgClass"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["IterableDiffers"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["KeyValueDiffers"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"]], { klass: [0, "klass"], ngClass: [1, "ngClass"] }, null), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵpod"](2, { "ngx-gallery-active": 0, "ngx-gallery-inactive-left": 1, "ngx-gallery-inactive-right": 2, "ngx-gallery-clickable": 3 }), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](3, 0, null, null, 2, "div", [["class", "ngx-gallery-icons-wrapper"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵand"](16777216, null, null, 1, null, View_ɵa_3)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](5, 278528, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgForOf"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["IterableDiffers"]], { ngForOf: [0, "ngForOf"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵand"](16777216, null, null, 1, null, View_ɵa_4)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](7, 16384, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null)], function (_ck, _v) { var _co = _v.component; var currVal_1 = "ngx-gallery-image"; var currVal_2 = _ck(_v, 2, 0, (_co.selectedIndex == _v.parent.context.$implicit.index), (_co.selectedIndex > _v.parent.context.$implicit.index), (_co.selectedIndex < _v.parent.context.$implicit.index), _co.clickable); _ck(_v, 1, 0, currVal_1, currVal_2); var currVal_3 = _co.actions; _ck(_v, 5, 0, currVal_3); var currVal_4 = (_co.showDescription && _co.descriptions[_v.parent.context.$implicit.index]); _ck(_v, 7, 0, currVal_4); }, function (_ck, _v) { var _co = _v.component; var currVal_0 = _co.getSafeUrl(_v.parent.context.$implicit.src); _ck(_v, 0, 0, currVal_0); }); }
function View_ɵa_1(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](0, 0, null, null, 2, "div", [], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵand"](16777216, null, null, 1, null, View_ɵa_2)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](2, 16384, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null)], function (_ck, _v) { var currVal_0 = (_v.context.$implicit.type === "image"); _ck(_v, 2, 0, currVal_0); }, null); }
function View_ɵa_7(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](0, 0, null, null, 1, "ngx-gallery-action", [], null, [[null, "closeClick"]], function (_v, en, $event) { var ad = true; if (("closeClick" === en)) {
        var pd_0 = (_v.context.$implicit.onClick($event, _v.parent.parent.context.$implicit.index) !== false);
        ad = (pd_0 && ad);
    } return ad; }, View_ɵb_0, RenderType_ɵb)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](1, 114688, null, 0, _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_1__["ɵb"], [], { icon: [0, "icon"], disabled: [1, "disabled"], titleText: [2, "titleText"] }, { closeClick: "closeClick" })], function (_ck, _v) { var currVal_0 = _v.context.$implicit.icon; var currVal_1 = _v.context.$implicit.disabled; var currVal_2 = _v.context.$implicit.titleText; _ck(_v, 1, 0, currVal_0, currVal_1, currVal_2); }, null); }
function View_ɵa_8(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](0, 0, null, null, 0, "div", [["class", "ngx-gallery-image-text"]], [[8, "innerHTML", 1]], [[null, "click"]], function (_v, en, $event) { var ad = true; if (("click" === en)) {
        var pd_0 = ($event.stopPropagation() !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null))], null, function (_ck, _v) { var _co = _v.component; var currVal_0 = _co.descriptions[_v.parent.parent.context.$implicit.index]; _ck(_v, 0, 0, currVal_0); }); }
function View_ɵa_6(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](0, 0, null, null, 10, "div", [["class", "ngx-gallery-image"]], null, [[null, "click"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("click" === en)) {
        var pd_0 = (_co.handleClick($event, _v.parent.context.$implicit.index) !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](1, 278528, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgClass"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["IterableDiffers"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["KeyValueDiffers"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"]], { klass: [0, "klass"], ngClass: [1, "ngClass"] }, null), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵpod"](2, { "ngx-gallery-active": 0, "ngx-gallery-inactive-left": 1, "ngx-gallery-inactive-right": 2, "ngx-gallery-clickable": 3 }), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](3, 0, null, null, 2, "video", [["controls", ""], ["style", "width:100%; height:100%; background: #000;"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](4, 0, null, null, 0, "source", [], [[8, "src", 4]], null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵted"](-1, null, [" Your browser does not support the video tag. "])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](6, 0, null, null, 2, "div", [["class", "ngx-gallery-icons-wrapper"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵand"](16777216, null, null, 1, null, View_ɵa_7)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](8, 278528, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgForOf"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["IterableDiffers"]], { ngForOf: [0, "ngForOf"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵand"](16777216, null, null, 1, null, View_ɵa_8)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](10, 16384, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null)], function (_ck, _v) { var _co = _v.component; var currVal_0 = "ngx-gallery-image"; var currVal_1 = _ck(_v, 2, 0, (_co.selectedIndex == _v.parent.context.$implicit.index), (_co.selectedIndex > _v.parent.context.$implicit.index), (_co.selectedIndex < _v.parent.context.$implicit.index), _co.clickable); _ck(_v, 1, 0, currVal_0, currVal_1); var currVal_3 = _co.actions; _ck(_v, 8, 0, currVal_3); var currVal_4 = (_co.showDescription && _co.descriptions[_v.parent.context.$implicit.index]); _ck(_v, 10, 0, currVal_4); }, function (_ck, _v) { var currVal_2 = _v.parent.context.$implicit.src; _ck(_v, 4, 0, currVal_2); }); }
function View_ɵa_5(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](0, 0, null, null, 2, "div", [], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵand"](16777216, null, null, 1, null, View_ɵa_6)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](2, 16384, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null)], function (_ck, _v) { var currVal_0 = (_v.context.$implicit.type === "video"); _ck(_v, 2, 0, currVal_0); }, null); }
function View_ɵa_9(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](0, 0, null, null, 1, "ngx-gallery-bullets", [], null, [[null, "bulletChange"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("bulletChange" === en)) {
        var pd_0 = (_co.show($event) !== false);
        ad = (pd_0 && ad);
    } return ad; }, View_NgxGalleryBulletsComponent_0, RenderType_NgxGalleryBulletsComponent)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](1, 114688, null, 0, _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_1__["NgxGalleryBulletsComponent"], [], { count: [0, "count"], active: [1, "active"] }, { bulletChange: "bulletChange" })], function (_ck, _v) { var _co = _v.component; var currVal_0 = _co.images.length; var currVal_1 = _co.selectedIndex; _ck(_v, 1, 0, currVal_0, currVal_1); }, null); }
function View_ɵa_10(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](0, 0, null, null, 1, "ngx-gallery-arrows", [], [[8, "className", 0]], [[null, "prevClick"], [null, "nextClick"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("prevClick" === en)) {
        var pd_0 = (_co.showPrev() !== false);
        ad = (pd_0 && ad);
    } if (("nextClick" === en)) {
        var pd_1 = (_co.showNext() !== false);
        ad = (pd_1 && ad);
    } return ad; }, View_NgxGalleryArrowsComponent_0, RenderType_NgxGalleryArrowsComponent)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](1, 114688, null, 0, _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_1__["NgxGalleryArrowsComponent"], [], { prevDisabled: [0, "prevDisabled"], nextDisabled: [1, "nextDisabled"], arrowPrevIcon: [2, "arrowPrevIcon"], arrowNextIcon: [3, "arrowNextIcon"] }, { prevClick: "prevClick", nextClick: "nextClick" })], function (_ck, _v) { var _co = _v.component; var currVal_1 = !_co.canShowPrev(); var currVal_2 = !_co.canShowNext(); var currVal_3 = _co.arrowPrevIcon; var currVal_4 = _co.arrowNextIcon; _ck(_v, 1, 0, currVal_1, currVal_2, currVal_3, currVal_4); }, function (_ck, _v) { var _co = _v.component; var currVal_0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵinlineInterpolate"](1, "ngx-gallery-image-size-", _co.size, ""); _ck(_v, 0, 0, currVal_0); }); }
function View_ɵa_0(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵvid"](2, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](0, 0, null, null, 4, "div", [], [[8, "className", 0]], null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵand"](16777216, null, null, 1, null, View_ɵa_1)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](2, 278528, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgForOf"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["IterableDiffers"]], { ngForOf: [0, "ngForOf"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵand"](16777216, null, null, 1, null, View_ɵa_5)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](4, 278528, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgForOf"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["IterableDiffers"]], { ngForOf: [0, "ngForOf"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵand"](16777216, null, null, 1, null, View_ɵa_9)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](6, 16384, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵand"](16777216, null, null, 1, null, View_ɵa_10)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](8, 16384, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null)], function (_ck, _v) { var _co = _v.component; var currVal_1 = _co.getImages(); _ck(_v, 2, 0, currVal_1); var currVal_2 = _co.getImages(); _ck(_v, 4, 0, currVal_2); var currVal_3 = _co.bullets; _ck(_v, 6, 0, currVal_3); var currVal_4 = _co.arrows; _ck(_v, 8, 0, currVal_4); }, function (_ck, _v) { var _co = _v.component; var currVal_0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵinlineInterpolate"](2, "ngx-gallery-image-wrapper ngx-gallery-animation-", _co.animation, " ngx-gallery-image-size-", _co.size, ""); _ck(_v, 0, 0, currVal_0); }); }
function View_ɵa_Host_0(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](0, 0, null, null, 1, "ngx-gallery-image", [], null, [[null, "mouseenter"], [null, "mouseleave"]], function (_v, en, $event) { var ad = true; if (("mouseenter" === en)) {
        var pd_0 = (_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵnov"](_v, 1).onMouseEnter() !== false);
        ad = (pd_0 && ad);
    } if (("mouseleave" === en)) {
        var pd_1 = (_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵnov"](_v, 1).onMouseLeave() !== false);
        ad = (pd_1 && ad);
    } return ad; }, View_ɵa_0, RenderType_ɵa)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](1, 638976, null, 0, _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_1__["ɵa"], [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["DomSanitizer"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"], _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_1__["NgxGalleryService"]], null, null)], function (_ck, _v) { _ck(_v, 1, 0); }, null); }
var ɵaNgFactory = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵccf"]("ngx-gallery-image", _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_1__["ɵa"], View_ɵa_Host_0, { images: "images", clickable: "clickable", selectedIndex: "selectedIndex", arrows: "arrows", arrowsAutoHide: "arrowsAutoHide", swipe: "swipe", animation: "animation", size: "size", arrowPrevIcon: "arrowPrevIcon", arrowNextIcon: "arrowNextIcon", autoPlay: "autoPlay", autoPlayInterval: "autoPlayInterval", autoPlayPauseOnHover: "autoPlayPauseOnHover", infinityMove: "infinityMove", lazyLoading: "lazyLoading", actions: "actions", descriptions: "descriptions", showDescription: "showDescription", bullets: "bullets" }, { imageClick: "imageClick", activeChange: "activeChange" }, []);

var styles_ɵb = [".ngx-gallery-icon[_ngcontent-%COMP%]{color:#fff;z-index:2000;display:inline-block;position:relative;margin-right:10px;margin-top:10px;font-size:25px;cursor:pointer;text-decoration:none}.ngx-gallery-icon[_ngcontent-%COMP%]   .ngx-gallery-icon-content[_ngcontent-%COMP%]{display:block}"];
var RenderType_ɵb = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵcrt"]({ encapsulation: 0, styles: styles_ɵb, data: {} });

function View_ɵb_0(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵvid"](2, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](0, 0, null, null, 1, "div", [["aria-hidden", "true"], ["class", "ngx-gallery-icon"]], [[2, "ngx-gallery-icon-disabled", null], [8, "title", 0]], [[null, "click"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("click" === en)) {
        var pd_0 = (_co.handleClick($event) !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](1, 0, null, null, 0, "i", [], [[8, "className", 0]], null, null, null, null))], null, function (_ck, _v) { var _co = _v.component; var currVal_0 = _co.disabled; var currVal_1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵinlineInterpolate"](1, "", _co.titleText, ""); _ck(_v, 0, 0, currVal_0, currVal_1); var currVal_2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵinlineInterpolate"](1, "ngx-gallery-icon-content ", _co.icon, ""); _ck(_v, 1, 0, currVal_2); }); }
function View_ɵb_Host_0(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵeld"](0, 0, null, null, 1, "ngx-gallery-action", [], null, null, null, View_ɵb_0, RenderType_ɵb)), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵdid"](1, 114688, null, 0, _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_1__["ɵb"], [], null, null)], function (_ck, _v) { _ck(_v, 1, 0); }, null); }
var ɵbNgFactory = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵccf"]("ngx-gallery-action", _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_1__["ɵb"], View_ɵb_Host_0, { icon: "icon", disabled: "disabled", titleText: "titleText" }, { closeClick: "closeClick" }, []);



/***/ }),

/***/ "./src/app/shop/product-details/product-details.component.ngfactory.js":
/*!*****************************************************************************!*\
  !*** ./src/app/shop/product-details/product-details.component.ngfactory.js ***!
  \*****************************************************************************/
/*! exports provided: RenderType_ProductDetailsComponent, View_ProductDetailsComponent_0, View_ProductDetailsComponent_Host_0, ProductDetailsComponentNgFactory */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RenderType_ProductDetailsComponent", function() { return RenderType_ProductDetailsComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "View_ProductDetailsComponent_0", function() { return View_ProductDetailsComponent_0; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "View_ProductDetailsComponent_Host_0", function() { return View_ProductDetailsComponent_Host_0; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductDetailsComponentNgFactory", function() { return ProductDetailsComponentNgFactory; });
/* harmony import */ var _product_details_component_scss_shim_ngstyle__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./product-details.component.scss.shim.ngstyle */ "./src/app/shop/product-details/product-details.component.scss.shim.ngstyle.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _node_modules_kolkov_ngx_gallery_kolkov_ngx_gallery_ngfactory__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/@kolkov/ngx-gallery/kolkov-ngx-gallery.ngfactory */ "./node_modules/@kolkov/ngx-gallery/kolkov-ngx-gallery.ngfactory.js");
/* harmony import */ var _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @kolkov/ngx-gallery */ "./node_modules/@kolkov/ngx-gallery/fesm2015/kolkov-ngx-gallery.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _product_details_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./product-details.component */ "./src/app/shop/product-details/product-details.component.ts");
/* harmony import */ var _shop_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../shop.service */ "./src/app/shop/shop.service.ts");
/* harmony import */ var _basket_basket_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../basket/basket.service */ "./src/app/basket/basket.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var xng_breadcrumb__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! xng-breadcrumb */ "./node_modules/xng-breadcrumb/fesm2015/xng-breadcrumb.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm2015/ngx-toastr.js");
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes,extraRequire}
 * tslint:disable
 */ 











var styles_ProductDetailsComponent = [_product_details_component_scss_shim_ngstyle__WEBPACK_IMPORTED_MODULE_0__["styles"]];
var RenderType_ProductDetailsComponent = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵcrt"]({ encapsulation: 0, styles: styles_ProductDetailsComponent, data: {} });

function View_ProductDetailsComponent_1(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 27, "div", [["class", "row "]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](1, 0, null, null, 3, "div", [["class", "col-6"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](2, 0, null, null, 2, "ngx-gallery", [["style", "display: inline-block; margin-bottom: 20px;"]], [[4, "width", null], [4, "height", null], [4, "transform", null]], [["window", "resize"]], function (_v, en, $event) { var ad = true; if (("window:resize" === en)) {
        var pd_0 = (_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 4).onResize() !== false);
        ad = (pd_0 && ad);
    } return ad; }, _node_modules_kolkov_ngx_gallery_kolkov_ngx_gallery_ngfactory__WEBPACK_IMPORTED_MODULE_2__["View_NgxGalleryComponent_0"], _node_modules_kolkov_ngx_gallery_kolkov_ngx_gallery_ngfactory__WEBPACK_IMPORTED_MODULE_2__["RenderType_NgxGalleryComponent"])), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵprd"](512, null, _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_3__["NgxGalleryService"], _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_3__["NgxGalleryService"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"]]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](4, 4571136, null, 0, _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_3__["NgxGalleryComponent"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_3__["NgxGalleryService"]], { options: [0, "options"], images: [1, "images"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](5, 0, null, null, 16, "div", [["class", "col-6"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](6, 0, null, null, 1, "h3", [], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](7, null, ["", ""])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](8, 0, null, null, 2, "p", [["style", "font-size: 2em;"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](9, null, ["", ""])), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵppd"](10, 1), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](11, 0, null, null, 6, "div", [["class", "d-flex justify-content-start align-items-center"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](12, 0, null, null, 0, "i", [["class", "fa fa-minus-circle text-warning mr-2"], ["style", "cursor: pointer; font-size: 2em;"]], null, [[null, "click"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("click" === en)) {
        var pd_0 = (_co.decrementQuantity() !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](13, 0, null, null, 1, "span", [["class", "font-weight-bold"], ["style", "font-size: 1.5em;"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](14, null, ["", ""])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](15, 0, null, null, 0, "i", [["class", "fa fa-plus-circle text-warning mx-2"], ["style", "cursor: pointer; font-size: 2em;"]], null, [[null, "click"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("click" === en)) {
        var pd_0 = (_co.incrementQuantity() !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](16, 0, null, null, 1, "button", [["class", "btn btn-outline-primary btn-lg ml-4"]], null, [[null, "click"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("click" === en)) {
        var pd_0 = (_co.addItemToBasket() !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["Add to Cart"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](18, 0, null, null, 3, "div", [["class", "d-flex justify-content-start align-items-center mt-5"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](19, 0, null, null, 2, "button", [["class", "btn btn-primary"]], null, [[null, "click"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("click" === en)) {
        var pd_0 = (_co.addFavourite(_co.product) !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](20, 0, null, null, 1, "i", [["class", "fa fa-heart"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, [" Add to Favourites"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](22, 0, null, null, 5, "div", [["class", "row mt-5"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](23, 0, null, null, 4, "div", [["class", "col-12 ml-3"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](24, 0, null, null, 1, "h4", [], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["Description"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](26, 0, null, null, 1, "p", [], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](27, null, ["", ""]))], function (_ck, _v) { var _co = _v.component; var currVal_3 = _co.galleryOptions; var currVal_4 = _co.galleryImages; _ck(_v, 4, 0, currVal_3, currVal_4); }, function (_ck, _v) { var _co = _v.component; var currVal_0 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 4).width; var currVal_1 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 4).height; var currVal_2 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v, 4).left; _ck(_v, 2, 0, currVal_0, currVal_1, currVal_2); var currVal_5 = _co.product.name; _ck(_v, 7, 0, currVal_5); var currVal_6 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵunv"](_v, 9, 0, _ck(_v, 10, 0, _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵnov"](_v.parent, 0), _co.product.price)); _ck(_v, 9, 0, currVal_6); var currVal_7 = _co.quantity; _ck(_v, 14, 0, currVal_7); var currVal_8 = _co.product.description; _ck(_v, 27, 0, currVal_8); }); }
function View_ProductDetailsComponent_0(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵpid"](0, _angular_common__WEBPACK_IMPORTED_MODULE_4__["CurrencyPipe"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["LOCALE_ID"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["DEFAULT_CURRENCY_CODE"]]), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](1, 0, null, null, 2, "div", [["class", "container mt-5"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵand"](16777216, null, null, 1, null, View_ProductDetailsComponent_1)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](3, 16384, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_4__["NgIf"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null)], function (_ck, _v) { var _co = _v.component; var currVal_0 = _co.product; _ck(_v, 3, 0, currVal_0); }, null); }
function View_ProductDetailsComponent_Host_0(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 1, "app-product-details", [], null, null, null, View_ProductDetailsComponent_0, RenderType_ProductDetailsComponent)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](1, 114688, null, 0, _product_details_component__WEBPACK_IMPORTED_MODULE_5__["ProductDetailsComponent"], [_shop_service__WEBPACK_IMPORTED_MODULE_6__["ShopService"], _basket_basket_service__WEBPACK_IMPORTED_MODULE_7__["BasketService"], _angular_router__WEBPACK_IMPORTED_MODULE_8__["ActivatedRoute"], xng_breadcrumb__WEBPACK_IMPORTED_MODULE_9__["BreadcrumbService"], ngx_toastr__WEBPACK_IMPORTED_MODULE_10__["ToastrService"]], null, null)], function (_ck, _v) { _ck(_v, 1, 0); }, null); }
var ProductDetailsComponentNgFactory = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵccf"]("app-product-details", _product_details_component__WEBPACK_IMPORTED_MODULE_5__["ProductDetailsComponent"], View_ProductDetailsComponent_Host_0, {}, {}, []);



/***/ }),

/***/ "./src/app/shop/product-details/product-details.component.scss.shim.ngstyle.js":
/*!*************************************************************************************!*\
  !*** ./src/app/shop/product-details/product-details.component.scss.shim.ngstyle.js ***!
  \*************************************************************************************/
/*! exports provided: styles */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "styles", function() { return styles; });
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes,extraRequire}
 * tslint:disable
 */ 
var styles = ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Nob3AvcHJvZHVjdC1kZXRhaWxzL3Byb2R1Y3QtZGV0YWlscy5jb21wb25lbnQuc2NzcyJ9 */"];



/***/ }),

/***/ "./src/app/shop/product-details/product-details.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/shop/product-details/product-details.component.ts ***!
  \*******************************************************************/
/*! exports provided: ProductDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductDetailsComponent", function() { return ProductDetailsComponent; });
/* harmony import */ var _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @kolkov/ngx-gallery */ "./node_modules/@kolkov/ngx-gallery/fesm2015/kolkov-ngx-gallery.js");

class ProductDetailsComponent {
    constructor(shopService, basketService, activatedRoute, breadcrumbService, toastr) {
        this.shopService = shopService;
        this.basketService = basketService;
        this.activatedRoute = activatedRoute;
        this.breadcrumbService = breadcrumbService;
        this.toastr = toastr;
        this.quantity = 1;
        this.breadcrumbService.set('@productDetails', ' ');
    }
    ngOnInit() {
        this.loadProduct();
    }
    initializeGallery() {
        this.galleryOptions = [
            {
                width: '500px',
                height: '500px',
                imagePercent: 100,
                thumbnailsColumns: 4,
                imageArrows: true,
                imageAnimation: _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_0__["NgxGalleryAnimation"].Slide,
                imageSize: _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_0__["NgxGalleryImageSize"].Contain,
                thumbnailSize: _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_0__["NgxGalleryImageSize"].Contain,
                preview: false
            }
        ];
    }
    getImages() {
        const imageUrls = [];
        console.log(this.product.photos);
        for (const photo of this.product.photos) {
            imageUrls.push({
                small: photo === null || photo === void 0 ? void 0 : photo.photoUrl,
                medium: photo === null || photo === void 0 ? void 0 : photo.photoUrl,
                big: photo === null || photo === void 0 ? void 0 : photo.photoUrl
            });
        }
        return imageUrls;
    }
    loadProduct() {
        // + -> to convert from string from URL to integer
        this.shopService.getProduct(+this.activatedRoute.snapshot.paramMap.get('id'))
            .subscribe(response => {
            this.product = response;
            this.breadcrumbService.set('@productDetails', this.product.name); // alias for breadcrumb in shop-routing module
            this.initializeGallery();
            this.galleryImages = this.getImages();
        }, error => {
            console.log(error);
        });
    }
    addItemToBasket() {
        this.basketService.addItemToBAsket(this.product, this.quantity);
    }
    incrementQuantity() {
        this.quantity++;
    }
    decrementQuantity() {
        if (this.quantity > 1) {
            this.quantity--;
        }
    }
    addFavourite(product) {
        this.shopService.addFavourite(product.id).subscribe(() => {
            this.toastr.success('Item ' + product.name + ' is marked as Favourite.');
        });
    }
}


/***/ }),

/***/ "./src/app/shop/shop-routing.module.ts":
/*!*********************************************!*\
  !*** ./src/app/shop/shop-routing.module.ts ***!
  \*********************************************/
/*! exports provided: ShopRoutingModule, ɵ0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShopRoutingModule", function() { return ShopRoutingModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵ0", function() { return ɵ0; });
/* harmony import */ var _shop_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./shop.component */ "./src/app/shop/shop.component.ts");
/* harmony import */ var _product_details_product_details_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./product-details/product-details.component */ "./src/app/shop/product-details/product-details.component.ts");


const ɵ0 = {
    breadcrumb: { alias: 'productDetails' }
};
const routes = [
    { path: '', component: _shop_component__WEBPACK_IMPORTED_MODULE_0__["ShopComponent"] },
    {
        path: ':id', component: _product_details_product_details_component__WEBPACK_IMPORTED_MODULE_1__["ProductDetailsComponent"],
        data: ɵ0
    }
];
class ShopRoutingModule {
}



/***/ }),

/***/ "./src/app/shop/shop.component.ngfactory.js":
/*!**************************************************!*\
  !*** ./src/app/shop/shop.component.ngfactory.js ***!
  \**************************************************/
/*! exports provided: RenderType_ShopComponent, View_ShopComponent_0, View_ShopComponent_Host_0, ShopComponentNgFactory */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RenderType_ShopComponent", function() { return RenderType_ShopComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "View_ShopComponent_0", function() { return View_ShopComponent_0; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "View_ShopComponent_Host_0", function() { return View_ShopComponent_Host_0; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShopComponentNgFactory", function() { return ShopComponentNgFactory; });
/* harmony import */ var _shop_component_scss_shim_ngstyle__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./shop.component.scss.shim.ngstyle */ "./src/app/shop/shop.component.scss.shim.ngstyle.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _product_item_product_item_component_ngfactory__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./product-item/product-item.component.ngfactory */ "./src/app/shop/product-item/product-item.component.ngfactory.js");
/* harmony import */ var _product_item_product_item_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./product-item/product-item.component */ "./src/app/shop/product-item/product-item.component.ts");
/* harmony import */ var _basket_basket_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../basket/basket.service */ "./src/app/basket/basket.service.ts");
/* harmony import */ var _shared_components_pager_pager_component_ngfactory__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../shared/components/pager/pager.component.ngfactory */ "./src/app/shared/components/pager/pager.component.ngfactory.js");
/* harmony import */ var _shared_components_pager_pager_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../shared/components/pager/pager.component */ "./src/app/shared/components/pager/pager.component.ts");
/* harmony import */ var _shared_components_paging_header_paging_header_component_ngfactory__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../shared/components/paging-header/paging-header.component.ngfactory */ "./src/app/shared/components/paging-header/paging-header.component.ngfactory.js");
/* harmony import */ var _shared_components_paging_header_paging_header_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../shared/components/paging-header/paging-header.component */ "./src/app/shared/components/paging-header/paging-header.component.ts");
/* harmony import */ var _shop_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./shop.component */ "./src/app/shop/shop.component.ts");
/* harmony import */ var _shop_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./shop.service */ "./src/app/shop/shop.service.ts");
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes,extraRequire}
 * tslint:disable
 */ 













var styles_ShopComponent = [_shop_component_scss_shim_ngstyle__WEBPACK_IMPORTED_MODULE_0__["styles"]];
var RenderType_ShopComponent = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵcrt"]({ encapsulation: 0, styles: styles_ShopComponent, data: {} });

function View_ShopComponent_2(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 3, "option", [], [[8, "selected", 0]], null, null, null, null)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](1, 147456, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NgSelectOption"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"], [8, null]], { value: [0, "value"] }, null), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](2, 147456, null, 0, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ɵangular_packages_forms_forms_x"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"], [8, null]], { value: [0, "value"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](3, null, [" ", " "]))], function (_ck, _v) { var currVal_1 = _v.context.$implicit.value; _ck(_v, 1, 0, currVal_1); var currVal_2 = _v.context.$implicit.value; _ck(_v, 2, 0, currVal_2); }, function (_ck, _v) { var _co = _v.component; var currVal_0 = (_co.shopParams.sortSelected === _v.context.$implicit.value); _ck(_v, 0, 0, currVal_0); var currVal_3 = _v.context.$implicit.name; _ck(_v, 3, 0, currVal_3); }); }
function View_ShopComponent_3(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 1, "li", [["class", "list-group-item"]], [[2, "active", null], [8, "value", 0]], [[null, "click"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("click" === en)) {
        var pd_0 = (_co.onBrandSelected(_v.context.$implicit.id) !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](1, null, [" ", " "]))], null, function (_ck, _v) { var _co = _v.component; var currVal_0 = (_v.context.$implicit.id === _co.shopParams.brandIdSelected); var currVal_1 = _v.context.$implicit.id; _ck(_v, 0, 0, currVal_0, currVal_1); var currVal_2 = _v.context.$implicit.name; _ck(_v, 1, 0, currVal_2); }); }
function View_ShopComponent_4(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 1, "li", [["class", "list-group-item"]], [[2, "active", null], [8, "value", 0]], [[null, "click"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("click" === en)) {
        var pd_0 = (_co.onTypeSelected(_v.context.$implicit.id) !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](1, null, [" ", " "]))], null, function (_ck, _v) { var _co = _v.component; var currVal_0 = (_v.context.$implicit.id === _co.shopParams.typeIdSelected); var currVal_1 = _v.context.$implicit.id; _ck(_v, 0, 0, currVal_0, currVal_1); var currVal_2 = _v.context.$implicit.name; _ck(_v, 1, 0, currVal_2); }); }
function View_ShopComponent_1(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 15, null, null, null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](1, 0, null, null, 1, "h5", [["class", "text-warning ml-3 mt-4 mb-3"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["Sort"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](3, 0, null, null, 2, "select", [["class", "custom-select mb-4"]], null, [[null, "change"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("change" === en)) {
        var pd_0 = (_co.onSortSelected($event.target.value) !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵand"](16777216, null, null, 1, null, View_ShopComponent_2)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](5, 278528, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_3__["NgForOf"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["TemplateRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["IterableDiffers"]], { ngForOf: [0, "ngForOf"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](6, 0, null, null, 1, "h5", [["class", "text-warning ml-3 my-3"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["Brands"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](8, 0, null, null, 2, "ul", [["class", "list-group"], ["my-3", ""]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵand"](16777216, null, null, 1, null, View_ShopComponent_3)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](10, 278528, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_3__["NgForOf"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["TemplateRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["IterableDiffers"]], { ngForOf: [0, "ngForOf"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](11, 0, null, null, 1, "h5", [["class", "text-warning ml-3 mt-3"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["Types"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](13, 0, null, null, 2, "ul", [["class", "list-group my-3"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵand"](16777216, null, null, 1, null, View_ShopComponent_4)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](15, 278528, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_3__["NgForOf"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["TemplateRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["IterableDiffers"]], { ngForOf: [0, "ngForOf"] }, null)], function (_ck, _v) { var _co = _v.component; var currVal_0 = _co.sortOptions; _ck(_v, 5, 0, currVal_0); var currVal_1 = _co.brands; _ck(_v, 10, 0, currVal_1); var currVal_2 = _co.types; _ck(_v, 15, 0, currVal_2); }, null); }
function View_ShopComponent_5(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 5, "div", [["class", "form-inline"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](1, 0, [[1, 0], ["search", 1]], null, 0, "input", [["class", "form-control mr-2"], ["placeholder", "Search"], ["style", "width: 300px"], ["type", "text"]], null, [[null, "keyup.enter"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("keyup.enter" === en)) {
        var pd_0 = (_co.onSearch() !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](2, 0, null, null, 1, "button", [["class", "btn btn-outline-primary mr-1 my-2"]], null, [[null, "click"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("click" === en)) {
        var pd_0 = (_co.onSearch() !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["Search"])), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](4, 0, null, null, 1, "button", [["class", "btn btn-outline-success ml-1 my-2"]], null, [[null, "click"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("click" === en)) {
        var pd_0 = (_co.onReset() !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵted"](-1, null, ["Reset"]))], null, null); }
function View_ShopComponent_6(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 2, "div", [["class", "col-4 mb-4"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](1, 0, null, null, 1, "app-product-item", [], null, null, null, _product_item_product_item_component_ngfactory__WEBPACK_IMPORTED_MODULE_4__["View_ProductItemComponent_0"], _product_item_product_item_component_ngfactory__WEBPACK_IMPORTED_MODULE_4__["RenderType_ProductItemComponent"])), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](2, 114688, null, 0, _product_item_product_item_component__WEBPACK_IMPORTED_MODULE_5__["ProductItemComponent"], [_basket_basket_service__WEBPACK_IMPORTED_MODULE_6__["BasketService"]], { product: [0, "product"] }, null)], function (_ck, _v) { var currVal_0 = _v.context.$implicit; _ck(_v, 2, 0, currVal_0); }, null); }
function View_ShopComponent_7(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 2, "div", [["class", "d-flex justify-content-center"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](1, 0, null, null, 1, "app-pager", [], null, [[null, "pageChanged"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("pageChanged" === en)) {
        var pd_0 = (_co.onPageChanged($event) !== false);
        ad = (pd_0 && ad);
    } return ad; }, _shared_components_pager_pager_component_ngfactory__WEBPACK_IMPORTED_MODULE_7__["View_PagerComponent_0"], _shared_components_pager_pager_component_ngfactory__WEBPACK_IMPORTED_MODULE_7__["RenderType_PagerComponent"])), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](2, 114688, null, 0, _shared_components_pager_pager_component__WEBPACK_IMPORTED_MODULE_8__["PagerComponent"], [], { totalCount: [0, "totalCount"], pageSize: [1, "pageSize"], pageNumber: [2, "pageNumber"] }, { pageChanged: "pageChanged" })], function (_ck, _v) { var _co = _v.component; var currVal_0 = _co.totalCount; var currVal_1 = _co.shopParams.pageSize; var currVal_2 = _co.shopParams.pageNumber; _ck(_v, 2, 0, currVal_0, currVal_1, currVal_2); }, null); }
function View_ShopComponent_0(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵqud"](671088640, 1, { searchTerm: 0 }), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](1, 0, null, null, 16, "div", [["class", "container mt-3"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](2, 0, null, null, 15, "div", [["class", "row"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](3, 0, null, null, 2, "section", [["class", "col-3"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵand"](16777216, null, null, 1, null, View_ShopComponent_1)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](5, 16384, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_3__["NgIf"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](6, 0, null, null, 11, "section", [["class", "col-9"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](7, 0, null, null, 10, "div", [["class", "container"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](8, 0, null, null, 4, "div", [["class", "d-flex justify-content-between align-items-center pb-2"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](9, 0, null, null, 1, "app-paging-header", [], null, null, null, _shared_components_paging_header_paging_header_component_ngfactory__WEBPACK_IMPORTED_MODULE_9__["View_PagingHeaderComponent_0"], _shared_components_paging_header_paging_header_component_ngfactory__WEBPACK_IMPORTED_MODULE_9__["RenderType_PagingHeaderComponent"])), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](10, 114688, null, 0, _shared_components_paging_header_paging_header_component__WEBPACK_IMPORTED_MODULE_10__["PagingHeaderComponent"], [], { pageNumber: [0, "pageNumber"], pageSize: [1, "pageSize"], totalCount: [2, "totalCount"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵand"](16777216, null, null, 1, null, View_ShopComponent_5)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](12, 16384, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_3__["NgIf"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](13, 0, null, null, 2, "div", [["class", "row"]], null, null, null, null, null)), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵand"](16777216, null, null, 1, null, View_ShopComponent_6)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](15, 278528, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_3__["NgForOf"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["TemplateRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["IterableDiffers"]], { ngForOf: [0, "ngForOf"] }, null), (_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵand"](16777216, null, null, 1, null, View_ShopComponent_7)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](17, 16384, null, 0, _angular_common__WEBPACK_IMPORTED_MODULE_3__["NgIf"], [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewContainerRef"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["TemplateRef"]], { ngIf: [0, "ngIf"] }, null)], function (_ck, _v) { var _co = _v.component; var currVal_0 = (_co.types && _co.brands); _ck(_v, 5, 0, currVal_0); var currVal_1 = _co.shopParams.pageNumber; var currVal_2 = _co.shopParams.pageSize; var currVal_3 = _co.totalCount; _ck(_v, 10, 0, currVal_1, currVal_2, currVal_3); var currVal_4 = _co.products; _ck(_v, 12, 0, currVal_4); var currVal_5 = _co.products; _ck(_v, 15, 0, currVal_5); var currVal_6 = (_co.totalCount > 0); _ck(_v, 17, 0, currVal_6); }, null); }
function View_ShopComponent_Host_0(_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵvid"](0, [(_l()(), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵeld"](0, 0, null, null, 1, "app-shop", [], null, null, null, View_ShopComponent_0, RenderType_ShopComponent)), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵdid"](1, 114688, null, 0, _shop_component__WEBPACK_IMPORTED_MODULE_11__["ShopComponent"], [_shop_service__WEBPACK_IMPORTED_MODULE_12__["ShopService"]], null, null)], function (_ck, _v) { _ck(_v, 1, 0); }, null); }
var ShopComponentNgFactory = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵccf"]("app-shop", _shop_component__WEBPACK_IMPORTED_MODULE_11__["ShopComponent"], View_ShopComponent_Host_0, {}, {}, []);



/***/ }),

/***/ "./src/app/shop/shop.component.scss.shim.ngstyle.js":
/*!**********************************************************!*\
  !*** ./src/app/shop/shop.component.scss.shim.ngstyle.js ***!
  \**********************************************************/
/*! exports provided: styles */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "styles", function() { return styles; });
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes,extraRequire}
 * tslint:disable
 */ 
var styles = [".list-group-item[_ngcontent-%COMP%] {\n  cursor: pointer;\n  border: none;\n  padding: 10px 20px;\n  font-size: 1.1em;\n}\n.list-group-item[_ngcontent-%COMP%]:focus {\n  outline: none;\n}\n.list-group-item.active[_ngcontent-%COMP%] {\n  border-radius: 10px;\n}\n.list-group-item[_ngcontent-%COMP%]:not(.active):hover {\n  color: #fff;\n  background-color: #99e6ff;\n  border-radius: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hvcC9zaG9wLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsZUFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0FBQ0Y7QUFDRTtFQUNFLGFBQUE7QUFDSjtBQUVFO0VBQ0UsbUJBQUE7QUFBSjtBQUdFO0VBQ0UsV0FBQTtFQUNBLHlCQUFBO0VBQ0EsbUJBQUE7QUFESiIsImZpbGUiOiJzcmMvYXBwL3Nob3Avc2hvcC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5saXN0LWdyb3VwLWl0ZW0ge1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxuICBib3JkZXI6IG5vbmU7XHJcbiAgcGFkZGluZzogMTBweCAyMHB4O1xyXG4gIGZvbnQtc2l6ZTogMS4xZW07XHJcblxyXG4gICY6Zm9jdXMge1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICB9XHJcblxyXG4gICYuYWN0aXZlIHtcclxuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgfVxyXG5cclxuICAmOm5vdCguYWN0aXZlKTpob3ZlciB7XHJcbiAgICBjb2xvcjogI2ZmZjtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICM5OWU2ZmY7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gIH1cclxufVxyXG5cclxuXHJcbiJdfQ== */"];



/***/ }),

/***/ "./src/app/shop/shop.component.ts":
/*!****************************************!*\
  !*** ./src/app/shop/shop.component.ts ***!
  \****************************************/
/*! exports provided: ShopComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShopComponent", function() { return ShopComponent; });
/* harmony import */ var _shared_models_shopParams__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../shared/models/shopParams */ "./src/app/shared/models/shopParams.ts");

class ShopComponent {
    constructor(shopService) {
        this.shopService = shopService;
        this.sortOptions = [
            { name: 'Alphabetical', value: 'name' },
            { name: 'Price: Low to High', value: 'priceAsc' },
            { name: 'Price: High to Low', value: 'priceDesc' }
        ];
        this.shopParams = this.shopService.getShopParams();
    }
    ngOnInit() {
        this.getProducts(true); // get products from the cache
        this.getBrands();
        this.getTypes();
    }
    getProducts(useCache = false) {
        this.shopService.getProducts(useCache).subscribe(response => {
            this.products = response.data;
            this.totalCount = response.count;
        }, error => {
            console.log(error);
        });
    }
    getBrands() {
        this.shopService.getBrands().subscribe(response => {
            this.brands = [{ id: 0, name: 'All' }, ...response];
        }, error => {
            console.log(error);
        });
    }
    getTypes() {
        this.shopService.getTypes().subscribe(response => {
            // ...response = spread operator and it adds another object at the array,
            // so we will have another type object: id = 0, name = all
            this.types = [{ id: 0, name: 'All' }, ...response];
        }, error => {
            console.log(error);
        });
    }
    onBrandSelected(brandId) {
        const params = this.shopService.getShopParams(); // Set params in our service, not locally
        params.brandIdSelected = brandId;
        params.pageNumber = 1; // when filtering, page should start with 1.
        this.shopService.setShopParams(params);
        this.getProducts();
    }
    onTypeSelected(typeId) {
        const params = this.shopService.getShopParams();
        params.typeIdSelected = typeId;
        params.pageNumber = 1;
        this.shopService.setShopParams(params);
        console.log(params);
        this.getProducts();
    }
    onSortSelected(sort) {
        const params = this.shopService.getShopParams();
        params.sortSelected = sort;
        this.shopService.setShopParams(params);
        this.getProducts();
    }
    onPageChanged(event) {
        // if needed to repair a bug...
        const params = this.shopService.getShopParams();
        if (params.pageNumber !== event) {
            params.pageNumber = event; // we're passing the pageNumber from pager component because  the event pageChanged() emits a number     
            this.shopService.setShopParams(params);
            this.getProducts(true); // get products from cache
        }
    }
    onSearch() {
        const params = this.shopService.getShopParams();
        params.search = this.searchTerm.nativeElement.value;
        params.pageNumber = 1;
        this.shopService.setShopParams(params);
        this.getProducts();
    }
    onReset() {
        this.searchTerm.nativeElement.value = '';
        this.shopParams = new _shared_models_shopParams__WEBPACK_IMPORTED_MODULE_0__["ShopParams"](); // will initialize locally all params with default values
        this.shopService.setShopParams(this.shopParams);
        this.getProducts(); // get the unfiltered products
    }
}


/***/ }),

/***/ "./src/app/shop/shop.module.ngfactory.js":
/*!***********************************************!*\
  !*** ./src/app/shop/shop.module.ngfactory.js ***!
  \***********************************************/
/*! exports provided: ShopModuleNgFactory */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShopModuleNgFactory", function() { return ShopModuleNgFactory; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _shop_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./shop.module */ "./src/app/shop/shop.module.ts");
/* harmony import */ var _node_modules_ngx_bootstrap_dropdown_ngx_bootstrap_dropdown_ngfactory__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/ngx-bootstrap/dropdown/ngx-bootstrap-dropdown.ngfactory */ "./node_modules/ngx-bootstrap/dropdown/ngx-bootstrap-dropdown.ngfactory.js");
/* harmony import */ var _node_modules_angular_router_router_ngfactory__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/@angular/router/router.ngfactory */ "./node_modules/@angular/router/router.ngfactory.js");
/* harmony import */ var _node_modules_ngx_bootstrap_modal_ngx_bootstrap_modal_ngfactory__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../node_modules/ngx-bootstrap/modal/ngx-bootstrap-modal.ngfactory */ "./node_modules/ngx-bootstrap/modal/ngx-bootstrap-modal.ngfactory.js");
/* harmony import */ var _shop_component_ngfactory__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./shop.component.ngfactory */ "./src/app/shop/shop.component.ngfactory.js");
/* harmony import */ var _product_details_product_details_component_ngfactory__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./product-details/product-details.component.ngfactory */ "./src/app/shop/product-details/product-details.component.ngfactory.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @kolkov/ngx-gallery */ "./node_modules/@kolkov/ngx-gallery/fesm2015/kolkov-ngx-gallery.js");
/* harmony import */ var ngx_bootstrap_positioning__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ngx-bootstrap/positioning */ "./node_modules/ngx-bootstrap/positioning/fesm2015/ngx-bootstrap-positioning.js");
/* harmony import */ var ngx_bootstrap_component_loader__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ngx-bootstrap/component-loader */ "./node_modules/ngx-bootstrap/component-loader/fesm2015/ngx-bootstrap-component-loader.js");
/* harmony import */ var ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ngx-bootstrap/dropdown */ "./node_modules/ngx-bootstrap/dropdown/fesm2015/ngx-bootstrap-dropdown.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm2015/ngx-bootstrap-modal.js");
/* harmony import */ var ngx_bootstrap_pagination__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ngx-bootstrap/pagination */ "./node_modules/ngx-bootstrap/pagination/fesm2015/ngx-bootstrap-pagination.js");
/* harmony import */ var ngx_bootstrap_carousel__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ngx-bootstrap/carousel */ "./node_modules/ngx-bootstrap/carousel/fesm2015/ngx-bootstrap-carousel.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_cdk_bidi__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/cdk/bidi */ "./node_modules/@angular/cdk/fesm2015/bidi.js");
/* harmony import */ var _angular_cdk_stepper__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/cdk/stepper */ "./node_modules/@angular/cdk/fesm2015/stepper.js");
/* harmony import */ var ng2_currency_mask_src_currency_mask_module__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ng2-currency-mask/src/currency-mask.module */ "./node_modules/ng2-currency-mask/src/currency-mask.module.js");
/* harmony import */ var ng2_currency_mask_src_currency_mask_module__WEBPACK_IMPORTED_MODULE_20___default = /*#__PURE__*/__webpack_require__.n(ng2_currency_mask_src_currency_mask_module__WEBPACK_IMPORTED_MODULE_20__);
/* harmony import */ var ngx_bootstrap_tabs__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ngx-bootstrap/tabs */ "./node_modules/ngx-bootstrap/tabs/fesm2015/ngx-bootstrap-tabs.js");
/* harmony import */ var ngx_dropzone__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ngx-dropzone */ "./node_modules/ngx-dropzone/fesm2015/ngx-dropzone.js");
/* harmony import */ var ngx_image_cropper__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ngx-image-cropper */ "./node_modules/ngx-image-cropper/fesm2015/ngx-image-cropper.js");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ../shared/shared.module */ "./src/app/shared/shared.module.ts");
/* harmony import */ var _shop_routing_module__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./shop-routing.module */ "./src/app/shop/shop-routing.module.ts");
/* harmony import */ var _shop_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./shop.component */ "./src/app/shop/shop.component.ts");
/* harmony import */ var _product_details_product_details_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./product-details/product-details.component */ "./src/app/shop/product-details/product-details.component.ts");
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes,extraRequire}
 * tslint:disable
 */ 




























var ShopModuleNgFactory = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵcmf"](_shop_module__WEBPACK_IMPORTED_MODULE_1__["ShopModule"], [], function (_l) { return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmod"]([_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](512, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ComponentFactoryResolver"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵCodegenComponentFactoryResolver"], [[8, [_node_modules_ngx_bootstrap_dropdown_ngx_bootstrap_dropdown_ngfactory__WEBPACK_IMPORTED_MODULE_2__["BsDropdownContainerComponentNgFactory"], _node_modules_angular_router_router_ngfactory__WEBPACK_IMPORTED_MODULE_3__["ɵangular_packages_router_router_lNgFactory"], _node_modules_ngx_bootstrap_modal_ngx_bootstrap_modal_ngfactory__WEBPACK_IMPORTED_MODULE_4__["ModalBackdropComponentNgFactory"], _node_modules_ngx_bootstrap_modal_ngx_bootstrap_modal_ngfactory__WEBPACK_IMPORTED_MODULE_4__["ModalContainerComponentNgFactory"], _shop_component_ngfactory__WEBPACK_IMPORTED_MODULE_5__["ShopComponentNgFactory"], _product_details_product_details_component_ngfactory__WEBPACK_IMPORTED_MODULE_6__["ProductDetailsComponentNgFactory"]]], [3, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ComponentFactoryResolver"]], _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModuleRef"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _angular_common__WEBPACK_IMPORTED_MODULE_7__["NgLocalization"], _angular_common__WEBPACK_IMPORTED_MODULE_7__["NgLocaleLocalization"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["LOCALE_ID"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormBuilder"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormBuilder"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ɵangular_packages_forms_forms_n"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ɵangular_packages_forms_forms_n"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, _angular_platform_browser__WEBPACK_IMPORTED_MODULE_9__["HAMMER_GESTURE_CONFIG"], _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_10__["CustomHammerConfig"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, ngx_bootstrap_positioning__WEBPACK_IMPORTED_MODULE_11__["PositioningService"], ngx_bootstrap_positioning__WEBPACK_IMPORTED_MODULE_11__["PositioningService"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["RendererFactory2"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["PLATFORM_ID"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, ngx_bootstrap_component_loader__WEBPACK_IMPORTED_MODULE_12__["ComponentLoaderFactory"], ngx_bootstrap_component_loader__WEBPACK_IMPORTED_MODULE_12__["ComponentLoaderFactory"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ComponentFactoryResolver"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injector"], ngx_bootstrap_positioning__WEBPACK_IMPORTED_MODULE_11__["PositioningService"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ApplicationRef"]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_13__["BsDropdownState"], ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_13__["BsDropdownState"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](4608, ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_14__["BsModalService"], ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_14__["BsModalService"], [_angular_core__WEBPACK_IMPORTED_MODULE_0__["RendererFactory2"], ngx_bootstrap_component_loader__WEBPACK_IMPORTED_MODULE_12__["ComponentLoaderFactory"], [2, ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_14__["ɵa"]]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _angular_common__WEBPACK_IMPORTED_MODULE_7__["CommonModule"], _angular_common__WEBPACK_IMPORTED_MODULE_7__["CommonModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, ngx_bootstrap_pagination__WEBPACK_IMPORTED_MODULE_15__["PaginationModule"], ngx_bootstrap_pagination__WEBPACK_IMPORTED_MODULE_15__["PaginationModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, ngx_bootstrap_carousel__WEBPACK_IMPORTED_MODULE_16__["CarouselModule"], ngx_bootstrap_carousel__WEBPACK_IMPORTED_MODULE_16__["CarouselModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_13__["BsDropdownModule"], ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_13__["BsDropdownModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ɵangular_packages_forms_forms_d"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ɵangular_packages_forms_forms_d"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ReactiveFormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ReactiveFormsModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormsModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _angular_router__WEBPACK_IMPORTED_MODULE_17__["RouterModule"], _angular_router__WEBPACK_IMPORTED_MODULE_17__["RouterModule"], [[2, _angular_router__WEBPACK_IMPORTED_MODULE_17__["ɵangular_packages_router_router_a"]], [2, _angular_router__WEBPACK_IMPORTED_MODULE_17__["Router"]]]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _angular_cdk_bidi__WEBPACK_IMPORTED_MODULE_18__["BidiModule"], _angular_cdk_bidi__WEBPACK_IMPORTED_MODULE_18__["BidiModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _angular_cdk_stepper__WEBPACK_IMPORTED_MODULE_19__["CdkStepperModule"], _angular_cdk_stepper__WEBPACK_IMPORTED_MODULE_19__["CdkStepperModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_14__["ModalModule"], ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_14__["ModalModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, ng2_currency_mask_src_currency_mask_module__WEBPACK_IMPORTED_MODULE_20__["CurrencyMaskModule"], ng2_currency_mask_src_currency_mask_module__WEBPACK_IMPORTED_MODULE_20__["CurrencyMaskModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, ngx_bootstrap_tabs__WEBPACK_IMPORTED_MODULE_21__["TabsModule"], ngx_bootstrap_tabs__WEBPACK_IMPORTED_MODULE_21__["TabsModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_10__["NgxGalleryModule"], _kolkov_ngx_gallery__WEBPACK_IMPORTED_MODULE_10__["NgxGalleryModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, ngx_dropzone__WEBPACK_IMPORTED_MODULE_22__["NgxDropzoneModule"], ngx_dropzone__WEBPACK_IMPORTED_MODULE_22__["NgxDropzoneModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, ngx_image_cropper__WEBPACK_IMPORTED_MODULE_23__["ImageCropperModule"], ngx_image_cropper__WEBPACK_IMPORTED_MODULE_23__["ImageCropperModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _shared_shared_module__WEBPACK_IMPORTED_MODULE_24__["SharedModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_24__["SharedModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _shop_routing_module__WEBPACK_IMPORTED_MODULE_25__["ShopRoutingModule"], _shop_routing_module__WEBPACK_IMPORTED_MODULE_25__["ShopRoutingModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1073742336, _shop_module__WEBPACK_IMPORTED_MODULE_1__["ShopModule"], _shop_module__WEBPACK_IMPORTED_MODULE_1__["ShopModule"], []), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵmpd"](1024, _angular_router__WEBPACK_IMPORTED_MODULE_17__["ROUTES"], function () { return [[{ path: "", component: _shop_component__WEBPACK_IMPORTED_MODULE_26__["ShopComponent"] }, { path: ":id", component: _product_details_product_details_component__WEBPACK_IMPORTED_MODULE_27__["ProductDetailsComponent"], data: _shop_routing_module__WEBPACK_IMPORTED_MODULE_25__["ɵ0"] }]]; }, [])]); });



/***/ }),

/***/ "./src/app/shop/shop.module.ts":
/*!*************************************!*\
  !*** ./src/app/shop/shop.module.ts ***!
  \*************************************/
/*! exports provided: ShopModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShopModule", function() { return ShopModule; });
class ShopModule {
}


/***/ })

}]);
//# sourceMappingURL=shop-shop-module-ngfactory.js.map