﻿using API.Dtos;
using API.Errors;
using API.Extensions;
using API.Helpers;
using AutoMapper;
using Core.Entities.Identity;
using Core.Interfaces;
using EmailService;
using Message = EmailService.Message;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.Options;

namespace API.Controllers
{
    public class AccountController : BaseApiController
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly SignInManager<AppUser> _signInManager;
        private readonly ITokenService _tokenService;
        private readonly IMapper _mapper;
        private readonly IEmailSender _emailSender;
        private readonly IConfiguration _config;
        private readonly AppSettings _appSettings;

        public AccountController(UserManager<AppUser> userManager, SignInManager<AppUser> signInManager, ITokenService tokenService, IMapper mapper, IEmailSender emailSender,
            IConfiguration config, IOptions<AppSettings> appSettings)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _tokenService = tokenService;
            _mapper = mapper;
            _emailSender = emailSender;
            _config = config;
            _appSettings = appSettings.Value;
        }



        [HttpPost("login")]
        public async Task<ActionResult<UserDto>> Login(LoginDto loginDto)
        {
            var user = await _userManager.FindByEmailAsync(loginDto.Email);

            if (user == null)
                return Unauthorized(new ApiResponse(401));

            // if password for username is wrong
            var result = await _signInManager.CheckPasswordSignInAsync(user, loginDto.Password, false);

            // could set 3 attempts for sign-in

            //if (!result.Succeeded)     
            //    return Unauthorized(new ApiResponse(401));

            if (result == Microsoft.AspNetCore.Identity.SignInResult.Failed)
            {
                return Unauthorized("Invalid Password.");
            }
            else if (result == Microsoft.AspNetCore.Identity.SignInResult.NotAllowed)
            {
                return Unauthorized("User cannot sign in without a confirmed email.");
            }

            return new UserDto
            {
                Email = user.Email,
                Token = await _tokenService.CreateToken(user),
                DisplayName = user.DisplayName
            };

        }



        [HttpPost("register")]
        public async Task<ActionResult<UserDto>> Register(RegisterDto registerDto)
        {
            if (CheckEmailExistsAsync(registerDto.Email).Result.Value)
            {
                return new BadRequestObjectResult(new ApiValidationErrorResponse
                { Errors = new[] { "Email address is in use." } });
            }

            var user = new AppUser
            {
                DisplayName = registerDto.DisplayName,
                Email = registerDto.Email,
                UserName = registerDto.Email
            };

            var result = await _userManager.CreateAsync(user, registerDto.Password);

            if (!result.Succeeded)
                return BadRequest(result.Errors);

            // Confirm email
         
            var token = await _userManager.GenerateEmailConfirmationTokenAsync(user);
            //  var uriBuilder = new UriBuilder(_config["returnPaths:ConfirmEmail"]);
            string urlPath = "";
            var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            if (env.ToLower() == "development")
            {
                urlPath = _config["returnPaths:ConfirmEmail"];
            }
            else
            {
                urlPath = Environment.GetEnvironmentVariable("ReturnPaths:ConfirmEmail");
            }
            //var uriBuilder = new UriBuilder(urlPath);

            //var query = HttpUtility.ParseQueryString(uriBuilder.Query);
            //query["token"] = token;
            //query["userid"] = user.Id.ToString();
            //uriBuilder.Query = query.ToString();
            //var confirmationLink = uriBuilder.ToString();

            //var message = new Message(new string[] { user.Email }, "Confirmation email link", confirmationLink, null);

            var confirmationLink = URLBuilder.BuildUrl(urlPath, token, user.Id.ToString());

            var message = new Message(new string[] { user.Email }, "Confirmation Email Link",
                URLBuilder.BuildContext(URLBuilder.MessageType.ConfirmEmail, confirmationLink, user.UserName), null);

            await _emailSender.SendEmailAsync(message);



            var roleResult = await _userManager.AddToRoleAsync(user, "Member");

            if (!roleResult.Succeeded) return BadRequest(result.Errors);

            return new UserDto
            {
                DisplayName = user.DisplayName,
                Token = await _tokenService.CreateToken(user),
                Email = user.Email
            };
        }



        [Authorize]
        [HttpGet]
        public async Task<ActionResult<UserDto>> GetCurrentUser()
        {
            var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);

            return new UserDto
            {
                DisplayName = user.DisplayName,
                Token = await _tokenService.CreateToken(user),
                Email = user.Email,
                Favourites = user.FavouriteProducts
            };
        }



        [HttpGet("emailexists")]
        public async Task<ActionResult<bool>> CheckEmailExistsAsync([FromQuery] string email)
        {
            return await _userManager.FindByEmailAsync(email) != null;
        }




        [Authorize]
        [HttpGet("address")]
        public async Task<ActionResult<AddressDto>> GetUserAdddress()
        {
            var user = await _userManager.FindUserByClaimsPrincipleWithAddressAsync(HttpContext.User);

            return _mapper.Map<Address, AddressDto>(user.Address);
        }



        [Authorize]
        [HttpPut("address")]
        public async Task<ActionResult<AddressDto>> UpdateUserAddress(AddressDto address)
        {
            var user = await _userManager.FindUserByClaimsPrincipleWithAddressAsync(HttpContext.User);

            user.Address = _mapper.Map<AddressDto, Address>(address);

            var result = await _userManager.UpdateAsync(user);

            if (result.Succeeded) return Ok(_mapper.Map<Address, AddressDto>(user.Address));

            return BadRequest("Problem updating the user");
        }



        // Forgot, Reset password

        [HttpPost("confirmemail")]
        public async Task<ActionResult> ConfirmEmail(ConfirmEmailDto confirmEmail)
        {
            var user = await _userManager.FindByIdAsync(confirmEmail.userid);
            var confirm = await _userManager.ConfirmEmailAsync(user, confirmEmail.token);
            if (confirm.Succeeded) return Ok();

            return BadRequest("Unable to confirm email");
        }

        [HttpPost("forgotpassword")]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordDto forgotPassword)
        {
            var user = await _userManager.Users
                        .IgnoreQueryFilters()
                        .Where(e => e.Email.ToLower() == forgotPassword.Email.ToLower())
                        .FirstOrDefaultAsync();

            if (user == null) return Unauthorized("Username not Found");

            var token = await _userManager.GeneratePasswordResetTokenAsync(user);
            // var uriBuilder = new UriBuilder(_config["returnPaths:PasswordChange"]);
            string urlPath = "";
            var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            if (env.ToLower() == "development")
            {
                urlPath = _config["returnPaths:Passwordchange"];
            }
            else
            {
                urlPath = Environment.GetEnvironmentVariable("ReturnPaths:PasswordChange");
            }
            var uriBuilder = new UriBuilder(urlPath);

            var query = HttpUtility.ParseQueryString(uriBuilder.Query);
            query["token"] = token;
            query["userid"] = user.Id.ToString();
            uriBuilder.Query = query.ToString();
            var changePasswordLink = uriBuilder.ToString();

            var message = new Message(new string[] { user.Email }, "Change Password link", changePasswordLink, null);
            await _emailSender.SendEmailAsync(message);

            return Ok();
        }

        [HttpPost("resetpassword")]
        public async Task<ActionResult> ResetPassword(ResetPasswordDto resetPassword)
        {
            var user = await _userManager.FindByIdAsync(resetPassword.UserId);
            if (user == null) return Unauthorized("Username not Found");

            var result = await _userManager.ResetPasswordAsync(user, resetPassword.Token, resetPassword.Password);
            if (result.Succeeded) return Ok();

            return BadRequest("Unable to reset password");
        }



        
        [HttpPost("authenticate-facebook")]
        public async Task<ActionResult<UserDto>> AuthenticateFacebook([FromBody] UserDto socialUser)     // fac POST cu date venite de la FB
        {
            if (CheckEmailExistsAsync(socialUser.Email).Result.Value)
            {
                var userFb = await _userManager.FindByEmailAsync(socialUser.Email);
                return new UserDto
                {
                    Email = userFb.Email,
                    Token = await _tokenService.CreateToken(userFb),
                    DisplayName = userFb.DisplayName
                };
                //return new BadRequestObjectResult(new ApiValidationErrorResponse
                //{ Errors = new[] { "Email address is in use." } });
            }

            var user = new AppUser
            {
                DisplayName = socialUser.DisplayName,
                UserName = socialUser.DisplayName,
                Email = socialUser.Email
            };

            var result = await _userManager.CreateAsync(user, user.Email.ToUpper() + user.DisplayName);      // hash Email - unique (concat email + username)

            if (!result.Succeeded)
                return BadRequest(result.Errors);

            await _userManager.GenerateEmailConfirmationTokenAsync(user);
            user.EmailConfirmed = true;
          
            var roleResult = await _userManager.AddToRoleAsync(user, "Member");

            if (!roleResult.Succeeded) return BadRequest(result.Errors);

            return new UserDto
            {
                DisplayName = user.DisplayName,
                Token = await _tokenService.CreateToken(user),
                Email = user.Email
            };
        }

    }
}
