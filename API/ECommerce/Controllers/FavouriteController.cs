﻿using API.Extensions;
using Core.Entities;
using Core.Entities.Identity;
using Core.Interfaces;
using Core.Specifications;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Controllers
{
    [Authorize]
    public class FavouriteController : BaseApiController
    {
        private readonly IGenericRepository<Product> _productsRepo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IFavouriteRepository _favouritesRepo;
        private readonly UserManager<AppUser> _userManager;

        public FavouriteController(IUnitOfWork unitOfWork, IFavouriteRepository favouritesRepo, UserManager<AppUser> userManager)
        {
            _unitOfWork = unitOfWork;
            _favouritesRepo = favouritesRepo;
            _userManager = userManager;
        }


        [HttpPost("{productId}")]        // product they're gonna be marking as Favourite
        public async Task<ActionResult> AddFavourite(int productId)
        {
            var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
            var sourceUserId = user.Id;
            var favouriteProduct = await _unitOfWork.Repository<Product>().GetByIdAsync(productId);
            var sourceUser = await _favouritesRepo.GetUserWithFavourites(sourceUserId);
         //   var sourceUser = await _userManager.FindByIdAsync(sourceUserId.ToString());

            if (favouriteProduct == null) return NotFound();

            var userFavourite = await _favouritesRepo.GetUserFavourite(sourceUserId, productId);

            if (userFavourite != null) return BadRequest("You already like this product.");

            userFavourite = new Favourite
            {
                SourceUserId = sourceUserId,
                SourceUser = user,
                FavouriteProductId = productId,
                FavouriteProduct = favouriteProduct
            };

            sourceUser.FavouriteProducts.Add(userFavourite);

            if (await _unitOfWork.Complete() == 1) return Ok();

            return BadRequest("Failed to like product.");
        }



        [HttpGet]
        public async Task<ActionResult<IEnumerable<FavouriteDto>>> GetUserFavourites([FromQuery] FavouriteParams favouriteParams)
        {
            var user = await _userManager.FindByEmailFromClaimsPrinciple(HttpContext.User);
            favouriteParams.UserId = user.Id;
            var favourites = await _favouritesRepo.GetUserFavourites(favouriteParams);

            Response.AddPaginationHeader(favourites.CurrentPage, favourites.PageSize, favourites.TotalCount, favourites.TotalPages);

            return Ok(favourites);
        }
    }
}
