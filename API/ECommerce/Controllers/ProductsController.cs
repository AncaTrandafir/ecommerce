﻿using Infrastructure.Data;
using Core.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Interfaces;
using Core.Specifications;
using API.Dtos;
using AutoMapper;
using API.Errors;
using Microsoft.AspNetCore.Http;
using API.Helpers;
using System.ComponentModel;
using Microsoft.AspNetCore.Authorization;

namespace API.Controllers
{
    //  [ApiController]
    //  [Route("api/[controller]")]
    // public class ProductsController : ControllerBase

    public class ProductsController : BaseApiController
    {
        // private readonly IProductRepository _repo;

        // public ProductsController(IProductRepository repo)             {
        //     _repo = repo;

        private readonly IGenericRepository<Product> _productsRepo;
        private readonly IGenericRepository<ProductBrand> _productBrandRepo;
        private readonly IGenericRepository<ProductType> _productTypeRepo;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IPhotoService _photoService;

        public ProductsController(IGenericRepository<Product> productsRepo,
                                  IGenericRepository<ProductBrand> productBrandRepo,
                                  IGenericRepository<ProductType> productTypeRepo,
                                  IMapper mapper,
                                  IUnitOfWork unitOfWork,
                                  IPhotoService photoService)   
        {
            _productsRepo = productsRepo;
            _productBrandRepo = productBrandRepo;
            _productTypeRepo = productTypeRepo;
            _mapper = mapper;   // automapper
            _unitOfWork = unitOfWork;
            _photoService = photoService;
        }



        [Cached(600)]   // our own attribute built in Helpers; 600 seconds timeToLive
        [HttpGet]
        public async Task<ActionResult<Pagination<ProductToReturnDto>>> GetProducts([FromQuery]ProductSpecParams productParams)
        {
            // var products = await _productsRepo.ListAllAsync();

            var spec = new ProductsWithTypesAndBrandsSpecification(productParams);  // it includes the Includes

            var countSpec = new ProductWithFilterForCountSpecification(productParams);

            var totalItems = await _productsRepo.CountAsync(countSpec);

            var products = await _productsRepo.ListAsync(spec);

            // Convert manually to Dto
            //    return Ok(products.Select(
            //        product => new ProductToReturnDto
            //        {
            //            Id = product.Id,
            //            Name = product.Name,
            //            Description = product.Description,
            //            PictureUrl = product.PictureUrl,
            //            Price = product.Price,
            //            ProductBrand = product.ProductBrand.Name,
            //            ProductType = product.ProductType.Name
            //        }).ToList());
            //}

            // Automapper for Dto

            var data = _mapper.Map<IReadOnlyList<Product>, IReadOnlyList<ProductToReturnDto>>(products);

            return Ok(new Pagination<ProductToReturnDto>(productParams.PageIndex, productParams.PageSize, totalItems, data));
        }



        [Cached(600)]
        [HttpGet("{Id}", Name = "GetProduct")]
        [ProducesResponseType(StatusCodes.Status200OK)]     // responses for Swagger
        [ProducesResponseType(typeof (ApiResponse), StatusCodes.Status404NotFound)]
        public async Task<ActionResult<Product>> GetProduct(int id)
        {
            var spec = new ProductsWithTypesAndBrandsSpecification(id);
            // return await _productsRepo.GetByIdAsync(Id);
            var product = await _productsRepo.GetEntityWithSpec(spec);

            if (product == null) return NotFound(new ApiResponse(404));

            return Ok(_mapper.Map<Product, ProductToReturnDto>(product));

        }



        [Cached(600)]
        [HttpGet("brands")]
        public async Task<ActionResult<IReadOnlyList<Product>>> GetProductBrands()
        {
           return Ok(await _productBrandRepo.ListAllAsync()); 
        }



        [Cached(600)]
        [HttpGet("types")]
        public async Task<ActionResult<IReadOnlyList<Product>>> GetProductTypes()
        {
            return Ok(await _productTypeRepo.ListAllAsync());
        }




        // CRUD for Products making use of UnitOfWork this time instead of GenericRepositories.

        [Authorize(Roles = "Admin, Moderator")]
        [HttpPost]
        public async Task<ActionResult<ProductToReturnDto>> CreateProduct(ProductCreateDto productToCreate)
        {
            var product = _mapper.Map<ProductCreateDto, Product>(productToCreate);

            _unitOfWork.Repository<Product>().Add(product);

            var result = await _unitOfWork.Complete();

            if (result <= 0) return BadRequest(new ApiResponse(400, "Problem creating product."));

            return Ok(_mapper.Map<Product, ProductToReturnDto>(product));
        }



        [Authorize(Roles = "Admin, Moderator")]
        [HttpPut("{id}")]
        public async Task<ActionResult<ProductToReturnDto>> UpdateProduct(int id, ProductCreateDto productToUpdate)
        {
            var product = await _unitOfWork.Repository<Product>().GetByIdAsync(id);

            _mapper.Map(productToUpdate, product);

            _unitOfWork.Repository<Product>().Update(product);

            var result = await _unitOfWork.Complete();

            if (result <= 0) return BadRequest(new ApiResponse(400, "Problem updating product."));

            return Ok(_mapper.Map<Product, ProductToReturnDto>(product));
        }



        [Authorize(Roles = "Admin, Moderator")]
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteProduct(int id)
        {
            var product = await _unitOfWork.Repository<Product>().GetByIdAsync(id);

            // Ensure the photo is removed from disk when we delete a product 
            // and there is a little check in there to make sure we don’t delete any of our original seeded images.
            foreach (var photo in product.Photos)
            {
                if (photo.Id > 18)
                {
                    _photoService.DeleteFromDisk(photo);
                }
            }

            _unitOfWork.Repository<Product>().Delete(product);

            var result = await _unitOfWork.Complete();

            if (result <= 0) return BadRequest(new ApiResponse(400, "Problem deleting product."));

            return Ok();
        }


        // Get the product with spec so we get the photos collection with the product. 
        // We want to return the product after creation so we need the includes methods here so that we also return the brand, type and photos for the product.

        [HttpPut("{id}/photo")]
        [Authorize(Roles = "Admin, Moderator")]
        public async Task<ActionResult<ProductToReturnDto>> AddProductPhoto(int id, [FromForm] ProductPhotoDto photoDto)
        {
            var spec = new ProductsWithTypesAndBrandsSpecification(id);
            var product = await _unitOfWork.Repository<Product>().GetEntityWithSpec(spec);

            if (photoDto.Photo.Length > 0)
            {
                var photo = await _photoService.SaveToDiskAsync(photoDto.Photo);

                if (photo != null)
                {
                    product.AddPhoto(photo.PhotoUrl, photo.FileName);

                    _unitOfWork.Repository<Product>().Update(product);

                    var result = await _unitOfWork.Complete();

                    if (result <= 0) return BadRequest(new ApiResponse(400, "Problem adding photo product."));
                }
                else
                {
                    return BadRequest(new ApiResponse(400, "Problem saving photo to disk."));
                }
            }

            return _mapper.Map<Product, ProductToReturnDto>(product);
        }



        [HttpDelete("{id}/photo/{photoId}")]
        [Authorize(Roles = "Admin, Moderator")]
        public async Task<ActionResult> DeleteProductPhoto(int id, int photoId)
        {
            var spec = new ProductsWithTypesAndBrandsSpecification(id);
            var product = await _unitOfWork.Repository<Product>().GetEntityWithSpec(spec);

            var photo = product.Photos.SingleOrDefault(x => x.Id == photoId);

            if (photo != null)
            {
                if (photo.isMain)
                    return BadRequest(new ApiResponse(400,
                        "You cannot delete the main photo."));

                _photoService.DeleteFromDisk(photo);
            }
            else
            {
                return BadRequest(new ApiResponse(400, "Photo does not exist."));
            }

            product.RemovePhoto(photoId);

            _unitOfWork.Repository<Product>().Update(product);

            var result = await _unitOfWork.Complete();

            if (result <= 0) return BadRequest(new ApiResponse(400, "Problem adding photo product."));

            return Ok();
        }



        [HttpPost("{id}/photo/{photoId}")]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult<ProductToReturnDto>> SetMainPhoto(int id, int photoId)
        {
            var spec = new ProductsWithTypesAndBrandsSpecification(id);
            var product = await _unitOfWork.Repository<Product>().GetEntityWithSpec(spec);

            if (product.Photos.All(x => x.Id != photoId)) return NotFound();

            product.SetMainPhoto(photoId);

            _unitOfWork.Repository<Product>().Update(product);

            var result = await _unitOfWork.Complete();

            if (result <= 0) return BadRequest(new ApiResponse(400, "Problem adding photo product."));

            return _mapper.Map<Product, ProductToReturnDto>(product);
        }
    }
}
