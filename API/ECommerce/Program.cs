using Core.Entities.Identity;
using Infrastructure.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace ECommerce
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            // CreateHostBuilder(args).Build().Run();

            // Nu mai generez DB din console cu update-database
            var host = CreateHostBuilder(args).Build();

            // all variables inside using will be disposed when code finishes
            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                var loggerFactory = services.GetRequiredService<ILoggerFactory>();
               
                try
                {
                    // Identity DB
                    var userManager = services.GetRequiredService<UserManager<AppUser>>();
                    var roleManager = services.GetRequiredService<RoleManager<AppRole>>();

                    // it creates the DB if not already exists
                    var context = services.GetRequiredService<StoreContext>();               

                    await context.Database.MigrateAsync();
                    
                    await StoreContextSeed.SeedUsersAsync(userManager, roleManager, loggerFactory);
                    await StoreContextSeed.SeedAsync(context, loggerFactory);

                    //   var identityContext = services.GetRequiredService<AppIdentityDbContext>();
                    //   await identityContext.Database.MigrateAsync();

                }   
                
                catch (Exception ex)
                {
                    var logger = loggerFactory.CreateLogger<Program>();
                    logger.LogError(ex, "An error occured during migration.");
                }
            }

            await host.RunAsync();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
