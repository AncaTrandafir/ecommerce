﻿using API.Dtos;
using AutoMapper;
using Core.Entities;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Helpers
{
    public class ProductUrlResolver : IValueResolver<Product, ProductToReturnDto, string>       // PhotoUrl is string
    {
        private readonly IConfiguration _config;

        public ProductUrlResolver(IConfiguration config)
        {
            _config = config;
        }

        // Map from photoUrl to API address + photoUrl
        public string Resolve(Product source, ProductToReturnDto destination, string destMember, ResolutionContext context)
        {
            var photo = source.Photos.FirstOrDefault(x => x.isMain);

            if (photo != null)
           // if (!string.IsNullOrEmpty(source.Photos))
            {
                return _config["ApiUrl"] + photo.PhotoUrl;   // definit in appSettings.json; rezulta fullPath of the image
            }
            return _config["ApiUrl"] + "images/products/placeholder.png";
        }
    }
}
