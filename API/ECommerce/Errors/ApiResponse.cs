﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Errors
{
    public class ApiResponse
    {
        public ApiResponse(int statusCode, string message = null)
        {
            StatusCode = statusCode;
            Message = message ?? GetDefaultMessageForStatusCode(statusCode);
                // ?? means if this is null do whatever it is after 
        }
      

        public int StatusCode{ get; set; }
        public string Message { get; set; }



        private string GetDefaultMessageForStatusCode(int statusCode)
        {
            return statusCode switch    // switch instead of case-break
            {
                400 => "A bad request, you have made.",
                401 => "Authorized, you are not.",
                404 => "Resource found, it was not.",
                500 => "Errors are the path to the dark side. Errors lead to anger. Anger leads to hate. Hate leads to career change.",
                _ => null
            };
          
        }

    }
}
